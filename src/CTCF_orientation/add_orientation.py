#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script aims to add orientation to peaks located \
inside genomic interval given in column upstream hit and downstream_hit of \
a table file
"""
import pandas as pd
from pathlib import Path
import subprocess as sp
import numpy as np
from typing import Dict
from .config_orientation import ConfigOrientation as ConfO


def get_peaks(df_dist: pd.DataFrame) -> pd.DataFrame:
    """
    Create a bed file containing CTCF peaks defined in the table \
    df_dist.

    :param df_dist: The dataframe containing exon and their distance to the \
    closest upstream or downstream CTCF peak.
    :return: A dataframe in a bed format containing the genomic intervals \
    were a CTCF peak is located

    >>> import numpy as np
    >>> d = pd.DataFrame({"upstream_hit": ["10:20-40", "X:1000-5000",
    ... np.nan],
    ... "downstream_hit": ["8:10-15", "9:7-20", np.nan]})
    >>> get_peaks(d)
      chr  start  stop
    0   8     10    15
    1   9      7    20
    2  10     20    40
    3   X   1000  5000
    """
    tmp1 = df_dist[-df_dist["downstream_hit"].isna()].copy()
    tmp2 = df_dist[-df_dist["upstream_hit"].isna()].copy()
    peaks = tmp1["downstream_hit"].str.split(r"[:-]").to_list()
    peaks += tmp2["upstream_hit"].str.split(r"[:-]").to_list()
    cat = list(map(str, range(1, 24))) + ["X", "Y"]
    df = pd.DataFrame(peaks, columns=["chr", "start", "stop"])\
        .drop_duplicates()
    df["chr"] = pd.Categorical(df["chr"], categories=cat, ordered=True)
    df["start"] = df["start"].astype(int)
    df["stop"] = df["stop"].astype(int)
    return df.sort_values(["chr", "start"], ascending=True)


def resize_peaks(df_bed: pd.DataFrame, size: int) -> pd.DataFrame:
    """
    resize peaks.

    :param df_bed: A dataframe of genomic intervals corresponding \
    to CTCF peaks
    :param size: The size used to resize the intervals
    :return: the dataframe with peak resized

    >>> d = pd.DataFrame({'chr': ['8', '9', '10', 'X'],
    ... 'start': [10, 7, 20, 1000],
    ... 'stop': [15, 20, 40, 5000]})
    >>> resize_peaks(d, 5)
      chr  start  stop
    0   8      7    17
    1   9      8    18
    2  10     25    35
    3   X   2995  3005

    """
    middle = np.floor((df_bed["start"] + df_bed["stop"]) / 2)
    nstart = middle - size
    nend = middle + size
    df_bed["start"] = nstart
    df_bed["stop"] = nend
    df_bed["start"] = df_bed["start"].astype(int)
    df_bed["stop"] = df_bed["stop"].astype(int)
    return df_bed


def write_bedfile(df_bed: pd.DataFrame, outfile: Path) -> None:
    """
    Create a bedfile containing the closest CTCF peaks upstream and dowstream \
    an exon.

    :param df_bed: containing the closest CTCF peaks upstream and dowstream \
    an exon for every exon
    :param outfile: The file were the bedfile will be created
    """
    df_bed.to_csv(outfile, sep="\t", index=False, header=False)


def gimmemotifs(bed_file: Path, outfile: Path, genome: Path, pfm: Path
                ) -> None:
    """

    :param bed_file: A file containing peaks that are close to an exon.
    :param outfile: The file where the gimmemotifs result will be stored
    :param genome: The genome to use for this analysis
    :param pmf: The matrix used to find CTCF motifs
    """
    cmd = f"gimme scan {bed_file} -g {genome} -b -p {pfm} -n 1 -c 0.8 " \
          f"> {outfile}"
    sp.check_call(cmd, shell=True)


def recover_peak_hit(bed_peak: Path, gimme_result: Path,
                     outfile: Path) -> None:
    """
    Create a bed file containing CTCF peak interval and gimme detected peaks.

    :param bed_peak: A bed file containing peaks
    :param gimme_result: The bed file containing gimmemotifs detected peaks
    """
    cmd = f"bedtools intersect -b {bed_peak} -a {gimme_result} -wa -wb > {outfile}"
    sp.check_call(cmd, shell=True)


def link_motif_strand_2hit(df_full: pd.DataFrame) -> Dict:
    """
    Create a dictionary that links a genomic interval containing a CTCF \
    peak to the strand of the motif to see if they are convergent or divergent.

    :param df_full: A dataframe containing the gimmemotifs prediction results \
    and the CTCF peaks interval
    :return: The dictionary containing motif location

    >>> d = pd.DataFrame({"c1": ["1", "1"], "s1": [10, 15], "e1": [15, 20],
    ... "n": ["MA CTCF"] * 2, "s": [10, 20], "strand": ["+", "-"],
    ... "chr": ["1", "1"], "start": [5, 10], "stop": [20, 25]})
    >>> link_motif_strand_2hit(d)
    {'1:5-20': '+', '1:10-25': '-'}
    """
    d = {}
    for i in range(df_full.shape[0]):
        row = df_full.iloc[i, :]
        name = f'{row["chr"]}:{row["start"]}-{row["stop"]}'
        d[name] = row["strand"]
    return d


def add_hit_strand(df_dist: pd.DataFrame, dic_strand: Dict[str, str]
                   ) -> pd.DataFrame:
    """
    Add the hit strand of each CTCF peak predicted by gimmemotifs.

    :param df_dist: A dictionary containing the intervals of \
    the closest downstream or upstream CTCF peak to an exon.
    :param dic_strand: A dictionary indicating The strand of the CTCF sites \
    in those peaks
    :return: The table df_dist with the strand of hit.

    >>> ds = {'1:5-20': '+', '1:10-25': '-'}
    >>> e = pd.DataFrame({'exon_name': ['A1BG_1', 'A1BG_2'],
    ... 'exon_id': ['1:5-20', '1:10-25'], 'dist': [0, 0],
    ... 'CTCF_hit_id': ['1:5-20', '1:10-25'],
    ... 'upstream_dist': [0, 0],
    ... 'upstream_hit': ['1:5-20', np.nan],
    ... 'downstream_dist': [0, 0],
    ... 'downstream_hit': [np.nan, '1:10-25'],
    ... 'infileTarget': ['CTCF_merged.bed', 'CTCF_merged.bed'],
    ... 'group': ['First', 'CE'],
    ... 'strand': ['-', '-']})
    >>> add_hit_strand(e, ds)[['CTCF_hit_id_strand', "upstream_hit_strand",
    ... "downstream_hit_strand"]].to_dict("list") == {
    ... 'CTCF_hit_id_strand': ['+', '-'], 'upstream_hit_strand': ['+', np.nan],
    ... 'downstream_hit_strand': [np.nan, '-']}
    True
    """
    for col in ["downstream_hit", "upstream_hit", "CTCF_hit_id"]:
        df_dist[f"{col}_strand"] =  df_dist[col].map(dic_strand)
    return df_dist[['exon_name', 'exon_id', 'dist', 'CTCF_hit_id',
                    'CTCF_hit_id_strand', 'upstream_dist', 'upstream_hit',
                    'upstream_hit_strand', 'downstream_dist', 'downstream_hit',
                    'downstream_hit_strand', 'infileTarget', 'group',
                    'strand']]


def update_dist_table(dist_file: Path, closest_peak_bed: Path,
                      gimme_result: Path, full_peak_bed: Path, genome: Path,
                      pfm: Path, final_file: Path) -> None:
    """
    add orientation to peaks located \
    inside genomic interval given in column upstream hit and downstream_hit of \
    a table file

    :param dist_file: A fuile containing the distance to the upstream and \
    downstream closest CTCF peak to an exon
    :param closest_peak_bed: A bed file containing only
    :param gimme_result: The bed file containing predicted CTCF sites \
    orientation
    :param full_peak_bed: A bed file containing predicted CTCF sites \
    orientation and the interval of the closest CTCF peak to an exon \
    where this site belongs.
    :param genome: A genome fasta file file
    :param pfm: motif file in terms of count matrix
    :param final_file: The file where the updated table will be stored
    """
    df_dist = pd.read_csv(dist_file, sep="\t")
    df_peaks = get_peaks(df_dist)
    write_bedfile(df_peaks, closest_peak_bed)
    gimmemotifs(closest_peak_bed, gimme_result, genome, pfm)
    recover_peak_hit(closest_peak_bed, gimme_result, full_peak_bed)
    df = pd.read_csv(full_peak_bed, sep="\t",
                     names=["c1", "s1", "e1", "n", "s", "strand", "chr",
                            "start", "stop"])
    dic_strand = link_motif_strand_2hit(df)
    ndf = add_hit_strand(df_dist, dic_strand)
    ndf.to_csv(final_file, sep="\t", index=False)


def launch_update():
    """
    add orientation to peaks located \
    inside genomic interval given in column upstream hit and downstream_hit of \
    a table file
    """
    update_dist_table(ConfO.dist, ConfO.closest_peak_bed,
                      ConfO.gimme_res, ConfO.full_peak_bed, ConfO.genome,
                      ConfO.pfm, ConfO.outfile)


if __name__ == "__main__":
    launch_update()