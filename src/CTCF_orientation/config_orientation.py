#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This file contains the path variables used in this module
"""


from pathlib import Path


class ConfigOrientation:
    """
    contains the path variables used in this module
    """
    output = Path(__file__).parents[2] / "result" / "CTCF_distance_files"
    data = Path(__file__).parents[2] / "data"
    dist = output / "All_Exons_vs_CTCF.csv"
    outfile = output / "All_Exons_vs_CTCF_orientation.csv"
    closest_peak_bed = output / "tmp.bed"
    gimme_res = output / "gimme_result.bed"
    full_peak_bed = dist.parent / "complete.bed"
    genome = data / "Homo_sapiens.GRCh37.dna.primary_assembly.fa"
    pfm = data / "CTCF.pfm"


class ConfigAnalysis:
    """
    Contains the files/folders used in this module
    """
    result = Path(__file__).parents[2] / "result"
    input_folder = result / "vinity_DDX5_17"
    test_folder = Path(__file__).parents[2] / "test" / "files"
    output = result / "directional_analysis"
    gene_bed = ConfigOrientation.data / "gene.bed"