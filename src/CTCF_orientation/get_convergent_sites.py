#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: This script aims to study the CTCF sites orientation \
between first and last exons of genes belonging to different groups
"""
from pathlib import Path
from typing import Tuple, List, Optional
from .config_orientation import ConfigAnalysis
from loguru import logger
import pandas as pd
import re
import sys
import lazyparser as lp
from ..vicinity_DDX5_17.statistical_analysis import get_stat_figure
from ..vicinity_DDX5_17.stat_annot import add_stat_annotation
from ..vicinity_DDX5_17.create_terminal_exon_file import format_exon_bed
import seaborn as sns
from rpy2.robjects import r, pandas2ri
import numpy as np
import doctest


@logger.catch(reraise=True)
def find_orientation_files(factor: str, kind: str = "last"
                           ) -> Tuple[Path, Path]:
    """
    Find file with terminal and first exons, their closest CTCF site \
    and the orientation of this site.

    :param factor: The factor of interest
    :param kind: The kind of exons to recover downstream the first exons, \
    it can be 'last' or 'internal'
    :return: The file containing terminal and first exons
    """
    rf = list(ConfigAnalysis.input_folder.glob(f"first_exons_{factor}.csv"))

    if kind == "last":
        rl = list(ConfigAnalysis.input_folder.glob(f"terminal_exons_"
                                                   f"{factor}.csv"))
    else:
        rl = list(ConfigAnalysis.input_folder.glob(
            f"distance_CTCF_internal_n_SIPP_exons_{factor}.csv"))
        if not rl:
            rl = list(ConfigAnalysis.input_folder.glob(
                f"distance_CTCF_internal_n_{factor}_exons.csv"))
    if not rf or not rl:
        raise FileNotFoundError(f"No first of {kind} exon file found for "
                                f"factor {factor}")
    if len(rf) > 1 or len(rl) > 1:
        raise IndexError(f"Multiple first or {kind} exon files found for "
                         f"factor {factor}:\nfirst files: {rf}"
                         f"\nlast files {rl}")
    return rf[0], rl[0]


def update_group_table(df: pd.DataFrame) -> pd.DataFrame:
    """
    If a dataframe doesn't contains the column uniq_regulation \
    creates it along with gene id and gene_name columns.

    :param df: A dataframe
    :return: The dataframe with the column regulation and uniq regulation
    """
    df.drop("regulation", axis=1, inplace=True)
    df.rename({"new_group": "uniq_regulation"}, axis=1, inplace=True)
    df["regulation"] = df["uniq_regulation"]\
        .str.replace(r"_up|_down|_DOWN|_UP", "")
    exon_df = format_exon_bed()[["exon_id", "gene_name", "exon_pos",
                                 "gene_id"]]
    df['gene_name'] = df['exon_name'].str.replace(r'_\d+', '')
    df['exon_pos'] = df['exon_name'].str.extract(r'_(\d+)').astype(int)
    df = df.merge(exon_df, how="left", on=["exon_id", "gene_name", "exon_pos"])
    df = df[-df['gene_id'].isna()]
    df['gene_id'] = df['gene_id'].astype(int)
    return df


def load_table(site_file: Path, bad_col: str, site: str = "closest",
               kind: str = "last") -> pd.DataFrame:
    """load a table containing ctcf orientation

    :param site_file: A file containing ctcf orientation for some exons.
    :param site: the ctcf peak upstream downstream or closest to the exon \
    defaults to "closest"
    :param bad_col: The column to remove
    :param kind: The kind of exons to recover downstream the first exons, \
    it can be 'last' or 'internal'
    :return: the table with ctfc orientation loaded
    """
    df = pd.read_csv(site_file, sep="\t")
    if kind != "last":
        df = update_group_table(df)
    df["regulation"] = df["regulation"].str.upper()
    df["uniq_regulation"] = df["uniq_regulation"].str.upper()
    if site == "closest":
        return df.drop(bad_col, axis=1)
    df.drop(["CTCF_hit_id", "CTCF_hit_id_strand", "dist"], axis=1,
            inplace=True)
    rd = {f"{site}_hit": "CTCF_hit_id",
          f"{site}_hit_strand": "CTCF_hit_id_strand",
          f"{site}_dist": "dist"}
    df.rename(rd, axis=1, inplace=True)
    return df.drop(bad_col, axis=1)


def df_merger(dff: pd.DataFrame, dfl: pd.DataFrame, good_col: str, kind: str
              ) -> pd.DataFrame:
    """
    Merge the dataframe of first exons with the dataframe of last or \
    internal exons.

    :param dff: Dataframe containing first exons and their upstream and \
    downstream closest CTCF site
    :param dfl: Dataframe containing last/internal exons and their upstream \
    and downstream closest CTCF site
    :param good_col: The column indicating the group of exons of interest
    :param kind: The kind of exons located in dfl
    :return: dff and dfl merged
    """
    if kind == "last":
        c = ["gene_id", "last_exon", good_col, "gene_name", "strand"]
        c2 = ["gene_id", "exon_pos", good_col, "gene_name", "strand"]
        v = "1:1"
    else:
        c = ["gene_id", good_col, "gene_name", "strand"]
        c2 = ["gene_id", good_col, "gene_name", "strand"]
        v = None
    return dff.merge(dfl, how="inner", left_on=c, right_on=c2,
                     suffixes=["_first", "_last"], validate=v)


@logger.catch(reraise=True)
def match_files(first_file: Path, last_file: Path, bad_col: str,
                max_distance: int = 2000, site: str = "closest",
                kind: str = "last") -> pd.DataFrame:
    """
    Load first and last exons files table and merges them.

    :param first_file: file with first exons, their closest CTCF site \
    and the orientation of this site.
    :param last_file: file with first exons, their closest CTCF site \
    and the orientation of this site.
    :param max_distance: The maximum distance to a CTCF site exons must \
     have to be taken into account (default to 2000 nt)
    :param site: the ctcf peak upstream downstream or closest to the exon \
    defaults to "closest"
    :param bad_col: column to remove
    :param kind: The kind of exons to recover downstream the first exons, \
    it can be 'last' or 'internal'
    :return: The merged dataframe given in input

    >>> ff = ConfigAnalysis.test_folder / "first_test.txt"
    >>> lf = ConfigAnalysis.test_folder / "terminal_test.txt"
    >>> match_files(ff, lf, "uniq_regulation")[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_first", "CTCF_hit_id_strand_first",
    ... 'CTCF_hit_id_last', 'CTCF_hit_id_strand_last']].iloc[0, :]
    exon_name_first                           DSC2_1
    exon_name_last                           DSC2_19
    dist_first                                     0
    dist_last                                      0
    CTCF_hit_id_first           18:28681704-28682125
    CTCF_hit_id_strand_first                       +
    CTCF_hit_id_last            18:28645944-28645954
    CTCF_hit_id_strand_last                        +
    Name: 0, dtype: object
    >>> res = match_files(ff, lf, "uniq_regulation", 2000, "surrounding")
    >>> res[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_first", "CTCF_hit_id_strand_first",
    ... 'CTCF_hit_id_last', 'CTCF_hit_id_strand_last']].iloc[0, :]
    exon_name_first                           DSG1_1
    exon_name_last                           DSG1_16
    dist_first                                     0
    dist_last                                      0
    CTCF_hit_id_first           18:28846451-28846887
    CTCF_hit_id_strand_first                       +
    CTCF_hit_id_last            18:28976764-28977174
    CTCF_hit_id_strand_last                        -
    Name: 2, dtype: object
    >>> res[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_first", "CTCF_hit_id_strand_first",
    ... 'CTCF_hit_id_last', 'CTCF_hit_id_strand_last']].iloc[1, :]
    exon_name_first                           DSG4_1
    exon_name_last                           DSG4_16
    dist_first                                     0
    dist_last                                      0
    CTCF_hit_id_first           18:28919253-28919449
    CTCF_hit_id_strand_first                       +
    CTCF_hit_id_last            18:29004274-29004888
    CTCF_hit_id_strand_last                        +
    Name: 3, dtype: object
    >>> res.shape
    (2, 31)
    >>> lf = ConfigAnalysis.test_folder / "test_internal.txt"
    >>> res = match_files(ff, lf, "uniq_regulation", 2000, kind="internal")
    >>> res[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_strand_first", 'CTCF_hit_id_strand_last']
    ... ].rename({"CTCF_hit_id_strand_first": "csf",
    ... "CTCF_hit_id_strand_last": "csl"}, axis=1)
      exon_name_first exon_name_last  dist_first  dist_last csf csl
    0          DSC2_1         DSC2_3           0          0   +   +
    3          DSG1_1         DSG1_2           0          0   +   +
    4          DSG1_1         DSG1_3           0          0   +   +
    5          DSG4_1         DSG4_2           0          0   -   -
    6          DSG4_1         DSG4_3           0          0   -   -
    >>> res = match_files(ff, lf, "regulation", 2000, kind="internal")
    >>> res[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_strand_first", 'CTCF_hit_id_strand_last']
    ... ].rename({"CTCF_hit_id_strand_first": "csf",
    ... "CTCF_hit_id_strand_last": "csl"}, axis=1)
      exon_name_first exon_name_last  dist_first  dist_last csf csl
    0          DSC2_1         DSC2_3           0          0   +   +
    3          DSG4_1         DSG4_2           0          0   -   -
    4          DSG4_1         DSG4_3           0          0   -   -
    >>> res = match_files(ff, lf, "uniq_regulation", 2000, "surrounding",
    ... "internal")
    >>> res[["exon_name_first",
    ... "exon_name_last", "dist_first",
    ... "dist_last", "CTCF_hit_id_strand_first", 'CTCF_hit_id_strand_last']
    ... ].rename({"CTCF_hit_id_strand_first": "csf",
    ... "CTCF_hit_id_strand_last": "csl"}, axis=1)
      exon_name_first exon_name_last  dist_first  dist_last csf csl
    0          DSC2_1         DSC2_3         0.0          0   +   +
    3          DSG1_1         DSG1_2         0.0          0   +   +
    4          DSG1_1         DSG1_3         0.0          0   +   +
    5          DSG4_1         DSG4_2         0.0          0   +   -
    7          DSG4_1         DSG4_4         0.0          0   +   -
    """
    if site == "closest":
        dff = load_table(first_file, bad_col, site)
        dfl = load_table(last_file, bad_col, site,  kind)
    else:
        dff = load_table(first_file, bad_col, "upstream")
        dfl = load_table(last_file, bad_col, "downstream", kind)
    regl = ["regulation", "uniq_regulation"]
    good_col = dict(zip(regl, regl[::-1]))[bad_col]
    required_cols = ["CTCF_hit_id_strand", "gene_id", good_col]
    for req in required_cols:
        if req not in dff.columns or req not in dfl.columns:
            raise ValueError('Some required columns are missing !')
    df = df_merger(dff, dfl, good_col, kind)
    logger.info(f"Total gene found with first and last exons: {df.shape[0]}")
    df = df[(abs(df["dist_first"]) <= max_distance) &
            (abs(df["dist_last"]) <= max_distance)].copy()
    logger.info(f"Genes with first and last exons close "
                f"(dist <={max_distance}) to a CTCF peak: {df.shape[0]}")
    df = df[(-df["CTCF_hit_id_strand_first"].isna()) &
            (-df["CTCF_hit_id_strand_last"].isna())].copy()
    logger.info(f"Genes with first and last exons close to a CTCF peak with "
                f"defined orientation: {df.shape[0]}")
    return df


@logger.catch(reraise=True)
def get_orientation(mseries: pd.Series) -> Optional[str]:
    """
    get the orientation of two peaks.

    :param mseries: A row of a dataframe containing the first and \
    the last exon of a gene and their closest CTCF peaks
    :return:

    >>> s = pd.Series({"CTCF_hit_id_first": "1:10-20",
    ... "CTCF_hit_id_last": "1:100-110", "strand": "+",
    ... "CTCF_hit_id_strand_first": "+", "CTCF_hit_id_strand_last": "-"})
    >>> get_orientation(s)
    'Convergent'
    >>> s = pd.Series({"CTCF_hit_id_first": "1:10-20",
    ... "CTCF_hit_id_last": "1:100-110", "strand": "+",
    ... "CTCF_hit_id_strand_first": "+", "CTCF_hit_id_strand_last": "+"})
    >>> get_orientation(s)
    'Same_direction'
    >>> s = pd.Series({"CTCF_hit_id_last": "1:10-20",
    ... "CTCF_hit_id_first": "1:100-110", "strand": "-",
    ... "CTCF_hit_id_strand_first": "+", "CTCF_hit_id_strand_last": "-"})
    >>> get_orientation(s)
    'Divergent'
    >>> s = pd.Series({"CTCF_hit_id_last": "1:10-100",
    ... "CTCF_hit_id_first": "1:50-110", "strand": "-",
    ... "CTCF_hit_id_strand_first": "+", "CTCF_hit_id_strand_last": "-"})
    >>> get_orientation(s)
    'Overlapping'
    """
    for c in ["CTCF_hit_id_first", "CTCF_hit_id_last",
              "CTCF_hit_id_strand_first", "CTCF_hit_id_strand_last", "strand"]:
        if not isinstance(mseries[c], str):
            return None
    peak1 = re.split("[:-]", mseries["CTCF_hit_id_first"])
    peak2 = re.split("[:-]", mseries["CTCF_hit_id_last"])
    peak1[1] = int(peak1[1])
    peak1[2] = int(peak1[2])
    peak2[1] = int(peak2[1])
    peak2[2] = int(peak2[2])
    if peak1[0] != peak2[0]:
        raise ValueError("Peaks on different chromosomes")
    if (
        peak1[1] < peak2[2] <= peak1[2]
        or peak1[1] <= peak2[1] < peak1[2]
        or peak2[1] < peak1[2] <= peak2[2]
        or peak2[1] <= peak1[1] < peak2[2]
    ):
        return "Overlapping"
    if mseries["CTCF_hit_id_strand_first"] == \
            mseries["CTCF_hit_id_strand_last"]:
        return "Same_direction"
    if mseries["strand"] == mseries["CTCF_hit_id_strand_first"]:
        return "Convergent"
    return "Divergent"


def compute_site_orientation(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add a column indicating the orientation of CTCF sites relative to the gene.

    :param df: A dataframe containing the first and last exons of genes \
    and their distance to the closest CTCF site along with it's orientation
    :return:The kind of orientation

    >>> d = pd.DataFrame({'CTCF_hit_id_first': ['1:10-20', '1:10-20',
    ... '1:100-110', '1:50-110'],
    ... 'CTCF_hit_id_last': ['1:100-110', '1:100-110', '1:10-20', '1:10-100'],
    ... 'strand': ['+', '+', '-', '-'],
    ... 'CTCF_hit_id_strand_first': ['+', '+', '+', '+'],
    ... 'CTCF_hit_id_strand_last': ['-', '+', '-', '-']})
    >>> compute_site_orientation(d)["orientation"]
    0        Convergent
    1    Same_direction
    2         Divergent
    3       Overlapping
    Name: orientation, dtype: object
    """
    df["orientation"] = df.apply(get_orientation, axis=1)
    return df


def convergent_site(df: pd.DataFrame) -> pd.DataFrame:
    """
    Create a column is_convergent based on orientation column.

    :param df: A dataframe containing an orientation column
    :return: The dataframe with a column is_convergent

    >>> d = pd.DataFrame({"orientation": ["Convergent", "Divergent",
    ... "Same_direction", "Overlapping"]})
    >>> convergent_site(d)
          orientation  is_convergent
    0      Convergent              1
    1       Divergent              0
    2  Same_direction              0
    3     Overlapping              0
    """
    df["is_convergent"] = [0] * df.shape[0]
    df.loc[df["orientation"] == "Convergent", "is_convergent"] = 1
    return df


def glm_binomial_stat(df: pd.DataFrame, output: Path, group_col: str,
                      partial_name: str) -> pd.DataFrame:
    """Perform a glm binomial test on ``df``

    :param df: A dataframe of exons and the distance to the closest CTCF \
    binding site
    :param output: The result folder
    :param partial_name: thee figure partial name
    :param group_col: The column containing the groups of genes
    :return The tukey's test results
    """
    tmp = df[["is_convergent", group_col]].copy()
    pandas2ri.activate()
    stat_s = r("""
    require("DHARMa")
    require("emmeans")
    function(data, output, partial_name){
        data$%s <- as.factor(data$%s)
        mod <- glm(is_convergent ~ %s, family="binomial", data=data)
        simulationOutput <- simulateResiduals(fittedModel = mod, n = 1000)
        png(paste0(output, "/dignostics_", partial_name, ".png"))
        plot(simulationOutput)
        dev.off()
        comp <- emmeans(mod, ~ %s)
        tab <- as.data.frame(pairs(comp))
        tab <- tab[, c('contrast', 'estimate', 'SE', 'z.ratio', 'p.value')]
        colnames(tab) <- c("Comparison", "Estimate", "Std.Error", 
                           "z.ratio", "p.value")
        return(tab)
    }
    """ % (group_col, group_col, group_col, group_col))
    return stat_s(tmp, str(output), partial_name)


def barplot_maker(df: pd.DataFrame, output: Path,
                  stats: List[List], group_col: str,
                  partial_name: str):
    """
    Create a barplot showing The percentage of close exons between \
    ctrl siPP_DOWN and siPP_UP.

    :param df: dataframe with exon and their \
    CTCF exons sites
    :param output: Folder were the result will be created
    :param stats: A list containing sublist. Each sublist contains \
    the name of the groups analyzed along whith their p-value
    :param partial_name: The partial name of the figure
    :param group_col: Column that contains the group type of the analyzed \
    gene
    """
    sns.set()
    sns.set_context("poster")
    df2 = df[[group_col, "is_convergent", "orientation"]].pivot_table(
        index=group_col, columns="is_convergent", aggfunc="count")
    df2.columns = df2.columns.get_level_values(1)
    df2["prop"] = df2[1] / (df2[0] + df2[1]) * 100
    df2.reset_index(inplace=True)
    g = sns.catplot(x=group_col, y="prop", data=df2, kind="bar", ci=None,
                    height=9, aspect=1.77)
    g.ax.set_xlabel("")
    g.ax.set_ylabel("Gene proportion (%)")
    g.ax.set_title('Proportion of gene with convergent CTCF sites')
    add_stat_annotation(g.ax, data=df2, x=group_col, y="prop",
                        loc='inside',
                        boxPairList=stats,
                        linewidth=1, fontsize="xx-small",
                        lineYOffsetToBoxAxesCoord=0.02)
    g.savefig(output / f"barplot_{partial_name}.pdf")


def append_prop(df: pd.DataFrame, filename: Path, threshold: int,
                group_column: str) -> None:
    """
    Add proportion of close CTCFs, sites for different kind of exons.

    :param df: dataframe with exon and their CTCF exons sites
    :param filename: file were the results will be written
    :param threshold: the distance in nucleotide between an CTCF biding site \
    and the exon
    :param group_column: The name of the group colum of interest
    """
    exon_type = df[group_column].unique()
    with filename.open(mode="a") as f:
        for ctype in exon_type:
            convergent = len(df.loc[(df[group_column] == ctype) &
                                    (df['is_convergent'] == 1), :])
            other = len(df.loc[(df[group_column] == ctype), :])
            res = round(convergent / other * 100, 2)
            f.write(f"{res} %  ({convergent} / {other}) of {ctype} genes "
                    f"with convergent CTCF sites near their first and terminal"
                    f"exons at most {threshold} nucleotide\n")


@lp.parse(verbose=["INFO", "DEBUG", "ERROR"],
          group_column=["regulation", "uniq_regulation"],
          kind=["last", "internal"], site=["closest", "surrounding"])
def get_directional_results(factor: str, max_distance: int = 2000,
                            group_column: str = "regulation",
                            site: str = "closest", kind: str = "last",
                            verbose: str = "INFO") -> None:
    """
    Create the table containing directional results.

    :param factor: The factor of interest
    :param max_distance: The maximum distance to a CTCF site exons must \
     have to be taken into account (default to 2000 nt)
    :param group_column: The group column of interest (default 'regulation')
    :param site: the ctcf peak upstream downstream or closest to the exon \
    (default "closest")
    :param kind: The kind of exons to recover downstream the first exons, \
    it can be 'last' or 'internal'
    :param verbose: The level of information to display (default INFO)
    """
    ConfigAnalysis.output.mkdir(exist_ok=True)
    outdiag = ConfigAnalysis.output / "diags"
    logger.remove()
    logger.add(sys.stderr, colorize=True, format="<level>{message}</level>",
               level=verbose)
    fmt = "{time} - {level} - {name} - {message}"
    pn = f"{factor}_{max_distance}_{group_column}_{site}_{kind}"
    logger.add(ConfigAnalysis.output / f"{pn}.log",
               backtrace=True, diagnose=True, format=fmt)
    script = str(Path(sys.argv[0]).relative_to(Path(".").resolve())
                 ).replace("/", ".").strip(".py")
    logger.info(f'python3 -m {script}  '
                f'{" ".join(sys.argv[1:])}')
    logger.debug("find orientation file")
    ff, lf = find_orientation_files(factor, kind)
    logger.debug("match file")
    regl = ["regulation", "uniq_regulation"]
    bc = dict(zip(regl, regl[::-1]))[group_column]
    df = match_files(ff, lf, bc, max_distance, site, kind)
    logger.debug("compute orientation")
    df = compute_site_orientation(df)
    df = convergent_site(df)
    df = df[df[group_column] != "MULTIPLE"].copy()
    df.to_csv(ConfigAnalysis.output /
              f"Orientation_{pn}.txt", sep="\t",
              index=False)
    group, counts = np.unique(df[group_column].values, return_counts=True)
    logger.info("recovered groups of genes: ")
    for i, g in enumerate(group):
        logger.info(f"gene {g}: {counts[i]} / {df.shape[0]}")
    outdiag.mkdir(exist_ok=True)
    res = glm_binomial_stat(df, outdiag, group_column, pn)
    res_file = ConfigAnalysis.output / f"stat_{pn}.txt"
    res.to_csv(res_file, sep="\t", index=False)
    append_prop(df, res_file, max_distance, group_column)
    barplot_maker(df, ConfigAnalysis.output, get_stat_figure(res),
                  group_column, pn)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        get_directional_results()
    else:
        doctest.testmod()
