#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Execute all script from this module
"""

from .gene_size_distribution import create_figure
from .scatter_gene_size_chiapet_loop import lineplot_maker
from .intragenic_loop_watcher import get_loop
import lazyparser as lp


@lp.parse
def launcher(control: str = "SRSF1", kind: str = "internal",
             global_weight: int = 3, weight: int = 2,
             project: str = "GSM970215", threshold: int = 15000,
             distance: int = 10000, mode: str = "DDX",
             logging_level: str = "DISABLE"):
    """
    Launch figure in this submodule

    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param threshold: The minimum size of gene to consider
    :param distance: The minimum required distance to consider an \
    interaction between exons
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table (default DDX)
    :param logging_level: The level of data to display
    """
    create_figure()
    lineplot_maker(distance)
    get_loop(control, kind, global_weight, weight, project, threshold,
             distance, mode, logging_level)


launcher()
