#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Configuration variables used in this module
"""

from pathlib import Path
from doctest import testmod
from typing import List
from itertools import filterfalse


def get_p(x: str):
    """
    return False if x contains 'Keep' True else

    >>> get_p('Keep: GMX32, GSM78')
    False
    >>> get_p('Delete: Bou, foo, bar')
    True
    """
    if "Keep" in x:
        return False
    else:
        return True


def get_good_project(project_file: Path) -> List[str]:
    """
    Return only good ChIA-PET projects base on TAD co-localisation results

    :param project_file: A file containing project of interest to recover \
    iteractions
    :return: The list of project to keep

    >>> import re
    >>> f = Path(__file__).parents[2] / "data" / \
    ... "projects_filtering20dataset.txt"
    >>> res = get_good_project(f)
    >>> type(res)
    <class 'list'>
    >>> len(res) > 0
    True
    >>> "GSM1018963_GSM1018964" in res
    True
    >>> res[0:2]
    ['GSM1018963_GSM1018964', 'GSM1018961_GSM1018962']
    >>> pat = re.compile(r"[GSM0-9_]+")
    >>> sum([re.findall(pat, c)[0] == c for c in res]) == len(res)
    True
    >>> f = Path(__file__).parents[2] / "data" / \
    ... "projects_filtering20dataset.txt"
    """

    with project_file.open("r") as f:
        line = next(filterfalse(get_p, f)).replace("Keep: ", ""). \
            replace('\n', '')
    return line.split(', ')


class ConfigLoop:
    """
    Contains config loop
    """
    nb_bins = 50
    weight = 2
    global_weight = 3


def get_exon_table(control: str = "SRSF1", kind: str = 'internal',
                   mode: str = "DDX") -> Path:
    """
    Get the table file containing exons, their distance to CTCF and \
    if they are regulated
    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table
    :return: The table containing exons regulated by ``control`` and \
    DDX5/17 exons

    >>> str(get_exon_table().relative_to(Path(__file__).parents[2]))
    'result/vinity_DDX5_17/distance_CTCF_internal_n_SIPP_exons_SRSF1.csv'
    >>> str(get_exon_table(kind="last").relative_to(Path(__file__).parents[2]))
    'result/vinity_DDX5_17/terminal_exons_SRSF1.csv'
    >>> str(get_exon_table(kind="first").relative_to(Path(__file__).
    ... parents[2]))
    'result/vinity_DDX5_17/first_exons_SRSF1.csv'
    """
    if mode == "DDX":
        my_file = Path(__file__).parents[2] / 'result' / \
            'vinity_DDX5_17' / \
            f'distance_CTCF_{kind}_n_SIPP_exons_{control}.csv'
    else:
        my_file = Path(__file__).parents[2] / 'result' / \
            'vinity_DDX5_17' / \
            f'distance_CTCF_{kind}_n_{control}_exons.csv'
    if kind == "first":
        my_file = my_file.parent / f'first_exons_{control}.csv'
    elif kind == "last":
        my_file = my_file.parent / f'terminal_exons_{control}.csv'
    return my_file


def get_intermediary_extremities(sf: str) -> Path:
    """
    Get the file that is used to create first and last exons table.

    :param sf: A splicing factor
    :return: he file that is used to create first and last exons table.

    >>> str(get_intermediary_extremities('SRSF1').
    ... relative_to(Path(__file__).parents[2]))
    'result/vinity_DDX5_17/distance_CTCF_ALL_n_SIPP_exons_SRSF1.csv'
    """
    return Path(__file__).parents[2] / "result" / "vinity_DDX5_17" / \
        f"distance_CTCF_ALL_n_SIPP_exons_{sf}.csv"


class ConfigInput:
    """
    A class containing input file for creating table files with
    """
    i_internal = Path(__file__).parents[2] / "data" / \
        "internal_exon_vs_CTCF.csv"
    i_extremities = Path(__file__).parents[2] / "data" / \
        "All_Exons_vs_CTCF.csv"
    intermediary = get_intermediary_extremities


class ConfigCP:
    """
    Contains every variables of interest for this module
    """
    base = Path(__file__).parents[2]
    data = base / "data"
    gene_bed = data / "gene.bed"
    result = base / "result"
    chia_pet_folder = result / "chia-pet"
    gene_size_folder = chia_pet_folder / "gene_size_distrib"
    gene_size_file = gene_size_folder / 'gene_size_hist.pdf'
    gene_loop_file = gene_size_folder / 'gene_loop_lineplot.pdf'
    db = data / "chia_pet_database.db"
    file_datasets_filtering = data / "projects_filtering20dataset.txt"
    file_filter_ctcf = data / "projects_filtering_ctcf_cohesin.txt"
    good_project = get_good_project(file_datasets_filtering)
    good_ctcf_co = get_good_project(file_filter_ctcf)
    loop_params = ConfigLoop
    get_exon_table = get_exon_table
    gene_loop_folder = chia_pet_folder / 'gene_loop'
    gene_loop_folder_orient = chia_pet_folder / 'gene_loop_orient'
    intragenic_loop_file = gene_loop_folder / 'intragenic_loop_list.txt'
    expanded_loop_file = gene_loop_folder_orient / \
        'expanded_intragenic_loop_list.txt'


def get_loop_file(control: str = "SRSF1", kind: str = "internal",
                  global_weight: int = 0, weight: int = 0,
                  project: str = "GSM970215", ext: str = ".txt",
                  target: str = "", reg: str = "", mode: str = "DDX",
                  folder: Path = ConfigCP.gene_loop_folder) -> Path:
    """
    get the file where the data about intragenic loop will be stored

    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param target: location where the other extremities of \
    the loop is falling
    :param reg: The regulation of interest
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table
    :param folder: The output folder

    >>> tmp = get_loop_file().relative_to(Path(__file__).parents[2])
    >>> str(tmp.name)
    'loop4sipp_n_siSRSF1_internal_exons_weight-0_project-GSM970215.txt'
    >>> str(tmp.parent)
    'result/chia-pet/gene_loop'
    >>> tmp = get_loop_file(global_weight=2).relative_to(
    ... Path(__file__).parents[2])
    >>> str(tmp.name)
    'loop4sipp_n_siSRSF1_internal_exons_weight-0_global_weight-2.txt'
    >>> tmp = get_loop_file(target="_internal", reg="uniq_regulation"
    ... ).relative_to(Path(__file__).parents[2])
    >>> str(tmp.name)
    'loop4sipp_n_siSRSF1_internal_exons_weight-0_project-GSM970215_internal_up_down.txt'
    """
    nr = "_up_down" if reg == "uniq_regulation" else ""
    if mode == "DDX":
        if global_weight != 0:
            return folder / \
                f"loop4sipp_n_si{control}_{kind}_exons_weight-{weight}" \
                f"_global_weight-{global_weight}{target}{nr}{ext}"
        else:
            return folder / \
                f"loop4sipp_n_si{control}_{kind}_exons_weight-{weight}" \
                f"_project-{project}{target}{nr}{ext}"
    else:
        if global_weight != 0:
            return folder / \
                f"loop4{control}_{kind}_exons_weight-{weight}" \
                f"_global_weight-{global_weight}{target}{nr}{ext}"
        else:
            return folder / \
                f"loop4{control}_{kind}_exons_weight-{weight}" \
                f"_project-{project}{target}{nr}{ext}"


if __name__ == "__main__":
    testmod()
