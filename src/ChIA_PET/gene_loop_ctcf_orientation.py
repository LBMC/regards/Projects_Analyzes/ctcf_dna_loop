#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Get the intragenic ChIA-PET loops and check whether we can \
find convergent CTCF site located around those exons
"""

import doctest
from .intragenic_loop_watcher import logging_def, get_table, format_exon_bed, \
    find_loop, sqlite3, get_dic_co_regulated_exon, create_regulation_column, \
    get_dic_last_exon, logging, get_gene_id_col, filter_gene, \
    get_gene_size_table
from .config_ChIAPET import ConfigCP, get_loop_file
import pandas as pd
from typing import Dict, List, Tuple, Optional
import re
from pathlib import Path
import lazyparser as lp
from ..CTCF_orientation.get_convergent_sites import compute_site_orientation, \
    convergent_site
from itertools import product
import seaborn as sns
import matplotlib.pyplot as plt


def get_loop_dataframe(interaction_dic: Dict[str, List],
                       gene_dic: Dict[int, int]):
    """
   get the table of intragenic interaction

    :param interaction_dic: A dictionary showing every exons that \
    interact with another one
    :param gene_dic:  A dictionary linking each gene to the number of the \
    last exon in the gene

    >>> get_loop_dataframe({'1_1': ['1_8', '1_5'], '1_8': ['1_1', '1_5'], 
    ... '2_3': ['2_17'], '3_5': ['3_1', '3_16']}, {1: 19, 2: 17, 3: 16})
        id target_id  loop  target_first  target_internal  target_last
    0  1_1       1_8     1             0                1            0
    1  1_1       1_5     1             0                1            0
    2  1_8       1_1     1             1                0            0
    3  1_8       1_5     1             0                1            0
    4  2_3      2_17     1             0                0            1
    5  3_5       3_1     1             1                0            0
    6  3_5      3_16     1             0                0            1
    """
    dic = {"id": [], 'target_id': [], "target_first": [],
           "target_internal": [], "target_last": []}
    pattern = r'_\d+'
    for exon, oexons in interaction_dic.items():
        gene_id = int(re.sub(pattern, '', exon))
        first_exon = f"{gene_id}_1"
        last_exon = f"{gene_id}_{gene_dic[gene_id]}"
        for oexon in oexons:
            dic['id'].append(exon)
            dic['target_id'].append(oexon)
            is_first = 1 if first_exon == oexon else 0
            is_last = 1 if last_exon == oexon else 0
            dic['target_first'].append(1 if first_exon == oexon else 0)
            dic['target_last'].append(1 if last_exon == oexon else 0)
            dic['target_internal'].append(
                1 if is_first + is_last == 0 else 0)
    dic['loop'] = [1] * len(dic['id'])
    df = pd.DataFrame(dic)
    return df[['id', 'target_id', 'loop', 'target_first', 'target_internal',
               'target_last']]


def read_table_file(table_file: Path, exon_df: pd.DataFrame,
                    threshold: int) -> pd.DataFrame:
    """Read a csv file corresponding to a table containing exon \
    regulation by Sipp and another factor and turns it into a pandas \
    dataframe. Then apply create_regulation_column function to it.

    :param table_file: The table containing exons regulated by ``control`` \
    and DDX5/17 exons
    :param exon_df: A dataframe with some columns containing fasterdb exon \
    data
    :param threshold: The minimum size to consider the gene
    :return: The corresponding dataframe after applying \
    create_regulation_column
    """
    logging.debug("Load the regulation bed ...")
    df = pd.read_csv(table_file, sep='\t')
    df = create_regulation_column(df)
    df = get_gene_id_col(df, exon_df)
    df = filter_gene(df, get_gene_size_table(threshold))
    df = df[['exon_name', 'id', 'uniq_regulation', 'regulation', 'dist',
             'CTCF_hit_id', 'CTCF_hit_id_strand', 'strand']]
    return df


def merge_loop_df_and_regulated_df(reg_df: pd.DataFrame,
                                   loop_df: pd.DataFrame,
                                   df_all: pd.DataFrame) -> pd.DataFrame:
    """
    Merge the dataframe containing exons regulated by DDX5/17 or \
    by another factor or control exons and the dataframe containing \
    intragenic loops.

    :param reg_df: dataframe containing exons regulated by DDX5/17 or \
    by another factor or control exons
    :param loop_df: dataframe containing \
    intragenic loops.
    :param df_all: A dataframe containing data about all exons
    :return: The dataframe merged

    >>> import numpy as np
    >>> loop = pd.DataFrame({'id': ['1_1', '2_3', '3_5', '3_5'], 
    ... 'target_id': ['1_5', '2_17', '3_18', '3_1'],
    ... 'loop': [1, 1, 1, 1], 'target_first': [0, 0, 0, 1],
    ... 'target_internal': [1, 0, 0, 0], 'target_last': [0, 1, 1, 0]})
    >>> reg = pd.DataFrame({'exon_name': ['DSC2_1', 'DSC1_3', 'DSG1_5',
    ... 'DSG4_8'], 'id': ['1_1', '2_3', '3_5', '4_8'],
    ... 'uniq_regulation': ['ctrl', 'SiPP_DOWN', 'SRSF1_up', 'ctrl'],
    ... 'regulation': ['ctrl', 'SiPP', 'SRSF1', 'ctrl'], 
    ... 'dist': [10, 20, 30, 40],
    ... 'CTCF_hit_id': ['1:10-20', '2:300-400', '3:50-60', '4:800-900'],
    ... 'CTCF_hit_id_strand': ['+', '-', '+', np.nan],
    ... "strand": ["+", "-", "+", "-"]})
    >>> da = pd.DataFrame({'exon_name': ['DSC2_5', 'DSC1_17', 'DSG1_18',
    ... 'DSG4_8'], 'id': ['1_5', '2_17', '3_18', '4_8'],
    ... 'uniq_regulation': ['ctrl', 'SiPP_DOWN', 'SRSF1_up', 'ctrl'],
    ... 'regulation': ['ctrl', 'SiPP', 'SRSF1', 'ctrl'], 
    ... 'dist': [50, 60, 70, 80], 
    ... 'CTCF_hit_id_strand': ['-', '+', '-', np.nan],
    ... 'strand': ['+', '-', '+', '-'],
    ... 'CTCF_hit_id': ['1:50-60', '2:170-180', '3:180-190', '4:800-900']})
    >>> r = merge_loop_df_and_regulated_df(reg, loop, da)
    >>> r[["exon_name", "id", "exon_name_target", "target_id", "dist", 
    ...    "dist_target", "strand"]]
      exon_name   id exon_name_target target_id  dist  dist_target strand
    0    DSC2_1  1_1           DSC2_5       1_5    10         50.0      +
    1    DSC1_3  2_3          DSC1_17      2_17    20         60.0      -
    2    DSG1_5  3_5          DSG1_18      3_18    30         70.0      +
    3    DSG1_5  3_5              NaN       3_1    30          NaN      +
    4    DSG4_8  4_8              NaN       NaN    40          NaN      -
    >>> r[["id", "target_id", "uniq_regulation", "uniq_regulation_target"]]
        id target_id uniq_regulation uniq_regulation_target
    0  1_1       1_5            ctrl                   ctrl
    1  2_3      2_17       SiPP_DOWN              SiPP_DOWN
    2  3_5      3_18        SRSF1_up               SRSF1_up
    3  3_5       3_1        SRSF1_up                    NaN
    4  4_8       NaN            ctrl                    NaN
    >>> r.rename({"CTCF_hit_id_strand": "hit_s",
    ... "CTCF_hit_id_strand_target": "hit_st"}, axis=1
    ... )[["id", 'target_id', "hit_s", "hit_st", "CTCF_hit_id",
    ... "CTCF_hit_id_target", "strand"]]
        id target_id hit_s hit_st CTCF_hit_id CTCF_hit_id_target strand
    0  1_1       1_5     +      -     1:10-20            1:50-60      +
    1  2_3      2_17     -      +   2:300-400          2:170-180      -
    2  3_5      3_18     +      -     3:50-60          3:180-190      +
    3  3_5       3_1     +    NaN     3:50-60                NaN      +
    4  4_8       NaN   NaN    NaN   4:800-900                NaN      -
    """
    logging.debug('Merging reg_df and loop_df')
    m = reg_df.merge(loop_df, how="left")
    for col in ['loop', 'target_first', 'target_internal', 'target_last']:
        m.loc[m[col].isna(), col] = 0
        m[col] = m[col].astype(int)
    df_all.rename({"id": "target_id"}, axis=1, inplace=True)
    return m.merge(df_all, how="left", on=["target_id"],
                   suffixes=["", "_target"])


def get_first_hit(mseries: pd.Series, pattern: re.Pattern,
                  position: str = "first"
                  ) -> Tuple[Optional[str], Optional[str]]:
    """
    Return the column
    :param mseries: A row of the dataframe
    :return: The CTCF hit id and strand of the first ar last exons

    >>> d = pd.Series({"id": "1_1", "target_id": '1_5',
    ... 'CTCF_hit_id': '1:10-20', 'CTCF_hit_id_target': '1:50-60',
    ... 'CTCF_hit_id_strand': '+', 'CTCF_hit_id_strand_target': '-'})
    >>> get_first_hit(d, re.compile(r'\d+_'), 'first')
    ('1:10-20', '+')
    >>> get_first_hit(d, re.compile(r'\d+_'), 'last')
    ('1:50-60', '-')
    >>> d = pd.Series({"id": "1_5", "target_id": '1_1',
    ... 'CTCF_hit_id': '1:50-60', 'CTCF_hit_id_target': '1:10-20',
    ... 'CTCF_hit_id_strand': '-', 'CTCF_hit_id_strand_target': '+'})
    >>> get_first_hit(d, re.compile(r'\d+_'), 'first')
    ('1:10-20', '+')
    >>> get_first_hit(d, re.compile(r'\d+_'), 'last')
    ('1:50-60', '-')
    """
    pos1 = re.sub(pattern, '', mseries["id"])
    try:
        post = re.sub(pattern, '', mseries["target_id"])
    except TypeError:
        return None, None
    if pos1 == post:
        raise ValueError(
            "exon position between  exon and it's target should be different"
        )
    if (pos1 < post and position == "first") or \
            (pos1 > post and position != "first"):
        return mseries["CTCF_hit_id"], mseries["CTCF_hit_id_strand"]
    return mseries["CTCF_hit_id_target"], mseries["CTCF_hit_id_strand_target"]


def add_columns_4_convergence(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add two columns to the dataframe to identify which CTCF_hit is related \
    to the first or the last exons.

    :param df: A dataframe containing loops and their closest CTCF peaks
    :return: The dataframe df with two new column

    >>> d = pd.DataFrame({'id': ['1_1', '1_5'], 'target_id': ['1_5', '1_1'],
    ... 'CTCF_hit_id': ['1:10-20', '1:50-60'],
    ... 'CTCF_hit_id_target': ['1:50-60', '1:10-20'],
    ... 'CTCF_hit_id_strand': ['+', '-'],
    ... 'CTCF_hit_id_strand_target': ['-', '+']})
    >>> add_columns_4_convergence(d)[["CTCF_hit_id_first",
    ... "CTCF_hit_id_strand_first", "CTCF_hit_id_last",
    ... "CTCF_hit_id_strand_last"]].to_dict("list") == {
    ... 'CTCF_hit_id_first': ['1:10-20', '1:10-20'],
    ... 'CTCF_hit_id_strand_first': ['+', '+'],
    ... 'CTCF_hit_id_last': ['1:50-60', '1:50-60'],
    ... 'CTCF_hit_id_strand_last': ['-', '-']}
    True
    """
    logging.debug("Add columns to compute convergence")
    pattern = re.compile(r'\d+_')
    for loc in ["first", "last"]:
        hit, strand = [], []
        for x, y in df.apply(get_first_hit, axis=1, pattern=pattern,
                             position=loc):
            hit.append(x)
            strand.append(y)
        df[f"CTCF_hit_id_{loc}"] = hit
        df[f"CTCF_hit_id_strand_{loc}"] = strand
    return df


def filter_identical_loop(df: pd.DataFrame, target_col: str) -> pd.DataFrame:
    """
    Filter loops that are anchored on the same CTCF sites.

    :param df: A dataframe cotnaining loops and the exons at their ends \
    along with the orientation of their closest CTCF sites.
    :param target_col: The column containing the exon regulation used to \
    sort the dataframe
    :return: The same dataframe without loops anchored one the same CTCF sites.
    """
    df = df[(df["loop"] == 1) & (-df["CTCF_hit_id_target"].isna())].copy()
    df["anchor"] = df.apply(lambda x: ";".join(sorted(
        [x["CTCF_hit_id"], x["CTCF_hit_id_target"]])), axis=1)
    total_line = df.shape[0]
    df = df.sort_values([target_col, "id"], ascending=[False, True]
                        ).drop_duplicates(subset="anchor", keep="first")
    ntot = df.shape[0]
    if total_line - ntot > 0:
        logging.warning(f"{total_line - ntot} / {total_line} loops were "
                        f"anchored at the same place !")
    return df


def make_barplot(df: pd.DataFrame, outfile: Path,
                 kind: str = "internal", target_col: str = "regulation",
                 sf: str = "SRSF1", y: str = "loop"):
    sns.set(context='poster')
    ndf = df[(df[y] != 0) &
             (-df["orientation"].isna()) & (df[target_col] != "multiple") &
             (df["orientation"] != "Overlapping")].copy()
    ndf[target_col] = ndf[target_col].str.upper()
    ndf = filter_identical_loop(ndf, target_col)
    df2 = ndf[["orientation", target_col, "is_convergent"]
              ].groupby([target_col, "orientation"]).count(
    ).reset_index().rename({"is_convergent": "count"}, axis=1)
    df2["tot"] = df2.groupby(target_col)["count"].transform(sum)
    df2["prop"] = df2["count"] / df2["tot"] * 100
    g = sns.catplot(x="orientation", y="prop", data=df2, kind="bar",
                    hue=target_col, ci=None,
                    height=9, aspect=1.77)
    g.ax.set_xlabel("")
    if y == "loop":
        ty = ""
    else:
        ty = f"and a {y.replace('target_', '')} exon in the other"
    sf = f"{sf} or DDX5/17" if str(sf) == "None" else sf
    g.ax.set_ylabel("Loop proportion (%)")
    if kind == "internal":
        g.ax.set_title(f"Proportion of intragenic loops involving {kind} "
                       f"control exons or regulated by {sf}\n"
                       f"in one end {ty} with different CTCF orientation")
    else:
        g.ax.set_title(f"Proportion of intragenic loops involving {kind} "
                       f"exons in one end {ty} \n in genes containing at "
                       f"least an exon regulated by {sf} "
                       f"with different CTCF orientation")
    diag_folder = outfile.parent / "diag"
    diag_folder.mkdir(exist_ok=True, parents=True)
    df2.to_csv(str(outfile).replace('.pdf', '_bar.txt'), sep="\t",
               index=False)
    g.savefig(outfile)
    plt.close()


def get_loop(control: str = "SRSF1", kind: str = "internal",
             global_weight: int = 3, weight: int = 2,
             project: str = "GSM970215", threshold: int = 15000,
             distance: int = 10000, mode: str = "DDX",
             projects: str = "ctcf_cohesin",
             logging_level: str = "DISABLE") -> None:
    """
    Create a table and a figure of loops for exon regulated by DDX5/17 and \
    ``control`` factor.

    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param threshold: The minimum size of gene to consider
    :param distance: The minimum distance to consider an interaction
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table
    :param projects: The kind of projects from which we want to get the loops.
    :param logging_level: The level of data to display
    """
    logging_def(ConfigCP.gene_loop_folder_orient, __file__, logging_level)
    table_file = get_table(control, kind, mode)
    table_all_file = get_table(control, "ALL", mode)
    exon_df = format_exon_bed()
    df = read_table_file(table_file, exon_df, threshold)
    df_all = read_table_file(table_all_file, exon_df, threshold)
    project_list = ConfigCP.good_ctcf_co if projects == "ctcf_cohesin" \
        else ConfigCP.good_project
    arr_interaction = find_loop(sqlite3.connect(ConfigCP.db), weight,
                                global_weight, project, distance,
                                good_project=project_list)
    dic_interaction = get_dic_co_regulated_exon(arr_interaction)
    dic_last_exon = get_dic_last_exon(exon_df)
    df_loop = get_loop_dataframe(dic_interaction, dic_last_exon)
    df_loop = merge_loop_df_and_regulated_df(df, df_loop, df_all)
    df_loop = add_columns_4_convergence(df_loop)
    logging.debug("Compute convergence")
    final_df = convergent_site(compute_site_orientation(df_loop))
    final_df.drop(["CTCF_hit_id_first", "CTCF_hit_id_last",
                   "CTCF_hit_id_strand_first", "CTCF_hit_id_strand_last"],
                  axis=1, inplace=True)
    outfile = get_loop_file(control, kind, global_weight, weight, project,
                            mode, folder=ConfigCP.gene_loop_folder_orient)
    outfile = outfile.parent / f"{outfile.stem}_{projects}.txt"
    outfile.parent.mkdir(parents=True, exist_ok=True)
    logging.debug(f'Saving result file into {outfile}')
    final_df.to_csv(outfile, sep="\t", index=False)
    logging.debug("Creating figures ...")
    target_cols1 = ["regulation"]
    target_cols2 = ["target_first", "target_last"]
    target_cols2 = [t for t in target_cols2 if t != f"target_{kind}"]
    prod = product(target_cols1, target_cols2)
    for t1, t2 in prod:
        logging.debug(f"    ... with {t1} and {t2} parameters")
        outfile = get_loop_file(control, kind, global_weight, weight, project,
                                ".pdf", "_" + t2, t1,
                                folder=ConfigCP.gene_loop_folder_orient)
        outfile = outfile.parent / f"{outfile.stem}_{projects}.pdf"
        make_barplot(final_df, outfile, kind, t1, control, t2)


@lp.parse(projects=["ctcf_cohesin", "20_datasets"])
def launcher(control: str = "SRSF1", kind: str = "internal",
             global_weight: int = 3, weight: int = 2,
             project: str = "GSM970215", threshold: int = 15000,
             distance: int = 10000, mode: str = "DDX",
             projects: str = "ctcf_cohesin",
             logging_level: str = "DISABLE"):
    """
    Launch figure in this submodule

    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param threshold: The minimum size of gene to consider
    :param distance: The minimum required distance to consider an \
    interaction between exons
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table (default DDX)
    :param projects: The kind of projects from which we want to get the loops.
    :param logging_level: The level of data to display
    """
    ConfigCP.gene_loop_folder_orient.mkdir(exist_ok=True)
    get_loop(control, kind, global_weight, weight, project, threshold,
             distance, mode, projects, logging_level)


if __name__ == "__main__":
    # doctest.testmod()
    launcher()
