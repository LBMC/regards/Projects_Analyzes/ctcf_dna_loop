#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: From a bed file containing FasterrDB gene, plot an histogram
showing the distribution of gene size
"""

from pathlib import Path
from typing import List
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from .config_ChIAPET import ConfigCP
from doctest import testmod
import numpy as np


def get_gene_size(gene_bed: Path) -> List[int]:
    """

    :param gene_bed: The bed containing fasterDB gene
    :return: The list of gene size

    >>> res = get_gene_size(ConfigCP.gene_bed)
    >>> res[0:5]
    [36445, 33629, 39344, 38130, 8187]
    >>> len(res)
    19426
    """
    df = pd.read_csv(gene_bed, sep="\t")
    size = df["end"] - df["start"]
    return list(size)


def make_hist(gene_size: List[int]):
    """
    Create the histogram showing the distribution of gene size.

    :param gene_size: The list of fasterDB gene size
    """
    ConfigCP.gene_size_file.parent.mkdir(parents=True, exist_ok=True)
    sns.set(context="poster")
    plt.figure(figsize=(20, 12))
    tmp = np.asarray(gene_size)
    sns.distplot(tmp[tmp <= np.quantile(tmp, 0.9)], kde=False)
    plt.xlabel = "gene_size"
    plt.savefig(ConfigCP.gene_size_file)


def create_figure():
    """
    Create gene size histogram
    """
    gene_size = get_gene_size(ConfigCP.gene_bed)
    make_hist(gene_size)
    tmp = np.asarray(gene_size)
    print(len(tmp[tmp < 500000]))
    print(len(tmp[tmp > 500000]))
    print(np.quantile(tmp, 0.9))


if __name__ == "__main__":
    testmod()
