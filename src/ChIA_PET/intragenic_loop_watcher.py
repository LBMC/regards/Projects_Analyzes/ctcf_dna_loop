#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to check if a list of exons \
regulated by a factor contains an intragenic loop and where this loop \
falls
"""

from .config_ChIAPET import ConfigCP, ConfigInput, get_loop_file
from pathlib import Path
from ..vicinity_DDX5_17.create_terminal_exon_file import \
    create_exon_first_or_last_table
from ..vicinity_DDX5_17.merge_ctrl_and_sipp import main_merge
import doctest
import pandas as pd
from ..vicinity_DDX5_17.create_terminal_exon_file import format_exon_bed
from typing import List, Dict
import sqlite3
import numpy as np
import re
from ..logging_conf import logging_def
import logging
import seaborn as sns
from rpy2.robjects import r, pandas2ri
from ..vicinity_DDX5_17.statistical_analysis import get_stat_figure
import matplotlib.pyplot as plt
from ..vicinity_DDX5_17.stat_annot import add_stat_annotation
from itertools import product


def get_table(control: str = "SRSF1", kind: str = "internal",
              mode: str = "DDX") -> Path:
    """
    Get the table file containing exons, their distance to CTCF and \
    if they are regulated and creates it if it doesn't exist.

    :param control: The control factor of interest
    :param kind: The kind of exon we want to analyse
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table
    :return: The table containing exons regulated by ``control`` and \
    DDX5/17 exons
    >>> res = get_table().relative_to(Path(__file__).parents[2])
    >>> str(res)
    'result/vinity_DDX5_17/distance_CTCF_internal_n_SIPP_exons_SRSF1.csv'
    >>> res.is_file()
    True
    >>> res = get_table("SRSF1", "first").relative_to(
    ... Path(__file__).parents[2])
    >>> str(res)
    'result/vinity_DDX5_17/first_exons_SRSF1.csv'
    >>> res.is_file()
    True
    """
    table_file = ConfigCP.get_exon_table(control, kind, mode)
    if not table_file.is_file():
        if mode != "DDX":
            raise FileNotFoundError(f"File {table_file} not found !")
        msg = "file containing exons, their regulation " \
              "by DDX5/17 and another factor and their distance to CTCF"
        if kind == "internal":
            logging.debug("Recovering " + msg)
            main_merge(table_file, ConfigInput.i_internal, factor=control,
                       remove_first_and_last=True)
        else:
            logging.debug("Creating " + msg)
            tmp_file = ConfigInput.intermediary(control)
            main_merge(tmp_file, ConfigInput.i_extremities, factor=control)
            create_exon_first_or_last_table(tmp_file, table_file,
                                            kind=kind)
    return table_file


def create_regulation_column(df: pd.DataFrame) -> pd.DataFrame:
    """
    From a table containing exons, and their regulation, check if the \
    column uniq-regulation exist. If it doest, create the corresponding column.

    :param df: table containing exons, and their regulation
    :return: The table with uniq_regulation column

    >>> df = pd.DataFrame({'new_group': ['ctrl', 'ctrl', 'SiPP_DOWN',
    ... 'SiPP_UP', 'SRSF1_down', 'SRSF1_up'], 'regulation': [None] * 6})
    >>> create_regulation_column(df)
      uniq_regulation regulation
    0            ctrl       ctrl
    1            ctrl       ctrl
    2       SiPP_DOWN       SiPP
    3         SiPP_UP       SiPP
    4      SRSF1_down      SRSF1
    5        SRSF1_up      SRSF1
    >>> df = pd.DataFrame({'uniq_regulation': ['ctrl', 'ctrl', 'SiPP_DOWN',
    ... 'SiPP_UP', 'SRSF1_down', 'SRSF1_up'], 'regulation': ['ctrl', 'ctrl',
    ... 'SiPP', 'SiPP', 'SRSF1', 'SRSF1']})
    >>> create_regulation_column(df)
      uniq_regulation regulation
    0            ctrl       ctrl
    1            ctrl       ctrl
    2       SiPP_DOWN       SiPP
    3         SiPP_UP       SiPP
    4      SRSF1_down      SRSF1
    5        SRSF1_up      SRSF1
    """
    logging.debug("Creation of regulation column ...")
    if "uniq_regulation" in df.columns:
        return df
    if "regulation" in df.columns:
        df.drop('regulation', inplace=True, axis=1)
    df.rename({"new_group": "uniq_regulation"}, axis=1, inplace=True)
    df['regulation'] = df['uniq_regulation'].str.replace(
        r"_down|_up|_DOWN|_UP", "")
    return df


def get_gene_id_col(df: pd.DataFrame, exon_df: pd.DataFrame) -> pd.DataFrame:
    """
    add gene_name, exon_pos , id and gene_id column to \
    ``df`` if it doesn't already contain them.

    :param df: Dataframe containing exons regulated by DDX5/17 and \
    anopther factor
    :param exon_df: A dataframe containing fasterdb exons data
    :return: The dataframe with the new columns if it doesn't have them

    >>> df = pd.DataFrame({"exon_name": ['DSC2_1', 'DSC1_1', 'DSG1_1',
    ... 'DSG4_1', 'TPT1_1', 'AC011260.1_1', 'DCC_1', 'SLC25A30_1', 'DSG3_1'],
    ... "exon_id": ['18:28681866-28682388', '18:28742495-28742819',
    ... '18:28898051-28898311', '18:28956740-28956922',
    ... '13:45915177-45915347', '18:48918412-48918627',
    ... '18:49866542-49867248', '13:45992435-45992516',
    ... '18:29027732-29027888']})
    >>> get_gene_id_col(df, format_exon_bed()).drop('exon_id', axis=1)
          exon_name   gene_name  exon_pos    id  gene_id
    0        DSC2_1        DSC2         1   1_1        1
    1        DSC1_1        DSC1         1   2_1        2
    2        DSG1_1        DSG1         1   3_1        3
    3        DSG4_1        DSG4         1   4_1        4
    4        TPT1_1        TPT1         1   6_1        6
    5  AC011260.1_1  AC011260.1         1   7_1        7
    6         DCC_1         DCC         1   8_1        8
    7    SLC25A30_1    SLC25A30         1   9_1        9
    8        DSG3_1        DSG3         1  10_1       10

    """
    logging.debug("Recovering gene_name, exon_pos, id and gene columns")
    if "gene_id" in df.columns:
        return df
    else:
        df['gene_name'] = df['exon_name'].str.replace(r'_\d+', '')
        df['exon_pos'] = df['exon_name'].str.extract(r'_(\d+)').astype(int)
        df = df.merge(exon_df, how="left",
                      on=["exon_id", "gene_name", "exon_pos"])
        df = df[-df['id'].isna()]
        df['gene_id'] = df['gene_id'].astype(int)
        df.drop('last_fasterDB_exon', inplace=True, axis=1)
    return df


def filter_gene(df: pd.DataFrame, gene_list: List[int]) -> pd.DataFrame:
    """
    From a dataframe contaning a filter_genecolumn gene_id, keep only values \
    present in ``gene_list``.

    :param df: A dataframe contaning exons and their regulation \
    by a factor and DDX5/17
    :return: The same dataframe without genes not in gene_list

    >>> d = pd.DataFrame({"exon_name": ['DSC2_1', 'DSC1_1', 'DSG1_1', 'DSG4_1',
    ... 'TPT1_1', 'AC011260.1_1', 'DCC_1', 'SLC25A30_1', 'DSG3_1'],
    ... "gene_name": ['DSC2', 'DSC1', 'DSG1', 'DSG4', 'TPT1',
    ... 'AC011260.1', 'DCC', 'SLC25A30', 'DSG3'], "exon_pos": [1] * 9,
    ... 'id': ['1_1', '2_1', '3_1', '4_1', '6_1', '7_1', '8_1', '9_1',
    ... '10_1'], 'gene_id': [ 1,  2,  3,  4,  6,  7,  8,  9, 10]})
    >>> filter_gene(d, [1, 2, 3])
      exon_name gene_name  exon_pos   id  gene_id
    0    DSC2_1      DSC2         1  1_1        1
    1    DSC1_1      DSC1         1  2_1        2
    2    DSG1_1      DSG1         1  3_1        3
    """
    return df[df['gene_id'].isin(gene_list)]


def get_gene_size_table(threshold: int) -> List[int]:
    """
    Return a list of gene with a size greater or equals to \
    ``threshold``
    :param threshold: The minimum size to consider the gene
    :return: List of gene with a size > threshold

    >>> res = get_gene_size_table(15000)
    >>> res[0:5]
    [1, 2, 3, 4, 7]
    >>> len(res)
    11820
    """
    df = pd.read_csv(ConfigCP.gene_bed, sep="\t")
    df['size'] = df['end'] - df['start']
    return list(df.loc[df['size'] >= threshold, "id"].values)


def read_table_file(table_file: Path, exon_df: pd.DataFrame,
                    threshold: int) -> pd.DataFrame:
    """Read a csv file corresponding to a table containing exon \
    regulation by Sipp and another factor and turns it into a pandas \
    dataframe. Then apply create_regulation_column function to it.

    :param table_file: The table containing exons regulated by ``control`` \
    and DDX5/17 exons
    :param exon_df: A dataframe with some columns containing fasterdb exon \
    data
    :param threshold: The minimum size to consider the gene
    :return: The corresponding dataframe after applying \
    create_regulation_column
    """
    logging.debug("Load the regulation bed ...")
    df = pd.read_csv(table_file, sep='\t')
    df = create_regulation_column(df)
    df = get_gene_id_col(df, exon_df)
    df = filter_gene(df, get_gene_size_table(threshold))
    df = df[['exon_name', 'id', 'uniq_regulation', 'regulation']]
    return df


def find_loop(cnx: sqlite3.Connection, weight: int,
              global_weight: int, project: str, distance: int,
              good_project: Path = ConfigCP.good_project) -> np.array:
    """
    Get the list of intragenic loop

    :param cnx: connexion to ChIA-PET database
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param distance: The miminum distance required to consider an interaction
    :return: The list of co-localisation of interest

    >>> r = find_loop(sqlite3.connect(ConfigCP.db),
    ... 1, 1, '', 1)
    >>> r[0:3]
    array([['10000_1', '10000_4'],
           ['10000_1', '10000_5'],
           ['10000_1', '10000_6']], dtype='<U9')
    """
    logging.debug("Recovering intragenic loops")
    if global_weight == 0:
        query = f"""SELECT t1.exon1, t1.exon2
                    FROM cin_exon_interaction t1, cin_exon t2, cin_exon t3
                    WHERE t1.weight >= {weight}
                    AND id_project = '{project}' 
                    AND t1.exon1 = t2.id
                    AND t1.distance IS NOT NULL
                    AND t1.distance >= {distance}
                    AND t1.exon2 = t3.id
                    AND t2.id_gene = t3.id_gene"""
    else:
        query = f"""SELECT t1.exon1, t1.exon2
                    FROM cin_exon_interaction t1, cin_exon t2, cin_exon t3
                    WHERE t1.weight >= {weight}
                    AND t1.distance IS NOT NULL
                    AND t1.distance >= {distance}
                    AND t1.exon1 = t2.id
                    AND t1.exon2 = t3.id
                    AND t2.id_gene = t3.id_gene
                    AND t1.id_project IN {tuple(good_project)}
                    GROUP BY exon1, exon2
                    HAVING COUNT(*) >= {global_weight}"""
    c = cnx.cursor()
    c.execute(query)
    return np.asarray(c.fetchall())


def get_dic_co_regulated_exon(arr_interaction: np.array):
    """
    Get all exon interacting with each other.

    :param arr_interaction: List of co-regulated exons.
    :return: Dictionary containing all regulated exons

    >>> a = np.array([['1_1', '2_1'], ['1_1', '3_1'], ['3_1', '2_1']])
    >>> get_dic_co_regulated_exon(a)
    {'1_1': ['2_1', '3_1'], '2_1': ['1_1', '3_1'], '3_1': ['1_1', '2_1']}
    """
    logging.debug('Creating the dictionary saying for an exon all '
                  'the exon that co-localise with it')
    d = {}
    for exon1, exon2 in arr_interaction:
        for e1, e2 in [(exon1, exon2), (exon2, exon1)]:
            if e1 not in d:
                d[e1] = [e2]
            else:
                d[e1] += [e2]
    return d


def get_loop_dataframe(interaction_dic: Dict[str, List],
                       gene_dic: Dict[int, int]):
    """
   get the table of intragenic interaction

    :param interaction_dic: A dictionary showing every exons that \
    interact with another one
    :param gene_dic:  A dictionary linking each gene to the number of the \
    last exon in the gene

    >>> get_loop_dataframe({'1_1': ['1_8', '1_5'], '2_3': ['2_17'],
    ... '3_5': ['3_1', '3_16']}, {1: 19, 2: 17, 3: 16})
        id  loop  target_first  target_internal  target_last
    0  1_1     1             0                1            0
    1  2_3     1             0                0            1
    2  3_5     1             1                0            1
    """
    dic = {"id": [], "target_first": [], "target_internal": [],
           "target_last": []}
    pattern = r'_\d+'
    for exon in interaction_dic:
        oexon = interaction_dic[exon]
        gene_id = int(re.sub(pattern, '', exon))
        first_exon = f"{gene_id}_1"
        last_exon = f"{gene_id}_{gene_dic[gene_id]}"
        internal = [e for e in oexon if e not in (first_exon, last_exon)]
        dic['id'].append(exon)
        dic['target_first'].append(1 if first_exon in oexon else 0)
        dic['target_last'].append(1 if last_exon in oexon else 0)
        dic['target_internal'].append(1 if internal else 0)
    dic['loop'] = [1] * len(dic['id'])
    df = pd.DataFrame(dic)
    return df[['id', 'loop', 'target_first', 'target_internal',
               'target_last']]


def save_loop_dataframe(interaction_dic: Dict[str, List],
                        gene_dic: Dict[int, int]):
    """
    Save the table of intragenic interaction

    :param interaction_dic: A dictionary showing every exons that interact \
    with another one
    :param gene_dic:  A dictionary linking each gene to the number of the \
    last exon in the gene
    """
    if ConfigCP.intragenic_loop_file.is_file():
        logging.debug('Recovering loops dataframe for every fasterDB exons')
        df = pd.read_csv(ConfigCP.intragenic_loop_file, sep="\t")
    else:
        logging.debug('Finding loops dataframe for every fasterDB exons')
        df = get_loop_dataframe(interaction_dic, gene_dic)
    return df


def get_dic_last_exon(exon_df: pd.DataFrame) -> Dict[int, int]:
    """


    :param exon_df: A dataframe containing data about fasterdb exons
    :return: A dictionary linking each gene to the number of the last exon \
    in the gene

    >>> df = pd.DataFrame({"gene_id": ['1', '2', '8', '1'],
    ... "last_fasterDB_exon": [10, 20, 30, 10]})
    >>> get_dic_last_exon(df)
    {1: 10, 2: 20, 8: 30}
    """
    logging.debug('Creating the dictionary indicating the number of exon '
                  'each gene contains')
    df = exon_df[['gene_id', 'last_fasterDB_exon']]
    df.index = exon_df['gene_id'].astype(int)
    df = df.drop_duplicates()
    df = df[['last_fasterDB_exon']]
    res = df.to_dict()
    return res['last_fasterDB_exon']


def merge_loop_df_and_regulated_df(reg_df: pd.DataFrame,
                                   loop_df: pd.DataFrame) -> pd.DataFrame:
    """
    Merge the dataframe containing exons regulated by DDX5/17 or \
    by another factor or control exons and the dataframe containing \
    intragenic loops.

    :param reg_df: dataframe containing exons regulated by DDX5/17 or \
    by another factor or control exons
    :param loop_df: dataframe containing \
    intragenic loops.
    :return: The dataframe merged

    >>> loop = pd.DataFrame({'id': ['1_1', '2_3', '3_5'],
    ... 'loop': [1, 1, 1], 'target_first': [0, 0, 1],
    ... 'target_internal': [1, 0, 0], 'target_last': [0, 1, 1]})
    >>> reg = pd.DataFrame({'exon_name': ['DSC2_1', 'DSC1_3', 'DSG1_5',
    ... 'DSG4_8'], 'id': ['1_1', '2_3', '3_5', '4_8'],
    ... 'uniq_regulation': ['ctrl', 'SiPP_DOWN', 'SRSF1_up', 'ctrl'],
    ... 'regulation': ['ctrl', 'SiPP', 'SRSF1', 'ctrl']})
    >>> r = merge_loop_df_and_regulated_df(reg, loop)
    >>> r[['exon_name', 'regulation', 'loop', 'target_first',
    ... 'target_internal', 'target_last']]
      exon_name regulation  loop  target_first  target_internal  target_last
    0    DSC2_1       ctrl     1             0                1            0
    1    DSC1_3       SiPP     1             0                0            1
    2    DSG1_5      SRSF1     1             1                0            1
    3    DSG4_8       ctrl     0             0                0            0

    """
    logging.debug('Merging reg_df and loop_df')
    m = reg_df.merge(loop_df, how="left").fillna(0)
    for col in ['loop', 'target_first', 'target_internal', 'target_last']:
        m[col] = m[col].astype(int)
    return m


def glm_binomial_stat(df: pd.DataFrame, output: Path, partial_name: str,
                      y: str, x: str) -> pd.DataFrame:
    """Perform a glm binomial test on ``df``

    :param df: A dataframe of exons and the distance to the closest CTCF \
    binding site
    :param output: The result folder
    :param partial_name: thee figure partial name
    :param y: The target variable
    :param x: The explanatory variables
    :return The tukey's test results
    """
    pandas2ri.activate()
    stat_s = r("""
    require("DHARMa")
    require("emmeans")
    function(data, output, partial_name){
        data$%s <- as.factor(data$%s)
        mod <- glm(%s ~ %s, family=binomial(link='logit'), data=data)
        tryCatch({
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(output, "/dignostics_", partial_name, ".png"))
            plot(simulationOutput)
            dev.off()
        }, error = function(cond){
            print('Error on diag figure')
            print(cond)})
        comp <- emmeans(mod, ~ %s)
        tab <- as.data.frame(pairs(comp))
        tab <- tab[, c('contrast', 'estimate', 'SE', 'z.ratio', 'p.value')]
        colnames(tab) <- c("Comparison", "Estimate", "Std.Error", 
                           "z.ratio", "p.value")
        return(tab)
    }
    """ % (x, x, y, x, x))
    return stat_s(df, str(output), partial_name)


def make_barplot(df: pd.DataFrame, outfile: Path,
                 target_col: str = "regulation", kind: str = "internal",
                 sf: str = "SRSF1", y: str = "loop"):
    sns.set(context='poster')
    ndf = df[df[target_col] != "multiple"].copy()
    df2 = ndf[[target_col, y]].groupby(target_col). \
        agg(["count", "sum"]).reset_index()
    df2.columns = [target_col] + list(df2.columns.get_level_values(1))[1:]
    df2["prop"] = df2["sum"] / (df2["count"]) * 100
    g = sns.catplot(x=target_col, y="prop", data=df2, kind="bar", ci=None,
                    height=9, aspect=1.77)
    g.ax.set_xlabel("")
    if y == "loop":
        ty = ""
    else:
        ty = f"with the other extremity falling in a " \
             f"{y.replace('target_', '')} exon"
    if kind == "internal":
        g.ax.set_ylabel("Exon proportion (%)")
        g.ax.set_title(f"Proportion of control exons and exons regulated by "
                       f"{sf} and DDX5/17\nthat are at the basis of an "
                       f"intragenic loop {ty}")
    else:
        g.ax.set_ylabel("Gene proportion (%)")
        g.ax.set_title(f"Proportion of control genes and genes containing an "
                       f"exon regulated by {sf} and DDX5/17\n having its "
                       f"{kind} exon at the basis of an intragenic loop {ty}")
    diag_folder = outfile.parent / "diag"
    diag_folder.mkdir(exist_ok=True, parents=True)
    df_stat = glm_binomial_stat(ndf, diag_folder,
                                outfile.name.replace('.pdf', '_diag.pdf'), y,
                                target_col)
    df_stat.to_csv(str(outfile).replace('.pdf', '_stat.txt'), sep="\t",
                   index=False)
    df2.to_csv(str(outfile).replace('.pdf', '_bar.txt'), sep="\t",
               index=False)
    add_stat_annotation(g.ax, data=df2, x=target_col, y="prop",
                        loc='inside',
                        boxPairList=get_stat_figure(df_stat),
                        linewidth=1, fontsize="xx-small",
                        lineYOffsetToBoxAxesCoord=0.02)
    g.savefig(outfile)
    plt.close()


def get_loop(control: str = "SRSF1", kind: str = "internal",
             global_weight: int = 3, weight: int = 2,
             project: str = "GSM970215", threshold: int = 15000,
             distance: int = 10000, mode: str = "DDX",
             logging_level: str = "DISABLE") -> None:
    """
    Create a table and a figure of loops for exon regulated by DDX5/17 and \
    ``control`` factor.

    :param control: The control factor of interest
    :param kind:The kind of exon we want to analyse
    :param weight: The minimum weight of the interaction to consider
    :param global_weight: The minimum number of projects in which \
    the interaction must be present to be considered
    :param project: The name of the project of interest
    :param threshold: The minimum size of gene to consider
    :param distance: The minimum distance to consider an interaction
    :param mode: The mode of loop analysis either 'DDX' to \
    seek table with exons regulated by DDX or another splicing factor \
    or 'custom' to select a customized table
    :param logging_level: The level of data to display
    """
    logging_def(ConfigCP.gene_loop_folder, __file__, logging_level)
    table_file = get_table(control, kind, mode)
    exon_df = format_exon_bed()
    df = read_table_file(table_file, exon_df, threshold)
    arr_interaction = find_loop(sqlite3.connect(ConfigCP.db), weight,
                                global_weight, project, distance)
    dic_interaction = get_dic_co_regulated_exon(arr_interaction)
    dic_last_exon = get_dic_last_exon(exon_df)
    df_loop = save_loop_dataframe(dic_interaction, dic_last_exon)
    final_df = merge_loop_df_and_regulated_df(df, df_loop)
    outfile = get_loop_file(control, kind, global_weight, weight, project,
                            mode)
    outfile.parent.mkdir(parents=True, exist_ok=True)
    logging.debug('Saving result file')
    final_df.to_csv(outfile, sep="\t", index=False)
    logging.debug("Creating figures ...")
    target_cols1 = ["regulation", "uniq_regulation"]
    target_cols2 = ['loop', "target_first", "target_internal", "target_last"]
    prod = product(target_cols1, target_cols2)
    for t1, t2 in prod:
        logging.debug(f"    ... with {t1} and {t2} parameters")
        outfile = get_loop_file(control, kind, global_weight, weight, project,
                                ".pdf", "_" + t2, t1)
        make_barplot(final_df, outfile, t1, kind, control,
                     t2)


if __name__ == "__main__":
    doctest.testmod()
