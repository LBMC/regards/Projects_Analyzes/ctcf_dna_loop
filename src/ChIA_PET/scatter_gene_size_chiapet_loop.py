#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a figure showing \
the percentage of gene having an intern loop in function of their size.
"""

import numpy as np
from .gene_size_distribution import get_gene_size
from .config_ChIAPET import ConfigCP
import sqlite3
from typing import List
from doctest import testmod
import pandas as pd
from tqdm import tqdm
import seaborn as sns
import matplotlib.pyplot as plt


def get_bin(nb_bin: int = 30) -> List[int]:
    """

    :param nb_bin: The number of bin we want in the final figure
    :return: An array of float indicating the size bin that will be \
    used in the figure

    >>> res = get_bin(10)
    >>> len(res)
    10
    >>> res
    [1, 16453, 32905, 49356, 65808, 82260, 98712, 115164, 131616, 148068]
    """
    gene_size = np.asarray(get_gene_size(ConfigCP.gene_bed))
    return list(map(int, np.round(np.linspace(1, np.quantile(gene_size, 0.9),
                                              nb_bin), 0)))


def get_gene_with_interest_size(cnx: sqlite3.Connection, min_size: int,
                                max_size: int) -> List[int]:
    """
    Return the list of gene having a size that is greater or equals to \
    min_size and lower to max_size.

    :param cnx: Connection to chia-pet database
    :param min_size: The minimum size the gene must have to be considered
    :param max_size: The maximum size the gene must have to be considered
    :return: The list of gene having a size s greater or equals to \
    min_size and lower to max_size.

    >>> res = get_gene_with_interest_size(sqlite3.connect(ConfigCP.db), 100,
    ... 200)
    >>> res
    [1271, 3976, 14570, 14949]
    """
    c = cnx.cursor()
    query = f"""SELECT id
               FROM cin_gene
               WHERE stop - start >= {min_size}
               AND stop - start < {max_size}
             """
    c.execute(query)
    return list(np.asarray(c.fetchall()).flatten())


def get_gene_with_intra_loop(cnx: sqlite3.Connection, gene_list: List[int],
                             weight: int, global_weight: int,
                             distance: int) -> float:
    """
    Get the percentage of gene with an intra loop in the gene present in \
    gene_list.

    :param cnx: Connection to chia-pet database
    :param gene_list: List of gene to consider
    :param weight: The weight of the interaction to consider
    :param global_weight: The number of project in wich the interaction must \
    appear
    :param distance: The minimum distance to consider the interaction
    :return: The percentage of gene with the genes persent in gene)_list

    >>> res = get_gene_with_intra_loop(sqlite3.connect(ConfigCP.db),
    ... [1, 2, 100, 3976], 1, 1, -1)
    >>> res
    75.0
    """
    c = cnx.cursor()
    query = f"""SELECT DISTINCT t3.id_gene
                FROM cin_exon_interaction t1, cin_exon t2, cin_exon t3
                WHERE t1.weight >= {weight}
                AND t1.id_project IN {tuple(ConfigCP.good_project)}
                AND t1.exon1 = t2.id
                AND t1.distance IS NOT NULL 
                AND t1.distance >= {distance}
                AND t1.exon2 = t3.id
                AND t2.id_gene = t3.id_gene
                AND t3.id_gene IN {tuple(gene_list)}
                GROUP BY t1.exon1, t1.exon2
                HAVING COUNT(*) >= {global_weight}
             """
    c.execute(query)
    return round(len(np.asarray(c.fetchall()).flatten()) /
                 len(gene_list) * 100, 2)


def build_dataframe(nb_bins: int, cnx: sqlite3.Connection, weight: int,
                    global_weight: int, distance: int) -> pd.DataFrame:
    """
    Build a dataframe to create the scatter plot of interest.

    :param nb_bins: The number of bin we want to create
    :param cnx: Connextion to chia-pet database
    :param weight:  The weight of the co-localisation to consider
    :param global_weight: The number of project in wich the interaction must \
    appear
    :param distance: The minimum distance to consider the interaction
    :return: The dataframe with the wanted content

    >>> df = build_dataframe(10, sqlite3.connect(ConfigCP.db), 5, 5, -1)
    >>> df
             gene_size  gene_with_intra_loop  nb_gene
    0        [1,16453[                  1.09     8017
    1    [16453,32905[                  3.16     3448
    2    [32905,49356[                  3.76     1968
    3    [49356,65808[                  3.65     1316
    4    [65808,82260[                  3.71      863
    5    [82260,98712[                  3.64      632
    6   [98712,115164[                  2.35      510
    7  [115164,131616[                  4.31      418
    8  [131616,148068[                  4.18      311
    """
    d = {'gene_size': [], "gene_with_intra_loop": [], 'nb_gene': []}
    my_bins = get_bin(nb_bins)
    pbar = tqdm(my_bins[:-1])
    for k, min_v in enumerate(pbar):
        max_v = my_bins[k + 1]
        interval = f'[{min_v},{max_v}['
        pbar.set_description(f'working on {interval}')
        d['gene_size'].append(interval)
        gene_list = get_gene_with_interest_size(cnx, min_v, max_v)
        d['nb_gene'].append(len(gene_list))
        percent = get_gene_with_intra_loop(cnx, gene_list, weight,
                                           global_weight, distance)
        d['gene_with_intra_loop'].append(percent)
    return pd.DataFrame(d)


def create_lineplot(df: pd.DataFrame, weight: int, global_weight: int,
                    distance: int):
    """
    Create a lineplot.

    :param df: The dataframe containing all the data we need
    :param weight: The weight of interest
    :param global_weight: The number of project in wich the interaction must \
    appear
    :param distance: the minimum distance to consider the interaction
    """
    sns.set()
    sns.set_context('poster')
    plt.figure(figsize=(20, 12))
    sns.lineplot(x="gene_size", y="gene_with_intra_loop", data=df,
                 color="blue", sort=False)
    plt.xticks(rotation=90)
    plt.subplots_adjust(bottom=0.25)
    ax2 = plt.twinx()
    sns.lineplot(x="gene_size", y="nb_gene", color="red", data=df, ax=ax2,
                 sort=False)
    plt.title(f"weight: {weight} - global_weight: {global_weight} - "
              f"distance: {distance} nt")
    plt.savefig(ConfigCP.gene_loop_file)


def lineplot_maker(distance: int):
    """
    Create the lineplot figure.

    :param distance: The minimum distance required to consider interaction \
    within gene
    """
    df = build_dataframe(ConfigCP.loop_params.nb_bins,
                         sqlite3.connect(ConfigCP.db),
                         ConfigCP.loop_params.weight,
                         ConfigCP.loop_params.global_weight, distance)
    create_lineplot(df, ConfigCP.loop_params.weight,
                    ConfigCP.loop_params.global_weight, distance)


if __name__ == "__main__":
    testmod()
