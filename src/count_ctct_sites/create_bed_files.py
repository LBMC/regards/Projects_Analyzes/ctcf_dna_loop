#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: the goal of this script is to create bed files containing \
only genes having at least one exon regulated by DDX5/17 or control exons and \
check the average number of CTCF sites located in those genes
"""
import doctest
from pathlib import Path
import pandas as pd
from typing import List, Tuple
import subprocess as sp
import seaborn as sns
from rpy2.robjects import r, pandas2ri
from ..vicinity_DDX5_17.stat_annot import add_stat_annotation


def load_exon_file(exon_file: Path) -> pd.DataFrame:
    """
    Load a file containing exons regulated or not by DDX5/17 \
    and their distance to CTCF site.

    :param exon_file: A file containing exons
    :return: the file loaded as a dataframe
    """
    return pd.read_csv(exon_file, sep="\t")


def create_bed_file(df: pd.DataFrame) -> pd.DataFrame:
    """
    Turn a dataframe containing exons its regulation, and it's distance \
     to the closest CTCF file to a bed file.

    :param df: dataframe containing exons its regulation, and it's distance \
     to the closest CTCF file
    :return: A bed file

    >>> di = pd.DataFrame({'exon_name': ['A1BG_1', 'A1BG_2', 'A1BG_3',
    ... 'A1BG_4'],
    ... 'dist': [-5784, 5471, 5359, 4995],
    ... 'exon_id': ['19:58867449-58867591', '19:58864770-58864865',
    ...  '19:58864658-58864693', '19:58864294-58864563'],
    ... 'CTCF_hit_id': ['19:58873375-58873839', '19:58859059-58859299',
    ...  '19:58859059-58859299', '19:58859059-58859299'],
    ... 'infileTarget': ['CTCF.bed', 'CTCF.bed', 'CTCF.bed', 'CTCF.bed'],
    ... 'group': ['First', 'CE', 'CE', 'siPP_DOWN'],
    ... 'strand': ['-', '-', '-', '-']})
    >>> create_bed_file(di)
      chr     start      stop exon_name group strand
    0  19  58867448  58867591    A1BG_1  CTRL      -
    1  19  58864769  58864865    A1BG_2  CTRL      -
    2  19  58864657  58864693    A1BG_3  CTRL      -
    3  19  58864293  58864563    A1BG_4  siPP      -
    """
    df = df[["exon_name", "exon_id", "group", "strand"]]
    coord = pd.DataFrame(df["exon_id"].str.split(r":|-").to_list(),
                         columns=["chr", "start", "stop"])
    coord["start"] = coord["start"].astype(int)
    coord["stop"] = coord["stop"].astype(int)
    coord["start"] -= 1
    df = pd.concat([df, coord], axis=1)
    df["group"] = df["group"].apply(lambda x: "siPP"
                                    if "siPP" in x else "CTRL")
    return df[["chr", "start", "stop", "exon_name", "group", "strand"]]


def find_ddx_geneid(bed_df: pd.DataFrame) -> List[int]:
    """
    Return the list of gene id of genes containing an exon regulated \
    by DDX5/17.

    :param bed_df: A bed dataframe
    :return: The list of gene id containing exons regulated by ddx5/17

    >>> di = pd.DataFrame({'exon_name': ['A1BG_1', 'A1BG_2', 'A1BG_3',
    ... 'A1BG_4'], 'dist': [-5784, 5471, 5359, 4995],
    ... 'exon_id': ['19:58867449-58867591', '19:58864770-58864865',
    ...  '19:58864658-58864693', '19:58864294-58864563'],
    ... 'CTCF_hit_id': ['19:58873375-58873839', '19:58859059-58859299',
    ...  '19:58859059-58859299', '19:58859059-58859299'],
    ... 'infileTarget': ['CTCF.bed', 'CTCF.bed', 'CTCF.bed', 'CTCF.bed'],
    ... 'group': ['First', 'CE', 'CE', 'siPP_DOWN'],
    ... 'strand': ['-', '-', '-', '-']})
    >>> di = create_bed_file(di)
    >>> find_ddx_geneid(di)
    [14046]
    """
    exon = pd.read_csv(Path(__file__).parents[2] / "data" / "exon.bed",
                       sep="\t")
    gene = pd.read_csv(Path(__file__).parents[2] / "data" / "gene.bed",
                       sep="\t")[["id", "score"]]
    gene.rename({"score": "gene_name", "id": "gene_id"}, axis=1, inplace=True)
    exon["gene_id"] = exon["id"].str.replace(r"_\d+", "").astype(int)
    exon = exon.merge(gene, how="left", on="gene_id")
    exon["exon_name"] = exon["gene_name"] + \
        exon["id"].str.replace(r"\d+_", "_")
    exon.rename({"#ref": "chr", "end": "stop"}, axis=1, inplace=True)
    df = exon.merge(bed_df, how="left", on=["chr", "start", "stop", "strand",
                                            "exon_name"])
    sipp_exons = df.loc[df["group"] == "siPP", "id"].to_list()
    return [int(x.split("_")[0]) for x in sipp_exons]


def get_genes_with_internal_exons(exon: pd.DataFrame) -> List[int]:
    """
    Get the list of genes with internal exons

    :param exon: A dataframe containing fasterdb exons and their id \
    in the name column
    :return: The list of genes with an internal exons

    >>> edf = pd.DataFrame({"#ref": [1, 1, 1, 1, 2, 2, 2, 3, 3],
    ... "start": [100, 200, 300, 400, 100, 200, 300, 100, 200],
    ... "stop": [150, 250, 350, 450, 150, 250, 350, 150, 250],
    ... "id": ["1_1", "1_2", "1_3", "1_4", "2_1", "2_2", "2_3", "3_1", "3_2"]})
    >>> get_genes_with_internal_exons(edf
    [1, 2]
    """
    exon["pos"] = exon["id"].str.replace(r"\d+_", "").astype(int)
    exon["gene"] = exon["id"].str.replace(r"_\d+", "").astype(int)
    return list(exon.loc[exon["pos"] >= 3, "gene"].unique())


def create_bed_files(genes_sipp: List[int], ctrl_genes: List[int],
                     outfolder: Path) -> Tuple[Path, Path]:
    """
    Create bed files of siPP genes and for control genes.

    :param genes_sipp: A list of genes containing at least on exon \
    regulated by ddx/14
    :param ctrl_genes: A list of control genes with at least on internal exons
    :param outfolder: The folder where the bed files will be created
    :return: The path to sipp bed files and CTRL bed file
    """
    genes = pd.read_csv(Path(__file__).parents[2] / "data" / "gene.bed",
                        sep="\t")
    genes = genes[genes["id"].isin(ctrl_genes)]
    genes["#ref"] = "chr" + genes["#ref"]
    ctrl_genes2 = [g for g in ctrl_genes if g not in genes_sipp]
    ctrl_bed = genes[genes["id"].isin(ctrl_genes2)]
    ctrl_bed.to_csv(outfolder / "CTRL_genes.bed", sep="\t", index=False)
    sipp_gene = [g for g in genes_sipp if g in ctrl_genes]
    sipp_bed = genes[genes["id"].isin(sipp_gene)]
    sipp_bed.to_csv(outfolder / "siPP_genes.bed", sep="\t", index=False)
    return outfolder / "siPP_genes.bed", outfolder / "CTRL_genes.bed"


def get_vector_ctcf_site_counts(bed_file: Path, group: str,
                                ) -> List[int]:
    """
    Get the the number of CTCF sites in each genes defined in the file \
    bed file.

    :param bed_file: A bed file containing gene
    :param group: The name of the group of gene containing in bed file
    :return: A dataframe containing the list of the number of CTCF sites \
    located in each genes, their name and their size
    """
    ctcf_bed = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"
    outf = bed_file.parent / bed_file.name.replace(".bed", "ctcf_counts.bed")
    cmd = f"bedtools intersect -a {bed_file} -b {ctcf_bed} -c > {outf}"
    sp.check_call(cmd, shell=True)
    df = pd.read_csv(outf, sep="\t", names=['A', 'B', 'C', 'D', 'gene', 'F',
                                            'CTCF_counts'])
    df["size"] = df["C"] - df["B"]
    df["group"] = [group] * df.shape[0]
    return df[["CTCF_counts", "size", "gene", "group"]]


def create_df_ctcf_count(sipp_bed: Path, ctrl_bed: Path,
                         output: Path) -> pd.DataFrame:
    """

    :param sipp_bed: A bed containing genes regulated by DDX5/17
    :param ctrl_bed: A bed file containing control gene
    :param output: Folder containing the results
    :return: a dataframe containing the umbed of CTCF sites in both \
    kind of genes
    """
    sipp_count = get_vector_ctcf_site_counts(sipp_bed, "siPP")
    ctrl_count = get_vector_ctcf_site_counts(ctrl_bed, "CTRL")
    df = pd.concat([sipp_count, ctrl_count], axis=0, ignore_index=True)
    df.to_csv(output / "CTCF_counts_in_genes.txt", sep="\t", index=False)
    return df


def glm_poisson(df: pd.DataFrame, outfolder: Path) -> float:
    """Perform a glm poisson test on ``df``

    :param df: A dataframe of exons and the distance to the closest CTCF \
    binding site
    :param outfolder: The result folder
    :return The pvalue
    """
    pandas2ri.activate()
    output = outfolder / "diag"
    output.mkdir(exist_ok=True)
    stat_s = r("""
        require("DHARMa")
        require("emmeans")
        require("MASS")
        function(data, output){
            data$group <- as.factor(data$group)
            mod <- glm.nb(CTCF_counts ~ group + log10(size), data=data)
            nulmod = glm.nb(CTCF_counts ~ log10(size), data=data)
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(output, "/dignostics.png"))
            plot(simulationOutput)
            dev.off()
            return(anova(nulmod, mod, test="Chisq"))
        }
        """)
    stat = stat_s(df, str(output))
    stat.to_csv(outfolder / "statistical_analysis.txt", sep="\t", index=False)
    return stat.iloc[-1, -1]


def create_figure(df: pd.DataFrame, output: Path, stat: float,
                  ) -> None:
    """
    Create a figure showing the number of CTCF sites in genes

    :param df: A dataframe containing the number of CTCF sites inside \
    each genes
    :param output: The output folder
    :param stat: A p-value
    """
    sns.set(context="talk")
    g = sns.catplot(x="group", y="CTCF_counts", data=df, kind="bar",
                    height=8, aspect=1.7)
    g.ax.set_title(f"Number of CTCF sites in"
                   f" DDX5/17-regulated or control genes")
    g.ax.set_ylabel(f"Average number of CTCF sites in genes ")
    g.ax.set_xlabel("")
    tmp = df.groupby("group").mean().reset_index()
    add_stat_annotation(g.ax, data=tmp, x="group", y="CTCF_counts",
                        loc='inside',
                        boxPairList=[["CTRL", "siPP", stat]],
                        linewidth=1, fontsize="xx-small",
                        lineYOffsetToBoxAxesCoord=0.2)
    g.savefig(output / f"CTCF_counts_in_genes.pdf")


def main():
    df = load_exon_file(Path(__file__).parents[2] / "data" /
                        "All_Exons_vs_CTCF.csv")
    df_bed = create_bed_file(df)
    sipp_genes = find_ddx_geneid(df_bed)
    exon_df = pd.read_csv(Path(__file__).parents[2] / "data" / "exon.bed",
                          sep="\t")
    gene_ctrl = get_genes_with_internal_exons(exon_df)
    outfolder = Path(__file__).parents[2] / "result" / "CTCF_counts"
    outfolder.mkdir(exist_ok=True)
    sipp_bed, ctrl_bed = create_bed_files(sipp_genes, gene_ctrl, outfolder)
    df_count = create_df_ctcf_count(sipp_bed, ctrl_bed, outfolder)
    pval = glm_poisson(df_count, outfolder)
    create_figure(df_count, outfolder, pval)


if __name__ == "__main__":
    main()
