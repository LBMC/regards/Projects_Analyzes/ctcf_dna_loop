#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: A file containing the subvariables used in this submodule
"""

from pathlib import Path

class ConfigTAD:
    """
    The variable used in this submodule
    """
    output = Path(__file__).parents[2] / "result" / "sipp_tad"
    gene_bed = Path(__file__).parents[2] / "data" / "gene.bed"
    tad_input = Path(__file__).parents[2] / "data" / "SHSY5Y_TAD.bed"
    tad_bed = output /  "SHSY5Y_TAD_merged.bed"
    exon_bed = Path(__file__).parents[2] / "data" / "exon.bed"
    exon_sipp = Path(__file__).parents[2] / "data" / "All_Exons_vs_CTCF.csv"
    bed_chiapet = Path(__file__).parents[2] / "data" / "GSM970215.bed"
    output_tadchiapet = output / "tad_chiapet.bed"
    readthrough_f = Path(__file__).parents[2] / "data" / "readthrough_gene.txt"