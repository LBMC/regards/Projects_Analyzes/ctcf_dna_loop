#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a TAD like file \
from a bed file containing PET interaction taken from a pet file
"""

from .configTAD import ConfigTAD
import pandas as pd
import doctest
from pathlib import Path
import subprocess as sp


def load_interaction() -> pd.DataFrame:
    """
    Get the PET interactions of the chiapet bed interaction file

    :return: A dataframe containing chiapet-pet interactrion

    >>> load_interaction().head()
                                                 pet
    0            6:624661..625172-6:786691..787617,2
    2    X:40594771..40595573-X:40653089..40653617,2
    4    1:86048008..86048656-1:86158181..86159073,2
    6  14:56857334..56858264-14:56940821..56941567,3
    8  17:29773713..29774248-17:29851982..29852947,4
    """
    df = pd.read_csv(ConfigTAD.bed_chiapet, sep="\t",
                     names=["A", "B", "C", "pet", "E", "F"])
    df = df[["pet"]].drop_duplicates().copy()
    return df


def expand_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Expand the pet coordinate into multiple columns

    :param df: A dataframe with a column containing pet coordinates.
    :return: the expended dataframe

    >>> d = load_interaction().head()
    >>> expand_df(d)
      chr1    start1     stop1 chr2    start2     stop2 weigth
    0    6    624661    625172    6    786691    787617      2
    2    X  40594771  40595573    X  40653089  40653617      2
    4    1  86048008  86048656    1  86158181  86159073      2
    6   14  56857334  56858264   14  56940821  56941567      3
    8   17  29773713  29774248   17  29851982  29852947      4
    """
    df_exp = df["pet"].str.split(r":|\.\.|-|,", expand=True)
    df_exp.columns = ["chr1", "start1", "stop1", "chr2", "start2", "stop2",
                      "weigth"]
    for col in ["start1", "start2", "stop1", "stop2"]:
        df_exp[col] = df_exp[col].astype(int)
    return df_exp


def is_overlapping(mseries: pd.Series) -> bool:
    """
    True if the pets of an anchor overlaps false else

    :param mseries: A row containing pet of a chiapet-anchor
    :return: True if the anchor are overlapping, false else.

    >>> s = pd.Series({"chr1": 1, "start1": 10, "stop1": 100, "chr2": 1,
    ... "start2": 10, "stop2": 100})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 10, "stop1": 100, "chr2": 1,
    ... "start2": 15, "stop2": 105})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 5, "stop1": 95, "chr2": 1,
    ... "start2": 10, "stop2": 100})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 15, "stop1": 105, "chr2": 1,
    ... "start2": 10, "stop2": 100})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 10, "stop1": 100, "chr2": 1,
    ... "start2": 5, "stop2": 95})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 10, "stop1": 100, "chr2": 1,
    ... "start2": 50, "stop2": 75})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 50, "stop1": 75, "chr2": 1,
    ... "start2": 10, "stop2": 100})
    >>> is_overlapping(s)
    True
    >>> s = pd.Series({"chr1": 1, "start1": 10, "stop1": 20, "chr2": 1,
    ... "start2": 20, "stop2": 30})
    >>> is_overlapping(s)
    False
    """
    return mseries["chr1"] == mseries["chr2"] and (
            mseries["start1"] < mseries["stop2"] <= mseries["stop1"] or
            mseries["start1"] <= mseries["start2"] < mseries["stop1"] or
            mseries["start2"] < mseries["stop1"] <= mseries["stop2"] or
            mseries["start2"] <= mseries["start1"] < mseries["stop2"])


def check_pet_properties(df: pd.DataFrame) -> pd.DataFrame:
    """
    Function that indicates the number of pet anchor interacting with itself \
    the number of pet with inter-chromosomal contacts and the other pets

    :param df: A dataframe with couple of interacting pets
    :return: The pets that are intra-chromosomal and non-overlaping.

    >>> d = expand_df(load_interaction().head())
    >>> r = check_pet_properties(d)
    0 / 5 are inter-chromosomal pets
    0 / 5 are overlapping pets
    5 / 5 are intra-chromosomal, non overlapping pets
    >>> r.shape[0]
    5
    """
    df = df.copy()
    total_couple = df.shape[0]
    interchr = df[df["chr1"] != df["chr2"]].shape[0]
    df["overlap"] = df.apply(is_overlapping, axis=1)
    overlapping = df[df["overlap"] == True].shape[0]
    other = df[(df["overlap"] == False) & (df["chr1"] == df["chr2"])].shape[0]
    print(f"{interchr} / {total_couple} are inter-chromosomal pets")
    print(f"{overlapping} / {total_couple} are overlapping pets")
    print(f"{other} / {total_couple} are intra-chromosomal, non overlapping "
          f"pets")
    return df[(df["overlap"] == False) & (df["chr1"] == df["chr2"])]


def define_bed_loop(df: pd.DataFrame) -> pd.DataFrame:
    """
    Create a bed file containing loops

    :param df: A dataframe containing intra-chromosomal, non overlapping \
    loops
    :return: A bed file containing loops border

    >>> d = check_pet_properties(expand_df(load_interaction().head()))
    0 / 5 are inter-chromosomal pets
    0 / 5 are overlapping pets
    5 / 5 are intra-chromosomal, non overlapping pets
    >>> define_bed_loop(d)
      chr     start      stop
    2   1  86048008  86159073
    3  14  56857334  56941567
    4  17  29773713  29852947
    0   6    624661    787617
    1   X  40594771  40653617
    """
    dic = {"chr": [], "start": [], "stop": []}
    for i in range(df.shape[0]):
        row = df.iloc[i, :]
        for k, c in zip(["chr", "start", "stop"], ["chr1", "start1", "stop2"]):
            dic[k].append(row[c])
    return pd.DataFrame(dic).sort_values(["chr", "start"])


def add_name_and_score(tad_chiapet_bed: Path) -> None:
    """
    Add name score and strand column to a 3 column bed file.

    :param tad_chiapet_bed: A bed file containing 'tad' inferred from \
    ChIA-PET data.
    """
    df = pd.read_csv(tad_chiapet_bed, sep="\t", names=["chr", "start", "stop"])
    df["names"] = [f"TAD_{i + 1}" for i in range(df.shape[0])]
    df["score"] = ["."] * df.shape[0]
    df["strand"] = ["."] * df.shape[0]
    df.to_csv(tad_chiapet_bed, sep="\t", index=False, header=False)


def merge_loops(df: pd.DataFrame) -> Path:
    """
    Merge overlapping loops together.

    :param df: a dataframe containing loops
    :return: The dataframe with the overlaping loops merged
    """
    infile = ConfigTAD.output / "loop_tmp.txt"
    df.to_csv(infile, sep="\t", index=False, header=False)
    cmd = f"bedtools merge -i {infile} -d -1 > {ConfigTAD.output_tadchiapet}"
    sp.check_call(cmd, shell=True)
    return ConfigTAD.output_tadchiapet


def create_tad_loop() -> Path:
    """
    Crteate a tad file based on ctcf loops
    """
    if not ConfigTAD.output_tadchiapet.is_file():
        df = load_interaction()
        df = expand_df(df)
        df = check_pet_properties(df)
        df = define_bed_loop(df)
        mfile = merge_loops(df)
        add_name_and_score(mfile)
    return ConfigTAD.output_tadchiapet


if __name__ == "__main__":
    doctest.testmod()