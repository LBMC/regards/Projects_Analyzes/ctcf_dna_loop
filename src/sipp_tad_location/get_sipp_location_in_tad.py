#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to detect if genes regulated \
by DDX/17 are more concentrated inside some specific tad or randomly \
distributed among tad.
"""

from pathlib import Path
import subprocess as sp
from .configTAD import ConfigTAD
import pandas as pd
from typing import List
from rpy2.robjects import r, pandas2ri
import numpy as np
from .create_tad_from_chiapetbed import create_tad_loop


def merge_tad_together() -> None:
    """
    Merge the TADs together

    :return: A file containing non overlapping tad
    """
    cmd = f"sort -k1,1 -k2,2n {ConfigTAD.tad_input} | " \
          f"bedtools merge -d -1 > {ConfigTAD.tad_bed}"
    sp.check_call(cmd, shell=True)


def add_name() -> None:
    """
    Add name to tad file

    """
    df = pd.read_csv(ConfigTAD.tad_bed, sep="\t",
                     names=["chr", "start", "stop"])
    df["name"] = [f"TAD_{i}" for i in range(1, df.shape[0] + 1)]
    df["score"] = ["."] * df.shape[0]
    df["strand"] = ["."] * df.shape[0]
    df.to_csv(ConfigTAD.tad_bed, sep="\t", index=False, header=False)


def intersection_tad_gene(fasterdb_gene: Path, tad_file: Path, tad_type: str
                          ) -> Path:
    """

    :param fasterdb_gene: A bed file containing fasterDB gene
    :param tad_file: A bed file containing TAD
    :param tad_type: The original file used to define tad
    :return: A file containing the intersection performed by bedtools
    """
    intersection_file = ConfigTAD.output / f"sipp_tad_{tad_type}.bed"
    intersection_file.parent.mkdir(exist_ok=True)
    cmd = f"bedtools intersect -a {fasterdb_gene} -b {tad_file} -f 0.50 -wa " \
          f"-wb > {intersection_file}"
    sp.check_call(cmd, shell=True)
    return intersection_file


def recover_sipp_gene_id() -> List[int]:
    """
    Recover the gene id of gene gene containing an exon regulated \
    by siDDX5/17

    :return: The gene id of gene having an exon regulated by siDDX5/17
    """
    df = pd.read_csv(ConfigTAD.exon_sipp, sep="\t")
    df = df[df["group"].isin(["siPP_DOWN", "siPP_UP"])]
    df = df[["exon_name", "exon_id"]]
    df_exon = pd.read_csv(ConfigTAD.exon_bed, sep="\t")
    df_exon["start"] += 1
    df_exon["exon_id"] = df_exon["#ref"] + ":" + df_exon["start"].astype(str) \
        + "-" + df_exon["end"].astype(str)
    df_exon["gene_id"] = df_exon["id"].str.replace(r"_\d+", "").astype(int)
    df_exon["pos"] = df_exon["id"].str.replace(r"\d+_", "")
    df_gene = pd.read_csv(ConfigTAD.gene_bed, sep="\t")[["id", "score"]]
    df_gene.rename({"id": "gene_id", "score": "symbol"}, axis=1, inplace=True)
    df_exon = df_exon.merge(df_gene, how="left", on="gene_id")
    df_exon["exon_name"] = df_exon["symbol"] + "_" + df_exon["pos"].astype(str)
    df = df.merge(df_exon, how="inner", on=["exon_name", "exon_id"])
    return list(df["gene_id"].unique())


def recover_readthrough_geneid() -> List[int]:
    """
    :return: The id of gene with readthrough in siPP condition
    """
    readthrough_gene = ConfigTAD.readthrough_f.open("r").read().splitlines()
    df_gene = pd.read_csv(ConfigTAD.gene_bed, sep="\t")
    df_gene = df_gene[df_gene["score"].isin(readthrough_gene)]
    return list(df_gene["id"].unique())


def create_sipp_tad_table(sipp_gene: List[int], readtgh_gene: List[int],
                          tad_intersection: Path
                          ) -> pd.DataFrame:
    """
    A dataframe containing for each gene, it's hosting TAD and it's \
    regulation by DDX5/17

    :param sipp_gene: A list of sipp genes
    :param readthrough_gene: A list of readthrough genes
    :param tad_intersection: A file containing tad and its hosting gene
    :return: A table containing for each gene it's sipp regulation and \
    it's hosting tad
    """
    df = pd.read_csv(tad_intersection, sep="\t",
                     names=["chr", "start", "stop", "gene_id", "gene_name",
                            "strand", "chr2", "start2", "stop2", "TAD", "a",
                            "b"])
    for col, gn in zip(["sipp_reg", "readthrough"], [sipp_gene, readtgh_gene]):
        df[col] = [0] * df.shape[0]
        df.loc[df["gene_id"].isin(gn), col] = 1
    df["sipp_or_readtgh"] = (df["sipp_reg"].astype(bool) |
                             df["readthrough"].astype(bool)).astype(int)
    df = df[["sipp_reg", "readthrough", "sipp_or_readtgh", "gene_id",
             "gene_name", "TAD"]]
    return df


def get_number_of_gene_in_tad(df: pd.DataFrame, stat_tad_table: Path
                              ) -> pd.DataFrame:
    """
    Add a column tad_size in the dataframe containing the number of gene \
    in the TAD.

    :param df: A table containing for each gene it's sipp regulation and \
    it's hosting tad
    :param stat_tad_table: A table used to compute stat data
    :return: The input table with a column tad_size
    """
    tad_size = {"TAD": [], "TAD_size": []}
    tads = df["TAD"].unique()
    for tad in tads:
        tad_size["TAD"].append(tad)
        tad_size["TAD_size"].append(sum(df["TAD"] == tad))
    df = df.merge(pd.DataFrame(tad_size), how="left", on="TAD")
    df = df.sort_values("TAD")
    df.to_csv(stat_tad_table, sep="\t", index=False)
    return df


def glmm_maker(df: pd.DataFrame, outfile: Path, col: str) -> float:
    """
    Make the glmm analysis to see if the exon regulated by siPP \
    are equally distributed among the TADs.

    :param df: The stat dataframe
    :param outfile: A name of a file
    :param col: the col used to make stat
    :return: the pvalue of glmm

    """
    pandas2ri.activate()
    glmm = r(
        """
        require("lme4")
        require("DHARMa")

        function(data, folder, partial_name) {
            null_mod <- glm(%s ~ log2(TAD_size) , family=binomial, data=data)
            mod <- glmer(%s ~ log2(TAD_size) + (1 | TAD), data=data, 
                         family=binomial)
            simulationOutput <- simulateResiduals(fittedModel = mod, n = 250)
            png(paste0(folder, "/dignostics_", partial_name, ".png"))
            plot(simulationOutput)
            dev.off()
            return(anova(mod, null_mod, test="Chisq"))
        }

        """ % (col, col))
    folder = outfile.parent / "diagnostics"
    folder.mkdir(parents=True, exist_ok=True)
    partial_name = f"{outfile.stem}_{col}"
    if df["sipp_reg"].sum() == 0:
        return np.nan
    return glmm(df, str(folder), partial_name)["Pr(>Chisq)"][1]


def create_stat_table(tad_type: str):
    """
    Create a table used to check is the exons regulated by DDX5/17 \
    are randomly distributed across TADS.

    :param tad_type: The type of tad of interest.
    :return:
    """
    if tad_type == "SHSY5Y":
        merge_tad_together()
        add_name()
        tad_file = ConfigTAD.tad_bed
    else:
        tad_file = create_tad_loop()
    intersection_file = intersection_tad_gene(ConfigTAD.gene_bed, tad_file,
                                              tad_type)
    df = create_sipp_tad_table(recover_sipp_gene_id(),
                               recover_readthrough_geneid(), intersection_file)
    stat_tad_table = ConfigTAD.output / f"stat_TAD_{tad_type}_table.txt"
    df = get_number_of_gene_in_tad(df, stat_tad_table)
    pvalue_file = ConfigTAD.output / f"p_value_file_{tad_type}.txt"
    with pvalue_file.open("w") as outfile:
        col = ["sipp_reg", "readthrough", "sipp_or_readtgh"]
        names = ["sipp", "readthrough", "sipp_or_readtgh"]
        for c, n in zip(col, names):
            pval = glmm_maker(df, stat_tad_table, c)
            outfile.write(f"H0: {c} genes are randomly distributed across "
                          f"TAD.\n"
                          f"H1: {c} genes are not randomly distributed across "
                          f"TAD"
                          f".\nglmm LRT pvalue: {pval}\n\n")


if __name__ == "__main__":
    # create_stat_table("SHSY5Y")
    create_stat_table("ChIAPET")
