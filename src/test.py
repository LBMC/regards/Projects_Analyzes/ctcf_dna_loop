#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

import doctest
from pathlib import Path
from typing import List
import unittest


# recover ignored files
def get_ignored_files() -> List[str]:
    """
    Recover ignored python files in gitignore
    """
    gitignore = Path(__file__).parents[1] / '.gitignore'
    if not gitignore.is_file():
        return []
    with gitignore.open('r') as f:
        files = f.read().splitlines()
    return [cfile.replace('.py', '').replace('/', '.')
            for cfile in files if cfile.endswith('.py')]


# Loading every python file in this folder
list_mod = [str(mfile.relative_to(Path(__file__).parents[1]))
            for mfile in list(Path(__file__).parent.rglob('*.py'))]
list_mod2 = [m.replace('.py', '').replace('/', '.') for m in list_mod
             if '__init__' not in m
             and '__main__' not in m
             and 'test' not in m]
final_mod = [mod for mod in list_mod2 if mod not in get_ignored_files()]


def load_tests(loader, tests, ignore):
    for cmod in final_mod:
        tests.addTest(doctest.DocTestSuite(cmod))
    return tests


if __name__ == "__main__":
    unittest.main()
