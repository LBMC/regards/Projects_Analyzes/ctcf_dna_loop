#!/usr/bin/env python3

"""
Launch every scripts in vicinity folder
"""

from .merge_ctrl_and_sipp import main_merge
from .statistical_analysis import main_stat
from .create_terminal_exon_file import create_exon_first_or_last_table
import lazyparser as lp
from .create_ASE_factor_file import create_exon_file
from .launcher_function import ConfFile, execute_logf, logfigure


@lp.parse(control=["U170K", "SRSF1", "RBM25", "RBMX", "HNRNPK", 'None'])
def launcher(control: str = "SRSF1", logf: bool = False):
    """
    Test if exons regulated by DDX5/17 or another `control factor`, \
    have a different closeness to CTCF peaks

    :param control: The control factor we want to use
    :param logf: A boolean indicating if we want to explore the relation \
    of exon and their log2 distance to CTCF peaks
    """
    create_exon_file(control)
    conf = ConfFile(control)
    if not conf.ief.is_file():
        main_merge(conf.ief, conf.iif, factor=control,
                   remove_first_and_last=True)
    if not conf.aef.is_file():
        main_merge(conf.aef, conf.iaf, factor=control)
    if not conf.ol.is_file():
        create_exon_first_or_last_table(conf.aef, conf.ol,
                                        kind="last")
    if not conf.of.is_file():
        create_exon_first_or_last_table(conf.aef, conf.of,
                                        kind="first")
    thresholds = [0, 2000]
    if logf:
        # execute_logf(conf, control)
        logfigure(conf, control)
    for t in thresholds:
        main_stat(conf.ief, conf.output_folder, factor=control,
                  threshold=t)
        main_stat(conf.ief, conf.output_folder, factor=control,
                  threshold=t, up_and_down=False)
        # main_stat(outfile_last, output_folder, factor=control, threshold=t,
        #           terminal='uniq_regulation', kind="last")
        main_stat(conf.ol, conf.output_folder, factor=control, threshold=t,
                  terminal='regulation', kind="last")
        # main_stat(outfile_last, output_folder, factor=control, threshold=t,
        #           terminal='uniq_regulation', kind="last", rm0=True)
        # main_stat(outfile_last, output_folder, factor=control, threshold=t,
        #           terminal='regulation', kind="last", rm0=True)
        # main_stat(outfile_first, output_folder, factor=control, threshold=t,
        #           terminal='uniq_regulation', kind="first")
        main_stat(conf.of, conf.output_folder, factor=control, threshold=t,
                  terminal='regulation', kind="first")
        # main_stat(outfile_first, output_folder, factor=control, threshold=t,
        #           terminal='uniq_regulation', kind="first", rm0=True)
        # main_stat(outfile_first, output_folder, factor=control, threshold=t,
        #           terminal='regulation', kind="first", rm0=True)


launcher()
