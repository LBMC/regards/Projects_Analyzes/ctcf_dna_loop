#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to generate an input \
for laucnher_custom_input.

From a file containing gene names, turn it in a file containing \
last exons identifier along with a given group name

GENE FILE                      EXON FILE
                            exon_name       group_exon
ABCD1                       ABCD1_11        readthrough
ABCF3               --->    ABCF3_21        readthrough
ABHD12                      ABCF3_21        readthrough
"""


from pathlib import Path
from typing import List

import lazyparser as lp
import pandas as pd
from loguru import logger

from .merge_ctrl_and_sipp import get_last_exon


def get_gene_list(mfile: Path) -> List[str]:
    """
    Get the list of gene contained in a file

    :param mfile: A file containing a list of genes
    :return: The list of genes symbol inside mFile
    """
    return mfile.open("r").read().splitlines()


def build_dataframe(
    gene_list: List[str],
    last_exons: List["str"],
    group_name: str,
) -> pd.DataFrame:
    """
    Build a dataframe containing only last exons corresponding to \
    genes located inside gene_list

    :param gene_list: A list of genes comming from a file
    :param last_exons: The last fasterDB exons
    :param group_name: A name for the selected last exons
    :return: The list of last exons corresponding to genes
    """
    df = pd.DataFrame({"exon_name": last_exons})
    df["gene"] = df["exon_name"].str.replace(r"_\d+", "", regex=True)
    df = df[df["gene"].isin(gene_list)].copy()
    df["group_exon"] = [group_name] * df.shape[0]
    lg = ", ".join([x for x in gene_list if x not in df["gene"].to_list()])
    if len(gene_list) != df.shape[0]:
        logger.warning(
            f"Warning: {len(gene_list) - df.shape[0]} genes lost : {lg}"
        )
    return df.drop("gene", axis=1)


@lp.parse(gene_file="file")
def build_exon_file(
    gene_file: str, group_name: str, output: str = "data"
) -> None:
    """
    Build a file containing a list of last exons belonging to genes find \
    in a file given in input (gene_file). The gene_file must contain a \
    list of genes symbol, like this

    ABCD1
    ABCF3
    ...

    it will produce a file like this

    exon_name       group_exon
    ABCD1_11        readthrough
    ABCF3_21        readthrough

    :param gene_file: A file containing a list of gene symbol
    :param group_name: The group_name column in the
    """
    outfile = Path(output) / f"{group_name}_exon.csv"
    outfile2 = Path(output) / f"{group_name}_exon.bed"
    if outfile.is_file():
        print(f"The file {outfile} exits. Remove it to produce the new file !")
        exit(1)
    genes = get_gene_list(Path(gene_file))
    exons, df_exon = get_last_exon(get_df=True)
    df_exon = df_exon[df_exon["score"].isin(genes)].copy()
    df_exon = df_exon.rename({"#ref": "chr", "name": "exons"}, axis=1)
    df_exon[["exons", "chr", "start", "end"]].to_csv(
        outfile2, sep="\t", index=False
    )
    df = build_dataframe(genes, exons, group_name)
    df.to_csv(outfile, sep="\t", index=False)


if __name__ == "__main__":
    build_exon_file()  # pylint: disable=no-value-for-parameter
