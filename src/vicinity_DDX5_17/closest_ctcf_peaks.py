#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to get the distribution of \
distances of exons and their closest CTCF peaks
"""
import doctest

from .ctcf_logdist import create_interval_from_series
import numpy as np
from typing import Tuple, Dict
from pathlib import Path
import pandas as pd
from tqdm import tqdm
from numba import jit
import multiprocessing as mp
from ..CTCF_orientation.add_orientation import update_dist_table, ConfO


def filter_interval(interval: Tuple, peaks: np.array) -> np.array:
    """
    Get only peaks inside intervals of interest.

    :param interval: A genomic interval
    :param peaks: A list of peak in the same chromosome
    :return: The list of peaks inside interval

    >>> inte = ("chr1", 0, 11, "+")
    >>> a = np.array([[10, 20], [20, 30], [50, 100], [100, 200]])
    >>> filter_interval(inte, a)
    array([[10, 20]])
    >>> inte = ("chr1", 11, 15, "+")
    >>> filter_interval(inte, a)
    array([[10, 20]])
    >>> inte = ("chr1", 11, 20, "+")
    >>> filter_interval(inte, a)
    array([[10, 20]])
    >>> inte = ("chr1", 11, 21, "+")
    >>> filter_interval(inte, a)
    array([[10, 20],
           [20, 30]])
    >>> inte = ("chr1", 180, 200, "+")
    >>> filter_interval(inte, a)
    array([[100, 200]])
    """
    return peaks[((peaks[:, 0] >= interval[1]) & (peaks[:, 0] < interval[2])) |
                 ((peaks[:, 1] > interval[1]) & (peaks[:, 1] <= interval[2])) |
                 ((interval[1] >= peaks[:, 0]) & (interval[1] < peaks[:, 1])) |
                 ((interval[2] > peaks[:, 0]) & (interval[2] <= peaks[:, 1]))
                 ]


def bedtools_maker(exon: Tuple, peaks: np.array
                   ) -> pd.DataFrame:
    """
    Perform a bed intersection

    :param input_file: The input file of interest
    :param peak_file: A file containing CTCF peaks
    :return: A dataframe of the peaks
    """
    df = pd.DataFrame(peaks, columns=["start", "stop"])
    df["chr"] = [exon[0]] * df.shape[0]
    return df


def compute_peak_distance(exon: Tuple[str, int, int, str],
                          peak: pd.Series) -> int:
    """
    Compute the distance between an exon and a peak.

    :param exon: A genomic interval corresponding to an exon
    :param peak: A serie corresponding to a peak
    :return: The distance between exon and ctcf peaks

    >>> p = pd.Series({"chr": "chr1", "start": 10, "stop": 15})
    >>> e = ("chr1", 10, 15, "+")
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 12, "stop": 13})
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 20})
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 12, "stop": 20})
    >>> compute_peak_distance(e, p)
    1
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 13})
    >>> compute_peak_distance(e, p)
    -1
    >>> p = pd.Series({"chr": "chr1", "start": 17, "stop": 20})
    >>> compute_peak_distance(e, p)
    3
    >>> p = pd.Series({"chr": "chr1", "start": 15, "stop": 20})
    >>> compute_peak_distance(e, p)
    2
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 9})
    >>> compute_peak_distance(e, p)
    -3
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 10})
    >>> compute_peak_distance(e, p)
    -3
    >>> p = pd.Series({"chr": "chr1", "start": 10, "stop": 15})
    >>> e = ("chr1", 10, 15, "-")
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 12, "stop": 13})
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 20})
    >>> compute_peak_distance(e, p)
    0
    >>> p = pd.Series({"chr": "chr1", "start": 12, "stop": 20})
    >>> compute_peak_distance(e, p)
    -1
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 13})
    >>> compute_peak_distance(e, p)
    1
    >>> p = pd.Series({"chr": "chr1", "start": 17, "stop": 20})
    >>> compute_peak_distance(e, p)
    -3
    >>> p = pd.Series({"chr": "chr1", "start": 15, "stop": 20})
    >>> compute_peak_distance(e, p)
    -2
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 9})
    >>> compute_peak_distance(e, p)
    3
    >>> p = pd.Series({"chr": "chr1", "start": 5, "stop": 10})
    >>> compute_peak_distance(e, p)
    3
    """
    middle_peak = int((peak["stop"] + peak["start"]) / 2)
    if exon[0] != peak["chr"]:
        raise ValueError("The peak and exons must be on the same chromosome")
    if exon[1] <= middle_peak < exon[2]:
        return 0
    if middle_peak < exon[1]:
        dist = exon[1] - middle_peak
        if exon[3] != "-":
            dist *= -1
    else:
        dist = middle_peak - exon[2]
        if exon[3] == "-":
            dist *= -1
    return dist


def compute_peak_distances(exon: Tuple[str, int, int, str],
                           df_peak: pd.DataFrame) -> pd.DataFrame:
    """
    Compute the distance between an exon and peaks.

    :param exon: A genomic interval corresponding to an exon
    :param df_peak: A dataframe corresponding to close peaks
    :return: The distance between exon and the center of ctcf peaks

    >>> dp = pd.DataFrame({"chr": ["chr1"] * 9,
    ... "start": [10, 12, 5, 12, 5, 17, 15, 5, 5],
    ... "stop": [15, 13, 20, 20, 13, 20, 20, 9, 10]})
    >>> e = ("chr1", 10, 15, "+")
    >>> compute_peak_distances(e, dp)
       peak_id  dist
    0  1:10-15     0
    1  1:12-13     0
    2   1:5-20     0
    3  1:12-20     1
    4   1:5-13    -1
    5  1:17-20     3
    6  1:15-20     2
    7    1:5-9    -3
    8   1:5-10    -3
    """
    distance = [compute_peak_distance(exon, df_peak.iloc[i, :])
                for i in range(df_peak.shape[0])]
    df_peak["dist"] = distance
    df_peak["peak_id"] = df_peak["chr"].str.lstrip("chr") + ":" + \
        df_peak["start"].astype(str) + \
        "-" + df_peak["stop"].astype(str)
    return df_peak[["peak_id", "dist"]]


def get_closest_sites(df_dist: pd.DataFrame, data: Dict) -> pd.DataFrame:
    """
    Get the closest peak upstream and downstream the exons.

    :param df_dist: A dataframe of distance
    :param data: A dictionary containing exon id and strand
    :return: The distance to the closest CTCF peaks upstream and downstream \
    the exon
    >>> dd = pd.DataFrame({'peak_id': ['chr1:10-15', 'chr1:12-13', 'chr1:5-20',
    ... 'chr1:12-20', 'chr1:5-13', 'chr1:17-20', 'chr1:15-20', 'chr1:5-9',
    ... 'chr1:5-10'],
    ... 'dist': [0, 0, 0, 2, -2, 4, 3, -4, -4]})
    >>> d = {"exon_id": "chr1:10-20", "strand": "+"}
    >>> get_closest_sites(dd, d).to_dict("list") == {
    ... 'exon_id': ['chr1:10-20'], 'upstream_dist': [0],
    ... 'upstream_hit': ['chr1:10-15'], 'downstream_dist': [0],
    ... 'downstream_hit': ['chr1:10-15'], "strand": ["+"]}
    True
    >>> dd = pd.DataFrame({'peak_id': [
    ... 'chr1:12-20', 'chr1:17-20', 'chr1:15-20', 'chr1:5-9', 'chr1:5-10'],
    ... 'dist': [2, 4, 3, -4, -4]})
    >>> get_closest_sites(dd, d).to_dict("list") == {
    ... 'exon_id': ['chr1:10-20'], 'upstream_dist': [-4],
    ... 'upstream_hit': ['chr1:5-9'], 'downstream_dist': [2],
    ... 'downstream_hit': ['chr1:12-20'], "strand": ["+"]}
    True
    >>> dd = pd.DataFrame({'peak_id': [
    ... 'chr1:12-20', 'chr1:17-20', 'chr1:15-20'],
    ... 'dist': [2, 4, 3]})
    >>> get_closest_sites(dd, d).iloc[:, 0:5]
          exon_id  upstream_dist  upstream_hit  downstream_dist downstream_hit
    0  chr1:10-20            NaN           NaN                2     chr1:12-20
    """
    if not df_dist.empty:
        df_dist["exon_id"] = [data["exon_id"]] * df_dist.shape[0]
        df_dist["strand"] = [data["strand"]] * df_dist.shape[0]
    df_upstream = df_dist[df_dist["dist"] <= 0].sort_values(
        "dist", ascending=False).iloc[0:1, :]
    df_downstream = df_dist[df_dist["dist"] >= 0].sort_values(
        "dist").iloc[0:1, :]
    if df_upstream.empty:
        df_upstream = pd.DataFrame({"peak_id": [np.nan], "dist": [np.nan],
                                    "exon_id": [data["exon_id"]],
                                    "strand": [data["strand"]]})
    if df_downstream.empty:
        df_downstream = pd.DataFrame({"peak_id": [np.nan], "dist": [np.nan],
                                      "exon_id": [data["exon_id"]],
                                      "strand": [data["strand"]]})
    df_upstream.rename({"peak_id": "upstream_hit",
                        "dist": "upstream_dist"}, axis=1, inplace=True)
    df_downstream.rename({"peak_id": "downstream_hit",
                          "dist": "downstream_dist"}, axis=1, inplace=True)
    df_dist = df_upstream.merge(df_downstream, how="inner",
                                on=["exon_id", "strand"])
    return df_dist[["exon_id", "upstream_dist", "upstream_hit",
                    "downstream_dist", "downstream_hit", "strand"]]

@jit(nopython=True)
def get_min(start: int, size: int) -> int:
    """
    Build and interval of data.

    :param start: a value
    :param size: another value
    :return: the minimum value between start - size and 0
    """
    if start - size < 0:
        return 0
    else:
        return start - size


def get_closest_peaks(peak_dic: Dict,
                      exon: Tuple[str, int, int, str],
                      size: int = 100000) -> pd.DataFrame:
    """
    Get the genomic intervals, relatively to an exon in which \
    we want to get the number of CTCF sites.

    :param peak_dic: A dictionary of peak
    :param exon: A genomic interval corresponding to an exon
    :param size: The number nucleotide after and before the exon that \
    corresponds to the region of interest
    :return: The relative positions of genomic interval around an exon of \
    interest

    >>> pf = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"
    >>> r = get_closest_peaks(load_peak_file(pf),
    ... ("chr1", 870000, 870050, '+'))
    >>> r
    exon_id            1:870000-870050
    upstream_dist                -2852
    upstream_hit       1:866936-867360
    downstream_dist               3700
    downstream_hit     1:873456-874045
    strand                           +
    Name: 0, dtype: object
    """
    interval = (exon[0], get_min(exon[1], size), exon[2] + size)
    peaks = peak_dic[interval[0]]
    peaks = filter_interval(interval, peaks)
    peaks = bedtools_maker(exon, peaks)
    if not peaks.empty:
        distance = compute_peak_distances(exon, peaks).copy()
    else:
        distance = pd.DataFrame({"peak_id": [], "dist": []})
    data = {"exon_id": exon[0].lstrip("chr") + ":" +
                       str(exon[1] + 1) + "-" + str(exon[2]),
            "strand": exon[3]}
    return get_closest_sites(distance, data).iloc[0, :]


def load_peak_file(peak_file: Path) -> Dict:
    """
    From a peak file return a dictionary of peaks

    :param peak_file: Load a file containing peaks
    :return: The dictionary of peaks
    """
    d = {}
    with peak_file.open("r") as infile:
        for line in infile:
            line = line.split("\t")
            if line[0] not in d:
                d[line[0]] = [[int(line[1]), int(line[2])]]
            else:
                d[line[0]].append([int(line[1]), int(line[2])])
    for k in d:
        d[k] = np.array(d[k])
    return d


def get_row(row: pd.Series, peak_dic: Dict, size: int,
            peak_file: Path) -> pd.Series:
    """
    Return a row containing the distance of CTCF peaks to an exon.

    :param row: A row corresponding to an exon
    :param peak_dic: A dictionary containing peaks
    :param size: The number nucleotide after and before the exon that \
    corresponds to the region of interest
    :param peak_file: The file containing ctcf peaks
    :return:Return a row containing the distance of CTCF peaks to an exon.
    """
    exon = create_interval_from_series(row)
    s = get_closest_peaks(peak_dic, exon, size)
    s["infileTarget"] = peak_file.name
    s["group"] = row["group"]
    s["exon_name"] = row["exon_name"]
    if "deltaPSI" in row:
        s["deltaPSI"] = row["deltaPSI"]
    if np.isnan(s["upstream_dist"]) and np.isnan(s["downstream_dist"]):
        s["dist"] = np.nan
        s["CTCF_hit_id"] = np.nan
        return s
    min_val = np.nanmin([abs(s["upstream_dist"]), s["downstream_dist"]])
    if min_val == abs(s["upstream_dist"]):
        s["dist"] = s["upstream_dist"]
        s["CTCF_hit_id"] = s["upstream_hit"]
    else:
        s["dist"] = s["downstream_dist"]
        s["CTCF_hit_id"] = s["downstream_hit"]
    return s


def create_ctcf_distance_file(ps: int, distance_file: Path, outfile: Path,
                              peak_file: Path,
                              size: int = 100000) -> pd.DataFrame:
    """
    Create a CTCF distance file

    :param ps: The number of processes to create
    :param distance_file: A file containing CTCF distances to the closest \
    CTCF peak.
    :param outfile: The final file
    :param peak_file: A file containing CTCF peaks
    :param size: The number nucleotide after and before the exon that \
    corresponds to the region of interest
    :return: The relative positions of genomic interval around an exon of \
    interest
    """
    df = pd.read_csv(distance_file, sep="\t")
    outfile.parent.mkdir(exist_ok=True)
    peak_dic = load_peak_file(peak_file)
    pool = mp.Pool(processes=ps)
    processes = []
    for i in range(df.shape[0]):
        row = df.iloc[i, :]
        args = (row, peak_dic, size, peak_file)
        processes.append(pool.apply_async(get_row, args))
    results = [p.get(timeout=None) for p in tqdm(processes,
                                                 desc="Parsing exons")]
    df = pd.DataFrame(results)
    cols = ["exon_name", "exon_id", "dist", "CTCF_hit_id", "upstream_dist",
             "upstream_hit", "downstream_dist", "downstream_hit",
             "infileTarget", "group", "strand"]
    if "deltaPSI" in list(df.columns):
        cols += ["deltaPSI"]
    df = df[cols]
    df.to_csv(outfile, sep="\t", index=False)
    orientation_file = outfile.parent / f"{outfile.stem}_orientation.csv"
    update_dist_table(outfile, ConfO.closest_peak_bed, ConfO.gimme_res,
                      ConfO.full_peak_bed, ConfO.genome, ConfO.pfm,
                      orientation_file)



def launcher() -> None:
    """
    Create a CTCF distance file
    """
    names = ["Sipp_exon_vs_CTCF.csv",
             "internal_exon_vs_CTCF.csv", "All_Exons_vs_CTCF.csv"]
    for n in names:
        create_ctcf_distance_file( 7, ConfO.data / n, ConfO.output / n,
                                   ConfO.data / "CTCF_merged.bed", 100000)


if __name__ == "__main__":
    launcher()
    # doctest.testmod()