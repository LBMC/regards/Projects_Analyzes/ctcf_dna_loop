#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Compute closest peak using bedtools
"""

from pathlib import Path
from typing import Dict
from loguru import logger
import subprocess as sp
import pandas as pd
import lazyparser as lp


def sort_exons(exon_bed: Path, output: Path) -> Path:
    """
    Sort a bed file containing exons and return the bed file sorted

    :param exon_bed: The bed file containing exons
    :param output: Folder were the sort bed will be created
    :return: Return path to sorted file
    """
    outfile = output / f"{exon_bed.stem}_sorted.bed"
    cmd = f"sort -k1,1 -k2,2n {exon_bed} | grep -v '#ref' | sed 's|^|chr|g' | grep -v 'chrMT' > {outfile}"
    sp.check_call(cmd, shell=True)
    return outfile


def sort_and_get_peak_center(bed_peak: Path, output: Path,
                             center_peak: bool = True) -> Path:
    """
    Sort a bedfile containing bed peaks and return only center of those peaks

    :param bed_peak: A bed file containing peaks
    :param output: Folder were the sort bed will be created
    :return: Sorted bed file containing peak center
    """
    outfile = output / f"{bed_peak.stem}_center.bed"
    cmd = f"sort -k1,1 -k2,2n {bed_peak} | "
    if center_peak:
        cmd += "awk 'BEGIN{FS=\"\\t\";OFS=\"\\t\"}{m = int(($3+$2)/2); print $1,m,m+1,$4,$5,$6}'"
        cmd += f" | sort -k1,1 -k2,2n > {outfile}"
        logger.info(f"sorting and centering {bed_peak}")
    else:
        logger.info(f"sorting{bed_peak}")
        cmd += "awk 'BEGIN{FS=\"\\t\";OFS=\"\\t\"}{print $1,$2,$3,$4,$5,\".\"}'"
        cmd += f" > {outfile}"
    sp.check_call(cmd, shell=True)
    return outfile


def compute_dist(exon_bed: Path, pic_center: Path, output: Path,
                 location: str = "upstream") -> Path:
    """
    Compute the distance between an  exons and it's closest peak \
    upstream or downstream the sequence

    :param exon_bed: The bed of exon
    :param pic_center: the bed of peak center
    :param output: folder where the file will be created
    :param location: location of closest peaks, defaults to "upstream"
    :return: The distance file upstream or downtream exon and peaks
    """
    lcp = "-id" if location == "upstream" else "-iu"
    name = f"distance_{pic_center.stem.replace('_center', '')}_{location}.txt"
    outfile = output / name
    cmd = f"bedtools closest -a {exon_bed} -b {pic_center} -D a {lcp} -t first"
    cmd += f" > {outfile}"
    sp.check_call(cmd, shell=True)
    return outfile


def get_distance_table(distance_file: Path, kind: str = "upstream"
                       ) -> pd.DataFrame:
    """
    read a file containing the upstream/downstream closest peak to exons

    :param distance_file: A file containing distance
    :param kind: the kind of distance located within the file, defaults to "upstream"
    :return: A temporary file
    """
    colnames = ["chr", "start", "end", "id", "score", "strand", "chrh",
                "starth", "endh", "name", "scoreh", "strandh", f"{kind}_dist"]
    df = pd.read_csv(distance_file, sep="\t",
                     names=colnames)
    df.loc[df["starth"] == -1, f"{kind}_dist"] = 1000000
    df[f"{kind}_hit"] = df["chrh"] + ":" + df["starth"].astype(str) + \
        "-" + df["endh"].astype(str)
    df[f"{kind}_hit"] = df[f"{kind}_hit"].replace(r"^chr", "", regex=True)
    df.loc[df["starth"] == -1, f"{kind}_hit"] = ""
    df["exon_id"] = df["chr"] + ":" + (df["start"] + 1).astype(str) + \
        "-" + df["end"].astype(str)
    df["exon_id"] = df["exon_id"].replace(r"^chr", "", regex=True)
    return df[["id", "exon_id", f"{kind}_dist", f"{kind}_hit", "strand"]
              ].copy()


def get_exons_name(df: pd.DataFrame, gene_bed: Path) -> pd.DataFrame:
    """
    Add exon_name

    :param df_dist: A dataframe of distance
    :param gene_bed: A bed file containing genes
    :return: df_dist with a gene name columns
    """
    df_gene = pd.read_csv(gene_bed, sep="\t",
                          usecols=["id", "score"])
    df["gene_id"] = df["id"].str.replace(r"_\d+", "", regex=True).astype(int)
    df["pos"] = df["id"].str.replace(r"\d+_", "", regex=True)
    df_gene = df_gene.rename({"id": "gene_id", "score": "gene_name"}, axis=1)
    df = df.merge(df_gene, how="inner", on="gene_id")
    df["exon_name"] = df["gene_name"] + "_" + df["pos"]
    return df.drop(["pos", "gene_id", "id"], axis=1).copy()


def build_dict(distance_file: Path) -> Dict[str, Dict[str, str]]:
    """
    Create a dictionary linking exon_name to a dictionary linking exon id \
    to their group

    :param distance_file: The distance file of interest
    :return: _description_
    """
    grp_df = pd.read_csv(distance_file, sep="\t",
                         usecols=["exon_name", "exon_id",
                                  "group"])
    dic = {}
    for _, row in grp_df.iterrows():
        if row["exon_name"] not in dic:
            dic[row["exon_name"]] = {row["exon_id"]: row["group"]}
        else:
            dic[row["exon_name"]][row["exon_id"]] = row["group"]
    return dic


def add_group(df: pd.DataFrame, grp_file: Path) -> pd.DataFrame:
    """
    Add a group column to df

    :param df: A dataframe of distance
    :param grp_file: A fie containing groups for each exon
    :return: The dataframe with a group column
    """
    dic_grp = build_dict(grp_file)
    groups = []
    for _, row in df.iterrows():
        groups.append(dic_grp.get(row["exon_name"], {}
                                  ).get(row["exon_id"], ""))
    df["group"] = groups
    return df


def add_hit_n_dist(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add hit_id and dist column

    :param df: The dataframe of interest
    :return: The datafrem with dist and hit_id column
    """
    dists = []
    hit_ids = []
    for _, row in df.iterrows():
        if row["upstream_dist"] <= row["downstream_dist"]:
            dists.append(row["upstream_dist"])
            hit_ids.append(row["upstream_hit"])
        else:
            dists.append(row["downstream_dist"])
            hit_ids.append(row["downstream_hit"])
    df["dist"] = dists
    df["hit_id"] = hit_ids
    return df


def build_complete_distance_file(upstream_file: Path, downstream_file: Path,
                                 peak_bed_name: str, group_file: Path,
                                 gene_bed: Path) -> pd.DataFrame:
    """
    Build the complete distance file

    :param upstream_file: A file containing upstream distance
    :param downstream_file: A file containing downstream distance
    :param peak_bed_name: The name of the input peak file
    :param group_file: The file indentifying exons
    :param gene_bed: A bed file containing genes
    :return: The complete distance file
    """
    df_up = get_distance_table(upstream_file, "upstream")
    df_down = get_distance_table(downstream_file, "downstream")
    df = df_up.merge(df_down, how="inner", on=["id", "exon_id", "strand"])
    df = get_exons_name(df, gene_bed)
    df["infileTarget"] = [peak_bed_name] * df.shape[0]
    df = add_group(df, group_file)
    df = add_hit_n_dist(df)
    return df[["exon_name", "exon_id", "dist", "hit_id", "upstream_dist",
               "upstream_hit", "downstream_dist", "downstream_hit",
               "infileTarget", "group", "strand"]].copy()


def get_internal_exon(df: pd.DataFrame) -> pd.DataFrame:
    """
    Get the internal exon in df

    :param df: A distance datafame
    :return: dataframe containing only internal exons
    """
    df = df[-df["exon_name"].str.endswith("_1")].copy()
    df["gene"] = df["exon_name"].str.replace(r"_\d+", "", regex=True)
    df["pos"] = df["exon_name"].str.replace(r".*_", "", regex=True)
    last_exon = df[["exon_name", "gene", "pos"]
                   ].sort_values(["gene", "pos"]
                                 ).drop_duplicates(
                                     subset="gene", keep="last"
                                     )["exon_name"].to_list()
    df = df[-df["exon_name"].isin(last_exon)].copy()
    return df.drop(["gene", "pos"], axis=1)


@lp.parse(exon_bed="file", peak_bed="file", group_file="file", gene_bed="file")
def build_distance_file(exon_bed: str, peak_bed: str, group_file: str,
                        gene_bed: str, output: str,
                        center_peak: bool = True) -> None:
    """
    Create a distance file needed for scripts in src.vicinity_DDX5_17 folder

    :param exon_bed: A bed file containing exons
    :param peak_bed: A bed file containing peaks
    :param group_file: A group file containing exons group \
    (data/All_Exons_vs_CTCF.csv file)
    :param gene_bed: A bed file containing genes
    :param output: Folder where the results will be created
    :param center_peaks: True to get the center of the peak file \
    False to only sort the peak file
    :return: The distance file
    """
    output_folder = Path(output)
    if not output_folder.is_dir():
        raise NotADirectoryError(f"{output_folder} doesn't exists !")
    output_inter = output_folder / "tmp_file"
    output_inter.mkdir(exist_ok=True)
    logger.info(f"sorting {exon_bed}")
    sorted_exon_bed = sort_exons(Path(exon_bed), output_inter)
    center_peak = sort_and_get_peak_center(Path(peak_bed), output_inter,
                                           center_peak)
    logger.info("build bedtools upstream distance file ")
    up_file = compute_dist(sorted_exon_bed, center_peak, output_inter,
                           "upstream")
    logger.info("build bedtools downstream distance file")
    down_file = compute_dist(sorted_exon_bed, center_peak, output_inter,
                             "downstream")
    logger.info("build distance table")
    df = build_complete_distance_file(up_file, down_file, Path(peak_bed).name,
                                      Path(group_file), Path(gene_bed))
    df.to_csv(output_folder / f"{Path(peak_bed).name}_input.txt", sep="\t",
              index=False)
    df = get_internal_exon(df)
    df.to_csv(output_folder / f"internal_{Path(peak_bed).name}_input.txt", sep="\t",
              index=False)


if __name__ == "__main__":
    build_distance_file() # pylint: disable=no-value-for-parameter

