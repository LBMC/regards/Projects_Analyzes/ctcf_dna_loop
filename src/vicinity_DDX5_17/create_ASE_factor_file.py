#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a file containing the \
exons up and down-regulated by a splicing factor
"""

from pathlib import Path
from typing import List
import pandas as pd


def get_input_file(sf: str, basedir: Path) -> List[Path]:
    """
    Return the files containing up and down-regulated exons by sf.

    :param sf: The splicing factor for which we want to get the regulated \
    file
    :param basedir: The Basedir
    :return: files containing up and down-regulated exons by sf.
    """
    folder = basedir / "data" / "exons_sf"
    return list(folder.glob(f"input*{sf}*"))


def extract_exon(filename: Path) -> List[str]:
    """
    Extract all exon in a file containing exon regulated by a splicing factor.

    :param filename: A interest file
    :return: The list of exon in filename
    """
    with filename.open('r') as f:
        return f.read().splitlines()


def create_input_exon_table(sf: str, input_file: List[Path]) -> pd.DataFrame:
    """
    Create the initial table with the exons regulated by sf.
    :param sf: The name of the splicing factor of interest
    :param input_file: The files containing up and down regulated exons by \
    sf
    :return: The initial table with the exons regulated by sf.
    """
    if len(input_file) != 2:
        raise ValueError(f'The input_file list should contains 2 files not '
                         f'{len(input_file)}')
    dic = {'exon_name': [], 'regulation': []}
    for mfile in input_file:
        reg = "up"
        if '_down' in mfile.name:
            reg = "down"
        exon_list = extract_exon(mfile)
        dic['exon_name'] += exon_list
        dic['regulation'] += [f'{sf}_{reg}'] * len(exon_list)
    return pd.DataFrame(dic)


def create_exon_and_gene_id_column(df_reg: pd.DataFrame, exon_bed: Path
                                   ) -> pd.DataFrame:
    """
    Add two column to df_reg : exon_id and gene_id.

    :param df_reg: The table containing exon up and down-regulated by sf.
    :param exon_bed: The bed file containing fasterdb exons
    :return: The df_reg table but with two new columns : exon_id and gene_id
    """
    df_exon = pd.read_csv(exon_bed, sep="\t")
    df_reg = df_reg.merge(df_exon, how="left", left_on="exon_name",
                          right_on="id")
    df_reg["start"] = df_reg["start"] + 1
    df_reg["exon_id"] = df_reg["ref"].astype(str) + ":" + \
                        df_reg["start"].astype(str) + "-" + \
                        df_reg["end"].astype(str)
    df_reg["gene_id"] = df_reg["exon_name"].str.replace(r"_\d+", "")
    df_reg["gene_id"] = df_reg["gene_id"].astype(int)
    return df_reg[["exon_name", "regulation", "exon_id", "gene_id"]]


def update_exon_name_column(df_reg: pd.DataFrame, gene_bed: Path
                            ) -> pd.DataFrame:
    """
    Update the 'exon_name' column in `df_reg` to identify the exon \
    by the gene name and it's position within the gene.

    :param df_reg: The table containing exon up and down-regulated by sf.
    :param gene_bed: The bed file containing fasterdb genes
    :return: The same df_reg without the gene_id column and with the \
    exon_name updated
    """
    df_gene = pd.read_csv(gene_bed, sep="\t")[["id", "score"]]
    df_gene.columns = ["gene_id", "gene_name"]
    df_reg = df_reg.merge(df_gene, how="left", on="gene_id")
    df_reg["exon_pos"] = df_reg["exon_name"].str.replace(r"\d+_", "")
    df_reg["name"] = df_reg["gene_name"] + "_" + df_reg["exon_pos"].astype(str)
    df_reg = df_reg[["name", "exon_id", "regulation"]]
    df_reg.columns = ["exon_name", "exon_id", "regulation"]
    return df_reg


def create_exon_file(sf: str):
    """
    Create a file containing exons regulated by a sf.

    :param sf: The sf name
    """
    base = Path(__file__).parents[2]
    exon_bed = base / "data" / "exon.bed"
    gene_bed = base / "data" / "gene.bed"
    outfile = base / "data" / f"ASE_{sf}.csv"
    if not outfile.is_file():
        if sf != "None":
            input_files = get_input_file(sf, base)
            df_reg = create_input_exon_table(sf, input_files)
            df_reg = create_exon_and_gene_id_column(df_reg, exon_bed)
            df_reg = update_exon_name_column(df_reg, gene_bed)
        else:
            df_reg = pd.DataFrame(
                columns=["exon_name", "exon_id", "regulation"])
        df_reg.to_csv(outfile, sep="\t", index=False)
