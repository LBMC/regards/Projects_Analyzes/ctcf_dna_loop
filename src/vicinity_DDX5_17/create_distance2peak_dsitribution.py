#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create \
a figure that display the distribution of the log2 distance of exons \
to their closest upstream or downstream CTCF peaks
"""

import doctest
from .ctcf_logdist import create_group_column, get_partial_name
from pathlib import Path
import pandas as pd
import numpy as np
import seaborn as sns
from typing import Optional


NCOLOR = {"SIPP": "red", "readthrough": "#CC00CC", "siPP_DOWN": "red",
              "siPP_UP": "#0070C0", "SRSF1": "#7F7F7F",
              "SRSF1_down": "#7F7F7F", 'chimerreadthg':"#CC00CC",
              "SRSF1_up": "#7F7F7F", "ctrl": "#BFBFBF"}

def get_figure_table(df_dist: pd.DataFrame, side: str) -> pd.DataFrame:
    """
    Get the column with the distance to CTCF peaks of interest.

    :param df_dist: A dataframe containing the closest CTCF Upstream and \
    downstream peaks for every internal/first or last exons in human genes
    :param side: The side on which we want to display the distance to \
    ctcf sites
    :return: The dataframe with the distance column of interest

    >>> d = pd.DataFrame({'exon_name': ['A1BG_2', 'A1BG_3', 'A1BG_4'],
    ... 'exon_id': ['19:58864770-58864865', '19:58864658-58864693',
    ... '19:58864294-58864563'],
    ... 'dist': [5590.0, 5478.0, 5114.0],
    ... 'CTCF_hit_id': ['19:58859059-58859299'] * 3,
    ... 'upstream_dist': [-8973.0, -9145.0, -9275.0],
    ... 'upstream_hit': ['19:58873375-58874302'] * 3,
    ... 'downstream_dist': [5590.0, 5478.0, 5114.0],
    ... 'downstream_hit': ['19:58859059-58859299'] * 3,
    ... 'infileTarget': ['CTCF_merged.bed'] * 3,
    ... 'group': ['CE'] * 3, 'strand': ['-'] * 3, 'final_group': ['ctrl'] * 3})
    >>> get_figure_table(d, "upstream")
      exon_name  upstream_dist   log2dist final_group
    0    A1BG_2        -8973.0  13.131375        ctrl
    1    A1BG_3        -9145.0  13.158767        ctrl
    2    A1BG_4        -9275.0  13.179132        ctrl
    >>> get_figure_table(d, "downstream")
      exon_name  downstream_dist   log2dist final_group
    0    A1BG_2           5590.0  12.448633        ctrl
    1    A1BG_3           5478.0  12.419434        ctrl
    2    A1BG_4           5114.0  12.320236        ctrl
    """
    dist_col = f"{side}_dist"
    ndf = df_dist[["exon_name", dist_col, "final_group"]].copy()
    ndf = ndf[abs(ndf[dist_col]) > 0].copy()
    ndf["log2dist"] = np.log2(abs(ndf[dist_col]))
    return ndf[["exon_name", dist_col, "log2dist", "final_group"]]


def make_distance_distribution(df_logdist: pd.DataFrame, side: str, kind: str,
                               outfile: Path, rm_grp: Optional[str] = None
                               ) -> None:
    """

    :param df_logdist: A dataframe containing the log2 distance to \
    the clostest CTCF peaks on the side `side`of the exon
    :param side: The side of interest to display the distance to CTCF peak \
    distribution
    :param kind: The kind of exon of interest
    :param outfile: The file where the distribution figure will be created
    :param rm_grp: Suffixes of the group to remove
    """
    sns.set(context="poster", style="white")
    df_logdist.rename({"final_group": "group"}, axis=1, inplace=True)
    if rm_grp is not None:
        df_logdist = df_logdist[(-df_logdist["group"].str.endswith(rm_grp)) &
                                (-df_logdist["group"].str.endswith(
                                    rm_grp.upper()))]
    g = sns.displot(data=df_logdist, x="log2dist", kind="kde",
                    hue="group", aspect=1.7, height=9,
                    palette=NCOLOR, common_norm=False, clip=[0, 16.61])
    g.fig.subplots_adjust(top=0.94, bottom=0.145)
    g.fig.suptitle(f"Distribution of the closest CTCF sites {side} of "
                   f"the {kind} exons ")
    g.set_xlabels(f"Log2 distance {side} the exon")
    if side == "upstream":
        g.ax.invert_xaxis()
        g.ax.yaxis.tick_right()
        g.ax.yaxis.set_label_position("right")
        g.ax.spines["left"].set_visible(False)
        g.ax.spines["right"].set_visible(True)
    # sns.move_legend(g, "lower center", ncol=6, fontsize=12, title="")
    g.savefig(outfile)


def create_distance_figure(filename: Path, folder: Path, factor: str,
                           up_and_down: bool = True, kind: str = "internal"):
    """
    Checks if DDX5_17 exons are more often close to CTCF sites than other

    :param filename: file that contains the dataframe with exon and their \
    CTCF exons sites
    :param folder: Folder where the figures will be created
    :param factor: The name of the output factor
    :param up_and_down: Say if we want to consider up and down exons separately
    :param kind: The kind of exons contained in filename. \
    Choose from first and last
    """
    df = pd.read_csv(filename, sep="\t")
    df = create_group_column(df, kind, up_and_down)
    kind = "" if kind == "internal" else kind
    for side in ["upstream", "downstream"]:
        output_folder, outfile = get_partial_name(up_and_down, kind, factor,
                                                  folder, side)
        output_folder.mkdir(exist_ok=True, parents=True)
        ndf = get_figure_table(df, side)
        ndf.to_csv(outfile.parent / f"{outfile.stem}.txt", sep="\t")
        if not up_and_down:
            make_distance_distribution(ndf, side, kind, outfile)
        else:
            for rm_grp, name in zip(["_up", "_down"], ["down", "up"]):
                outf = outfile.parent / f"{outfile.stem}_{name}.pdf"
                make_distance_distribution(ndf.copy(), side, kind, outf,
                                           rm_grp=rm_grp)



if __name__ == "__main__":
    doctest.testmod()
