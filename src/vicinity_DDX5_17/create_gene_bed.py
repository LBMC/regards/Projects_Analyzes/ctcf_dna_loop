#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
This script will create a bed file containing FasterDB genes
"""

import pymysql
from .config import *
import pandas as pd
import logging
from pathlib import Path
import numpy as np


def connection() -> pymysql.connections.Connection:
    """
    :return: (pymysql object to connected to the fasterDB database)
    """
    cnx = pymysql.connect(user=user, password=password,
                          host=host, database=fasterdb)
    return cnx


def get_gene(cnx: pymysql.connections.Connection) -> pd.DataFrame:
    """
    Get every gene in FasterDB.

    :param cnx: Connection to fasterdb
    :return: The result of the query corresponding to a bed file
    """
    query = """
        SELECT chromosome, start_sur_chromosome -1, end_sur_chromosome,
               official_symbol, strand
        FROM genes
    """
    df = pd.read_sql_query(query, cnx)
    dic = {"-1": "-", "1": "+"}
    df["strand"] = df["strand"].map(dic)
    df["score"] = np.full(len(df), ".", dtype='<U1')
    logging.debug(df.head())
    return df[["chromosome", "start_sur_chromosome -1", "end_sur_chromosome",
               "official_symbol", "score", "strand"]]


def main_bed():
    """Create the Fasterdb gene bed"""
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    cnx = connection()
    output = Path(__file__).parents[2] / "result" / "vinity_DDX5_17"
    output.mkdir(exist_ok=True)
    output_file = output / "genes.bed"
    logging.info("Recovering gene data")
    df = get_gene(cnx)
    logging.info(f"Saving bed file to {output_file}")
    df.to_csv(output_file, sep="\t", index=False, header=False)
