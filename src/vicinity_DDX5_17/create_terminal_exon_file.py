#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to get the final exons \
of each gene of the CTCF distance table and say if the gene hosting this \
exons contains at least on exons regulated by U1-70K or siPP
"""

from functools import reduce
from pathlib import Path

import pandas as pd


def get_reg(x: str, y: str) -> str:
    """
    Return the regulation of interest
    :param x: The regulation of exon x
    :param y: The regulation of exon y
    :return: the regulation wanted
    """
    if x == y:
        return x
    if x != "ctrl" and y != "ctrl":
        return "multiple"
    return x if x != "ctrl" else y


def get_r(x: str, y: str) -> str:
    """
    Return the regulation of interest
    :param x: The regulation of exon x
    :param y: The regulation of exon y
    :return: the regulation wanted
    """
    x = x.split("_")[0].upper() if "_" in x else x
    y = y.split("_")[0].upper() if "_" in y else y
    if x == y:
        return x
    if x != "ctrl" and y != "ctrl":
        return "multiple"
    return x if x != "ctrl" else y


def uniq_regulation(serie: pd.Series) -> str:
    """
    Applies the function get_reg on a pandas serie
    """
    return reduce(get_reg, serie)


def regulation(serie: pd.Series) -> str:
    """
    Applies the function get_r on a pandas serie
    """
    return reduce(get_r, serie)


def select_first_or_last_exon(
    df: pd.DataFrame,
    kind: str = "last",
    remove_genes_with_no_internal_exons: bool = True,
) -> pd.DataFrame:
    """
    Select only the lasts exons from the dataframe given in input.

    :param df: The target dataframe
    :param kind: The kind of exons to keep
    :param remove_genes_with_no_internal_exons: A boolean indicating whether \
    to remove genes with no internal exons, default to True
    :return: The datframe with only the last exons of each gene
    """
    exon_df = format_exon_bed()
    if "regulation" in df.columns:
        df.drop("regulation", axis=1, inplace=True)
    df["gene_name"] = df["exon_name"].str.replace(r"_\d+", "")
    df["exon_pos"] = df["exon_name"].str.extract(r"_(\d+)").astype(int)
    df = df.merge(exon_df, how="left", on=["exon_id", "gene_name", "exon_pos"])
    df = df[-df["id"].isna()]
    df["gene_id"] = df["gene_id"].astype(int)
    if kind == "first":
        df_last = df.sort_values(
            ["gene_id", "exon_pos"], ascending=[True, True]
        ).drop_duplicates(subset=["gene_id"], keep="last")[
            ["gene_id", "exon_pos"]
        ]
        df_last.columns = ["gene_id", "last_exon"]
        df = df.merge(df_last, how="left", on="gene_id")
    df_tmp = df.sort_values(
        ["gene_id", "exon_pos"], ascending=[True, True]
    ).drop_duplicates(subset=["gene_id"], keep=kind)
    df = (
        df[["new_group", "gene_id"]]
        .groupby("gene_id")
        .agg({"new_group": [uniq_regulation, regulation]})
        .reset_index()
    )
    df = df.droplevel(0, axis=1).rename(columns={"": "gene_id"})

    df_final = df_tmp.merge(df, how="left", on="gene_id")
    df_final.drop("new_group", axis=1, inplace=True)
    df_final["regulation"] = df_final["regulation"].apply(
        lambda x: x.split("_")[0].upper() if "_" in x else x
    )
    # we only take terminal exon if their are in gene with more than 2 exons
    # i.e. gene with internal exons
    df_final["last_fasterDB_exon"] = df_final["last_fasterDB_exon"].astype(int)
    v = 2 if remove_genes_with_no_internal_exons else 0
    if kind == "last":
        df_final = df_final.loc[
            (df_final["exon_pos"] > v)
            & (df_final["exon_pos"] == df_final["last_fasterDB_exon"]),
            :,
        ]
    else:
        df_final = df_final.loc[
            (df_final["last_fasterDB_exon"] > v) & (df_final["exon_pos"] == 1),
            :,
        ]
    return df_final


def format_exon_bed() -> pd.DataFrame:
    """
    :return: A dataframe of exon with it's coordinate id + it's \
    id corresponding to it's gene id and it's position within the gene
    """
    exon_file = Path(__file__).parents[2] / "data" / "exon.bed"
    exon_bed = pd.read_csv(exon_file, sep="\t")
    exon_bed["exon_id"] = (
        exon_bed["#ref"].astype(str)
        + ":"
        + (exon_bed["start"] + 1).astype(str)
        + "-"
        + exon_bed["end"].astype(str)
    )
    exon_bed["gene_id"] = exon_bed["id"].str.replace(r"_\d+", "").astype(int)
    exon_bed["exon_pos"] = exon_bed["id"].str.replace(r"\d+_", "").astype(int)
    gene_bed = Path(__file__).parents[2] / "data" / "gene.bed"
    df_gene = pd.read_csv(gene_bed, sep="\t")[["id", "score"]]
    df_gene.columns = ["gene_id", "gene_name"]
    exon_bed = exon_bed.merge(df_gene, how="left", on=["gene_id"])
    tmp = exon_bed.sort_values(
        ["gene_id", "exon_pos"], ascending=[True, True]
    ).drop_duplicates(subset=["gene_id"], keep="last")[["gene_id", "exon_pos"]]
    tmp["exon_pos"] = tmp["exon_pos"].astype(int)
    tmp.columns = ["gene_id", "last_fasterDB_exon"]
    exon_bed = exon_bed.merge(tmp, how="left", on="gene_id")
    return exon_bed[
        [
            "exon_id",
            "id",
            "gene_id",
            "gene_name",
            "exon_pos",
            "last_fasterDB_exon",
        ]
    ]


def create_exon_first_or_last_table(
    filename: Path,
    outfile: Path,
    kind: str = "last",
    remove_genes_with_no_internal_exons: bool = True,
):
    """
    Create a table containing terminal exons of genes with at least \
    one internal exons and their distance to the closest \
    CTCF site
    :param filename: File containing FasterDB exons and the distance \
    to their closest CTCG sites.
    :param kind: The kind of exons to keep. Choose from first and last
    :param outfile: The output file name
    :param remove_genes_with_no_internal_exons: A boolean indicating whether \
    to remove genes with no internal exons, default to True
    """
    df = pd.read_csv(filename, sep="\t")
    df = select_first_or_last_exon(
        df, kind, remove_genes_with_no_internal_exons
    )
    df.to_csv(outfile, sep="\t", index=False)
