#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a figure showing the \
distance of CTCF to exons belonging to different group
"""
import doctest
import pandas as pd
import numpy as np
from math import log2
from typing import Tuple, List, Dict
import re
from pathlib import Path
import subprocess as sp
import seaborn as sns
from typing import Optional
from tqdm import tqdm
from loguru import logger
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


def create_group_column(df: pd.DataFrame, kind: str = "internal",
                        up_and_down: bool = False):
    """
    Create a column that associate each exon to a given group of exon.

    :param df: A dataframe containing exons and their distance to CTCF peaks.
    :param kind: The kind of exons selected
    :param up_and_down: A boolean indicating if we want to study up and \
    down regulated exons
    :return: A dataframe with the table column

    >>> d = pd.DataFrame({'exon_name': ['DSC2_19', 'MPHOSPH8_16', 'CRYL1_11',
    ... 'FNDC3A_29', 'IFT88_30', 'CDADC1_12', 'ABCC13_16'],
    ... 'uniq_regulation': ['ctrl', 'SRSF1_down', 'SRSF1_up', 'siPP_DOWN',
    ... 'multiple', 'siPP_UP', 'multiple'],
    ... 'regulation': ['ctrl', 'SRSF1', 'SRSF1', 'SIPP', 'multiple', 'SIPP',
    ... 'SIPP']})
    >>> create_group_column(d, "terminal", True)
         exon_name final_group regulation
    0      DSC2_19        ctrl       ctrl
    1  MPHOSPH8_16  SRSF1_down      SRSF1
    2     CRYL1_11    SRSF1_up      SRSF1
    3    FNDC3A_29   siPP_DOWN       SIPP
    5    CDADC1_12     siPP_UP       SIPP
    >>> create_group_column(d, "terminal", False)
         exon_name uniq_regulation final_group
    0      DSC2_19            ctrl        ctrl
    1  MPHOSPH8_16      SRSF1_down       SRSF1
    2     CRYL1_11        SRSF1_up       SRSF1
    3    FNDC3A_29       siPP_DOWN        SIPP
    5    CDADC1_12         siPP_UP        SIPP
    6    ABCC13_16        multiple        SIPP
    >>> d = pd.DataFrame({'exon_name': ['A1BG_2', 'A1BG_3', 'A1BG_4',
    ... 'A1BG_5', 'A1BG_6', 'A1BG_7'], 'new_group': ['ctrl', 'multiple',
    ... 'siPP_DOWN', 'siPP_UP', 'SRSF1_down', 'SRSF1_up']})
    >>> create_group_column(d, "internal", False)
      exon_name   new_group final_group
    0    A1BG_2        ctrl        ctrl
    2    A1BG_4   siPP_DOWN        siPP
    3    A1BG_5     siPP_UP        siPP
    4    A1BG_6  SRSF1_down       SRSF1
    5    A1BG_7    SRSF1_up       SRSF1
    >>> create_group_column(d, "internal", True)
      exon_name   new_group final_group
    0    A1BG_2        ctrl        ctrl
    2    A1BG_4   siPP_DOWN   siPP_DOWN
    3    A1BG_5     siPP_UP     siPP_UP
    4    A1BG_6  SRSF1_down  SRSF1_down
    5    A1BG_7    SRSF1_up    SRSF1_up
    """
    if kind == "internal":
        df = df.loc[df["new_group"] != "multiple", :].copy()
        group = df["new_group"].values
        if not up_and_down:
            group = [g.split("_")[0] for g in group]
        df["final_group"] = group
        return df
    if up_and_down:
        df = df.loc[df["uniq_regulation"] != "multiple", :].copy()
        df.rename({"uniq_regulation": "final_group"}, axis=1, inplace=True)
        return df
    df = df.loc[df["regulation"] != "multiple", :].copy()
    df.rename({"regulation": "final_group"}, axis=1, inplace=True)
    return df


def get_relative_exon_positions(side: str, strand: str, start: int = 200,
                                stop: int = 100000,
                                nb_interval: int = 25) -> np.array:
    """
    Get the genomic intervals, relatively to an exon in which \
    we want to get the number of CTCF sites.

    :param side: the side of exons of interest (upstream or downstream)
    :param strand: the strand of the exon
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval we want to have
    :return: The relative positions of genomic interval around an exon of \
    interest

    >>> list(get_relative_exon_positions("downstream", "+")[0:5])
    [200.0, 256.0, 329.0, 422.0, 541.0]
    >>> list(get_relative_exon_positions("upstream", "-")[0:5])
    [200.0, 256.0, 329.0, 422.0, 541.0]
    >>> list(get_relative_exon_positions("downstream", "-")[0:5])
    [-200.0, -256.0, -329.0, -422.0, -541.0]
    >>> list(get_relative_exon_positions("upstream", "+")[0:5])
    [-200.0, -256.0, -329.0, -422.0, -541.0]
    """
    strand_v = -1 if strand == "-" else 1
    side_v = -1 if side == "upstream" else 1
    return np.round(np.logspace(log2(start), log2(stop), num=nb_interval + 1,
                                base=2) * strand_v * side_v, 0)


def create_interval_from_series(exon: pd.Series) -> Tuple[str, int, int, str]:
    """
    Create a genomic interval from a pandas series.

    :param exon: A row of a dataframe
    :return: The exons values

    >>> e = pd.Series({"exon_name": "SRSF1_1", "exon_id": "10:201-300",
    ... "strand": "-"})
    >>> create_interval_from_series(e)
    ('chr10', 200, 300, '-')
    """
    tmp = re.split("[:-]", exon["exon_id"]) + [exon["strand"]]
    return "chr" + tmp[0], int(tmp[1]) - 1, int(tmp[2]), tmp[3]


def create_interval_with_relative_positions(exon: Tuple[str, int, int, str],
                                            loc: Tuple[int, int], side: str,
                                            ) -> Tuple[str, int, int]:
    """
    Get the absolute positions of a genomic interval.

    :param exon: An exon
    :param loc: The relative strand and stop position related to an exon
    :param side: The side of the exon of interest
    :return: The genomic interval with absolute position

    >>> create_interval_with_relative_positions(('chr10', 200, 300, '+'),
    ... (10, 20), "downstream")
    ('chr10', 310, 320)
    >>> create_interval_with_relative_positions(('chr10', 200, 300, '-'),
    ... (10, 20), "upstream")
    ('chr10', 310, 320)
    >>> create_interval_with_relative_positions(('chr10', 200, 300, '-'),
    ... (-10, -20), "downstream")
    ('chr10', 180, 190)
    >>> create_interval_with_relative_positions(('chr10', 200, 300, '+'),
    ... (-10, -20), "upstream")
    ('chr10', 180, 190)
    """
    d = {1: 2, -1: 1}
    loc = sorted(loc)
    side_v = -1 if side == "upstream" else 1
    strand_v = -1 if exon[-1] == "-" else 1
    return (exon[0], int(exon[d[side_v * strand_v]] + loc[0]),
            int(exon[d[side_v * strand_v]] + loc[1]))


def get_abscissa(relative_positions: np.array) -> List[float]:
    """
    Get the abscissa values of relative .

    :param relative_positions: The relative positions regarding to an exons.
    :return: The log2 abscissa values associated to the center of relative \
    positions

    >>> r = [200.0, 256.0, 329.0, 422.0, 541.0, 1000]
    >>> get_abscissa(r)
    [7.833, 8.192, 8.553, 8.911, 9.59]
    >>> get_abscissa([-200.0, -256.0, -329.0, -422.0, -541.0, -1000])
    [7.833, 8.192, 8.553, 8.911, 9.59]
    """
    rp = relative_positions
    return [round(log2(abs(np.mean([rp[i], rp[i + 1]]))), 3)
            for i in range(len(rp) - 1)]


def write_tmp_file(output_file: Path, intervals: List[Tuple[str, int, int]]
                   ) -> None:
    """
    :param output_file: The file that will be used as an input for bedtools
    :param interval: An list of interval of inter
    """
    intervals = ["\t".join(map(str, interval)) for interval in intervals]
    with output_file.open("w") as infile:
        infile.write("\n".join(intervals) + "\n")


def bedtools_maker(input_file: Path, peak_file: Optional[Path]) -> List[int]:
    """
    Perform a bed intersection

    :param input_file: The input file of interest
    :param peak_file: A file containing CTCF peaks
    :return: The number of peaks inside every intervals interval in -a
    """
    cmd = f"bedtools intersect -a {input_file} -b {peak_file} -F 0.5 -c | " \
          f"cut -f 4"
    return [int(n) for n in sp.check_output(cmd, shell=True
                                            ).strip().split(b'\n')]


def get_abscissa_n_ordinate(output_folder: Path, peak_file: Path,
                            exon: Tuple[str, int, int, str],
                            side: str, start: int = 200, stop: int = 100000,
                            nb_interval: int = 25) -> Tuple[List, List]:
    """
    Get the genomic intervals, relatively to an exon in which \
    we want to get the number of CTCF sites.

    :param output_folder: The folder where result will be created. \
    We need it to perform bedtools intersection
    :param peak_file: A file containing CTCF peaks
    :param exon: A genomic interval corresponding to an exon
    :param side: the side of exons of interest (upstream or downstream)
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval we want to have
    :return: The relative positions of genomic interval around an exon of \
    interest

    >>> pf = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"
    >>> a, o = get_abscissa_n_ordinate(
    ... Path("/media/Data/Projects/Cyril/DNA_loop/data/test"), pf,
    ... ("chr1", 870000, 870050, '+'), "downstream")
    >>> a == [7.833, 8.192, 8.553, 8.911, 9.269, 9.628, 9.987, 10.345, 10.703,
    ... 11.062, 11.42, 11.779, 12.138, 12.496, 12.855, 13.214, 13.572, 13.931,
    ... 14.29, 14.648, 15.007, 15.366, 15.724, 16.083, 16.441]
    True
    >>> o == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0,
    ... 1, 1, 4, 7]
    True
    >>> a, o = get_abscissa_n_ordinate(
    ... Path("/media/Data/Projects/Cyril/DNA_loop/data/test"), pf,
    ... ("chr1", 870000, 870050, '-'), "upstream")
    >>> a == [7.833, 8.192, 8.553, 8.911, 9.269, 9.628, 9.987, 10.345, 10.703,
    ... 11.062, 11.42, 11.779, 12.138, 12.496, 12.855, 13.214, 13.572, 13.931,
    ... 14.29, 14.648, 15.007, 15.366, 15.724, 16.083, 16.441]
    True
    >>> o == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0,
    ... 1, 1, 4, 7]
    True
    >>> a2, o = get_abscissa_n_ordinate(
    ... Path("/media/Data/Projects/Cyril/DNA_loop/data/test"), pf,
    ... ("chr1", 710000, 710050, '+'), "downstream")
    >>> a2 == a
    True
    >>> o
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2]
    """
    p = get_relative_exon_positions(side, exon[-1], start, stop,
                                    nb_interval)
    output = output_folder / "tmp.txt"
    abscissa = get_abscissa(p)
    intervals = []
    for i in range(len(p) - 1):
        interval = create_interval_with_relative_positions(exon,
                                                           (p[i], p[i + 1]),
                                                           side)
        intervals.append(interval)
    write_tmp_file(output, intervals)
    ordinate = bedtools_maker(output, peak_file)
    output.unlink()
    return abscissa, ordinate


def bad_exon(interval: Tuple[str, int, int, str], stop: int, side: str,
             dic_size: Dict[str, int]) -> bool:
    """

    :param interval: An exon from the dataframe with ctcf distance
    :param stop: The maximum value of the interval of interest
    :param side: Upstream of downstream
    :param dic_size: A dictionary indicating the size of a chromosome
    :return: True if it's a bad exon False, else

    >>> bad_exon(("chr1", 10, 20, '+'), 20, "upstream", {"chr1": 100})
    True
    >>> bad_exon(("chr1", 10, 20, '+'), 10, "upstream", {"chr1": 100})
    False
    >>> bad_exon(("chr1", 10, 20, '+'), 9, "upstream", {"chr1": 100})
    False
    >>> bad_exon(("chr1", 10, 20, '-'), 20, "upstream", {"chr1": 100})
    False
    >>> bad_exon(("chr1", 10, 20, '-'), 20, "upstream", {"chr1": 39})
    True
    >>> bad_exon(("chr1", 10, 20, '+'), 20, "downstream", {"chr1": 39})
    True
    >>> bad_exon(("chr1", 10, 20, '+'), 20, "downstream", {"chr1": 40})
    False
    >>> bad_exon(("chr1", 10, 20, '-'), 10, "downstream", {"chr1": 40})
    False
    >>> bad_exon(("chr1", 9, 20, '-'), 10, "downstream", {"chr1": 40})
    True
    """
    if (
        (side == "upstream" and interval[-1] == "+")
        or (side == "downstream" and interval[-1] == "-")
    ) and interval[1] - stop < 0:
        return True
    return (
        (side == "downstream" and interval[-1] == "+")
        or (side == "upstream" and interval[-1] == "-")
    ) and interval[2] + stop > dic_size[interval[0]]


def exon_analysis(row: pd.Series, output_folder: Path, peak_file: Path,
                  dic_size: Dict,
                  side: str, start: int = 200,
                  stop: int = 100000, nb_interval: int = 25
                  ) -> Optional[pd.DataFrame]:
    """
    :param row: An exon and its distance to CTCF peak
    :param output_folder: Folder where we want to create the input file used \
    to perform bedtools intersections
    :param peak_file: A file containing CTCF peaks
    :param side: The side of the exon of interest
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval in which CTCF sites are counted
    :param dic_size: A dictionary indicating the size of a chromosome
    :return: The table containing the distance to ctcf peaks for the \
    exon of interest
    """
    exon = create_interval_from_series(row)
    if bad_exon(exon, stop, side, dic_size):
        return None
    x_vec, y_vec = get_abscissa_n_ordinate(output_folder, peak_file,
                                           exon, side,
                                           start, stop, nb_interval)
    return pd.DataFrame({"exon_name": [row["exon_name"]] * len(x_vec),
                         "distance": x_vec, "ctcf_peaks": y_vec,
                         "regulation": [row["final_group"]] * len(x_vec)})


def parse_chromosome_size_file() -> Dict[str, int]:
    """
    Read a file containing the chromosome name and it's size.

    :return: A dictionary linking each chromosome to it's size
    """
    mfile = Path(__file__).parents[2] / "data" / "hg19.ren.chrom.sizes"
    with mfile.open("r") as infile:
        dic = {}
        for line in infile:
            chrom, size = line.strip().split("\t")
            dic["chr" + chrom] = int(size)
    return dic


def create_ctcf_log_position_expanded_table(
        df: pd.DataFrame, output_folder: Path, peak_file: Path,
        side: str, start: int = 200,
        stop: int = 100000, nb_interval: int = 25) -> pd.DataFrame:
    """

    :param df: A dataframe containing exons and their distance to CTCF peaks.
    :param output_folder: Folder where we want to create the input file used \
    to perform bedtools intersections
    :param peak_file: A file containing CTCF peaks
    :param side: The side of the exon of interest
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval in which CTCF sites are counted
    :return: A dataframe indicating for each exon the number of CTCF sites \
    located after/before it (according to the side parameter) and their \
    relative log2 distance from them

    >>> pf = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"
    >>> d = pd.DataFrame({'exon_name': ['A1BG_2', 'A1BG_3'],
    ... 'dist': [100, 100],
    ... 'exon_id': ['1:870001-870050', '1:710001-710050'],
    ... 'CTCF_hit_id': ['1:869901-870000', '1:709901-700050'],
    ... 'group': ['CE', 'CE'],
    ... 'strand': ['+', '+'],
    ... 'Intragenique': ['outside', 'outside'],
    ... 'Intragenique+-1000': ['intragenique', 'intragenique'],
    ... 'new_group': ['ctrl', 'SIPP_down']})
    >>> d = create_group_column(d, 'internal', up_and_down=False)
    >>> d = create_ctcf_log_position_expanded_table(d,
    ... Path("/media/Data/Projects/Cyril/DNA_loop/data/test"), pf,
    ... "downstream")
    >>> d[d["distance"] > 15]
       exon_name  distance  ctcf_peaks regulation
    20    A1BG_2    15.007           0       ctrl
    21    A1BG_2    15.366           1       ctrl
    22    A1BG_2    15.724           1       ctrl
    23    A1BG_2    16.083           4       ctrl
    24    A1BG_2    16.441           7       ctrl
    45    A1BG_3    15.007           0       SIPP
    46    A1BG_3    15.366           0       SIPP
    47    A1BG_3    15.724           1       SIPP
    48    A1BG_3    16.083           1       SIPP
    49    A1BG_3    16.441           2       SIPP
    """
    list_df = []
    bad_count = 0
    dic_size = parse_chromosome_size_file()
    for i in tqdm(range(df.shape[0])):
        row = df.iloc[i, :]
        tdf = exon_analysis(row, output_folder, peak_file, dic_size,
                            side, start, stop, nb_interval)
        if tdf is not None:
            list_df.append(tdf)
        else:
            bad_count += 1
    if bad_count > 0:
        logger.warning(f"{bad_count} / {df.shape[0]} were not taken into "
                       f"account because they are too close of chromosome "
                       f"boundaries")
    return pd.concat(list_df, axis=0, ignore_index=True)


def get_avg_number_of_peak(df: pd.DataFrame) -> pd.DataFrame:
    """

    :param df: A dataframe indicating for each exon the number of CTCF sites \
    located after/before it (according to the side parameter) and their \
    relative log2 distance from them
    :return: The dataframe with mean ctcf sites number across log2 distance \
    intervals

    >>> d = pd.DataFrame({'exon_name': ['A1BG_2', 'A1BG_2', 'A1BG_2', 'A1BG_2',
    ... 'A1BG_2', 'A1BG_3', 'A1BG_3', 'A1BG_3', 'A1BG_3', 'A1BG_3', 'A1BG_4',
    ... 'A1BG_4', 'A1BG_4', 'A1BG_4', 'A1BG_4', 'A1BG_5', 'A1BG_5', 'A1BG_5',
    ... 'A1BG_5', 'A1BG_5'],
    ... 'distance': [15.007, 15.366, 15.724, 16.083, 16.441, 15.007, 15.366,
    ... 15.724, 16.083, 16.441, 15.007, 15.366, 15.724, 16.083, 16.441, 15.007,
    ... 15.366, 15.724, 16.083, 16.441], 'ctcf_peaks': [0, 1, 1, 4, 7, 0, 0,
    ... 1, 1, 2, 0, 2, 2, 8, 20, 1, 1, 2, 2, 8], 'regulation': ['ctrl', 'ctrl',
    ... 'ctrl', 'ctrl', 'ctrl', 'SIPP', 'SIPP', 'SIPP', 'SIPP', 'SIPP',
    ... 'ctrl', 'ctrl', 'ctrl', 'ctrl', 'ctrl', 'SIPP', 'SIPP', 'SIPP',
    ... 'SIPP', 'SIPP']})
    >>> get_avg_number_of_peak(d)
      regulation  distance  ctcf_peaks
    0       SIPP    15.007         0.5
    1       SIPP    15.366         0.5
    2       SIPP    15.724         1.5
    3       SIPP    16.083         1.5
    4       SIPP    16.441         5.0
    5       ctrl    15.007         0.0
    6       ctrl    15.366         1.5
    7       ctrl    15.724         1.5
    8       ctrl    16.083         6.0
    9       ctrl    16.441        13.5
    """
    return df.groupby(["regulation", "distance"]).mean().reset_index()


def get_fold_change_dataframe(df: pd.DataFrame, test_group: str,
                              ctrl: str = "ctrl") -> pd.DataFrame:
    """
    :param df: The dataframe with mean ctcf sites number across log2 distance \
    intervals
    :param test_group: The test group used to compute the fold \
    change of the number of site
    :param ctrl: The test ctrl used to compute the fold \
    change of the number of site as the reference
    :return: the dataframe with the fold change in CTCF site number

    >>> d = pd.DataFrame({'regulation': ['SIPP', 'SIPP', 'SIPP', 'SIPP',
    ... 'SIPP', 'ctrl', 'ctrl', 'ctrl', 'ctrl', 'ctrl'],
    ... 'distance': [15.007, 15.366, 15.724, 16.083, 16.441, 15.007, 15.366,
    ... 15.724, 16.083, 16.441], 'ctcf_peaks': [0.5, 0.5, 1.5, 1.5, 5.0, 0.0,
    ... 1.5, 1.5, 6.0, 13.5]})
    >>> get_fold_change_dataframe(d, "SIPP", "ctrl")
       distance  ctcf_peaks regulation
    0    15.007         NaN       SIPP
    1    15.366    0.333333       SIPP
    2    15.724    1.000000       SIPP
    3    16.083    0.250000       SIPP
    4    16.441    0.370370       SIPP
    """
    df_test = df.loc[df["regulation"] == test_group,
                     ["distance", "ctcf_peaks"]].copy()
    df_ctrl = df.loc[df["regulation"] == ctrl,
                     ["distance", "ctcf_peaks"]].copy()
    df = df_test.merge(df_ctrl, how="left", on="distance",
                       suffixes=["_test", "_ctrl"])
    df["ctcf_peaks"] = df["ctcf_peaks_test"] / df["ctcf_peaks_ctrl"]
    df = df.drop(["ctcf_peaks_ctrl", "ctcf_peaks_test"], axis=1)
    df["regulation"] = [test_group] * df.shape[0]
    df.loc[np.isinf(df["ctcf_peaks"]), "ctcf_peaks"] = np.nan
    return df


def make_complete_fold_change_table(df: pd.DataFrame,
                                    ctrl: str = "ctrl") -> pd.DataFrame:
    """

    :param df: The dataframe with mean ctcf sites number across log2 distance \
    intervals
    :param ctrl: The test ctrl used to compute the fold \
    change of the number of site as the reference
    :return: the dataframe with the fold change in mean CTCF site number \
    across different interval near exons of interest

    >>> d = pd.DataFrame({'regulation': ['SIPP', 'SIPP', 'SIPP', 'SIPP',
    ... 'SIPP', 'ctrl', 'ctrl', 'ctrl', 'ctrl', 'ctrl'] + ["SRSF1"] * 5,
    ... 'distance': [15.007, 15.366, 15.724, 16.083, 16.441] * 3,
    ... 'ctcf_peaks': [0.5, 0.5, 1.5, 1.5, 5.0, 0.0,
    ... 1.5, 1.5, 6.0, 13.5, 1, 1, 3, 3, 10]})
    >>> make_complete_fold_change_table(d)
       distance  ctcf_peaks regulation
    0    15.007         NaN       SIPP
    1    15.366    0.333333       SIPP
    2    15.724    1.000000       SIPP
    3    16.083    0.250000       SIPP
    4    16.441    0.370370       SIPP
    5    15.007         NaN      SRSF1
    6    15.366    0.666667      SRSF1
    7    15.724    2.000000      SRSF1
    8    16.083    0.500000      SRSF1
    9    16.441    0.740741      SRSF1
    """
    test_groups = [x for x in df["regulation"].unique() if x != ctrl]
    list_df = [get_fold_change_dataframe(df, test, ctrl)
               for test in test_groups]
    return pd.concat(list_df, axis=0, ignore_index=True)


def make_lineplot(df: pd.DataFrame, side: str, ctrl: str, kind: str,
                  outfile: Path) -> None:
    """

    :param df: the dataframe with the fold change in mean CTCF site number \
    across different interval near exons of interest
    :param side: The side of the exon of interest
    :param ctrl: The test ctrl used to compute the fold \
    change of the number of site as the reference
    :param kind: The kind of exons selected
    :param outfile: The file where the figure will be written
    """
    sns.set(context="poster")
    x_col = "distance"
    y_col = "ctcf_peaks"
    hue_col = "regulation"
    df = df[-df[y_col].isna()].copy()
    df.sort_values(x_col, inplace=True)
    ncolor = {"SIPP": "red", "readthrough": "#CC00CC", "siPP_DOWN": "red",
              "siPP_UP": "blue", "SRSF1": "dimgray", "SRSF1_down": "salmon",
              "SRSF1_up": "dodgerblue"}
    g = sns.relplot(x=x_col, y=y_col, data=df,
                    hue=hue_col, aspect=1.7, height=12,
                    palette=ncolor,  zorder=2, kind="line", markers=True)
    sns.lineplot(x=x_col, y=y_col, data=df, hue=hue_col, palette=ncolor,
                 markers=True, style=hue_col)
    g.fig.subplots_adjust(top=0.95)
    g.fig.suptitle(f"Fold change (vs {ctrl} exons) in CTCF peaks {side} "
                   f"{kind} exons")
    g.set_axis_labels(f"Log2 nucleotide distance {side}",
                      "Fold change in CTCF peaks")
    plt.savefig(outfile)


def get_partial_name(up_and_down: bool,
                     kind: str, factor: str, output: Path, side: str
                     ) -> Tuple[Path, Path]:
    """
    Get the folder that will be used and the figure name that will be produced.

    :param up_and_down: A boolean indicating if we want to study up and \
    down regulated exons
    :param kind: The kind of exons contained in filename. \
    Choose from first and last
    :param factor: The name of the interest factor
    :param output: Folder were the results will be created
    :param side: The side of interest
    :return: The partial name of the figure And the folder where the result \
    will be created
    """
    basename = f'distance_CTCF_exons_si{factor}_siPP'
    if up_and_down:
        basename += '_inclus_exclus'
    etype = kind.replace("first", "premier").replace("last", "terminaux")
    if kind != "":
        basename = basename.replace('CTCF_exons', f'CTCF_{etype}-exons')
    folder = output / basename
    return folder, folder / f"{factor}-SIPP-{kind}_{side}_log2figure.pdf"


def create_complete_figure_table(df: pd.DataFrame, output_folder: Path,
                                 peak_file: Path, side: str, start: int = 200,
                                 stop: int = 100000, nb_interval: int = 25):
    """
    :param df: A dataframe containing exons and their distance to CTCF peaks.
    :param output_folder: Folder where we want to create the input file used \
    to perform bedtools intersections
    :param peak_file: A file containing CTCF peaks
    :param side: The side of the exon of interest
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval in which CTCF sites are counted
    :return: the dataframe with the fold change in mean CTCF site number \
    across different interval near exons of interest

    >>> test = Path(__file__).parents[2] / "test" / "files"
    >>> d = pd.read_csv(test / "internal_test_exon.txt", sep="\\t")
    >>> d = create_group_column(d, "internal", False)
    >>> pf = test / "CTCF_peaks_test.bed"
    >>> create_complete_figure_table(d, Path(__file__).parents[2], pf,
    ... "downstream", nb_interval = 3)
       distance  ctcf_peaks regulation
    0     9.803    1.000000       SIPP
    1    12.792    0.500000       SIPP
    2    15.781    0.166667       SIPP
    >>> create_complete_figure_table(d, Path(__file__).parents[2], pf,
    ... "upstream", nb_interval = 3)
       distance  ctcf_peaks regulation
    0     9.803    0.333333       SIPP
    1    12.792    0.000000       SIPP
    2    15.781    1.000000       SIPP
    """
    exp_df = create_ctcf_log_position_expanded_table(
        df, output_folder, peak_file, side, start, stop, nb_interval)
    exp_df = get_avg_number_of_peak(exp_df)
    return make_complete_fold_change_table(exp_df, "ctrl")


def create_lineplot_figure(filename: Path, folder: Path, factor: str,
                           up_and_down: bool = True, kind: str = "internal",
                           start: int = 200, stop: int = 100000,
                           nb_interval: int = 25,
                           peak_file: Optional[Path] = None):
    """
    Checks if DDX5_17 exons are more often close to CTCF sites than other

    :param filename: file that contains the dataframe with exon and their \
    CTCF exons sites
    :param folder: Folder where the figures will be created
    :param peak_file: A file containing CTCF peaks
    :param factor: The name of the output factor
    :param up_and_down: Say if we want to consider up and down exons separatly
    :param kind: The kind of exons contained in filename. \
    Choose from first and last
    :param start: The number of nucleotide after the exon that corresponds \
    to the stating points of regions for which we want to get the number of \
    CTCF peaks
    :param stop: The number nucleotide after the exon that corresponds \
    to the ending positions of regions for which we want to get the number of \
    CTCF peaks
    :param nb_interval: The number of interval in which CTCF sites are counted
    """
    df = pd.read_csv(filename, sep="\t")
    df = create_group_column(df, kind, up_and_down)
    if peak_file is None:
        peak_file = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"
    for side in ["upstream", "downstream"]:
        output_folder, outfile = get_partial_name(up_and_down, kind, factor,
                                                  folder, side)
        output_folder.mkdir(exist_ok=True, parents=True)
        exp_df = create_complete_figure_table(df, output_folder, peak_file,
                                              side, start, stop, nb_interval)
        exp_df.to_csv(outfile.parent / f"{outfile.stem}.txt", sep="\t")
        make_lineplot(exp_df, side, "ctrl", kind, outfile)


if __name__ == "__main__":
    doctest.testmod()
