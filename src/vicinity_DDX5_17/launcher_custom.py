#!/usr/bin/env python3

"""
Launch every scripts in vicinity folder with a custom pic to exon distance file
and a custom file containing last exons
"""

from pathlib import Path

import lazyparser as lp
from rpy2.rinterface_lib.embedded import RRuntimeError

from .create_ASE_factor_file import create_exon_file
from .create_terminal_exon_file import create_exon_first_or_last_table
from .launcher_function import ConfCustom
from .merge_ctrl_and_sipp import main_merge_custom
from .statistical_analysis import main_stat


@lp.parse
def launcher(
    distance_file: str,
    factor_name: str,
    exon_list: str,
    name_exon: str,
    rm_nointernal_gene: bool = True,
):
    """
    Test if exons regulated by DDX5/17 or another `control factor`, \
    have a different closeness to some factor peaks

    :param distance_file: The distance between exon and factor_name peacks
    :param factor_name: The name of the factor studied
    :param exon_list: A filme containing custom regulated exon list \
    on which we want to perform the analysis
    :param name_exon: The name of the list of exons given with exon_list \
    parameter
    :param rm_nointernal_gene: Remove genes without internal exons. Default \
    to True
    """
    distance_filep = Path(distance_file)
    conf = ConfCustom(
        distance_filep, name_exon, factor_name, rm_nointernal_gene
    )
    if not conf.aef.is_file():
        main_merge_custom(
            conf.aef,
            conf.iaf,
            Path(exon_list),
            rm_gene_with_internal_exon=rm_nointernal_gene,
        )
    if not conf.ol.is_file():
        create_exon_first_or_last_table(
            conf.aef,
            conf.ol,
            kind="last",
            remove_genes_with_no_internal_exons=rm_nointernal_gene,
        )
    thresholds = [0, 2000]
    for t in thresholds:
        try:
            main_stat(
                conf.ol,
                conf.output_folder,
                factor=name_exon,
                threshold=t,
                terminal="regulation",
                kind="last",
                factor_name=factor_name,
            )
        except RRuntimeError:
            pass


if __name__ == "__main__":
    launcher()  # pylint: disable=no-value-for-parameter
