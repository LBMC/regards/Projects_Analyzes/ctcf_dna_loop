#!/usr/bin/env python3

"""
Launch every scripts in vicinity folder
"""

from .merge_ctrl_and_sipp import main_merge_custom
from .statistical_analysis import main_stat
from pathlib import Path
from .create_terminal_exon_file import create_exon_first_or_last_table
import lazyparser as lp
import datetime
from .ctcf_logdist import create_lineplot_figure
from .launcher_function import execute_logf, ConfFile, logfigure

# Executing every functions ...

@lp.parse()
def launcher(custom_file: str, group_name: str, logf: bool):
    """
    Test if exons regulated by DDX5/17 or another `control factor`, \
    have a different closeness to CTCF peaks

    :param control: The control factor we want to use
    """
    conf = ConfFile(group_name, True)
    if not conf.ief.is_file():
        main_merge_custom(conf.ief, conf.iif,
                          Path(custom_file), remove_first_and_last=True)
    if not conf.aef.is_file():
        main_merge_custom(conf.aef, conf.iaf, Path(custom_file))
    if not conf.ol.is_file():
        create_exon_first_or_last_table(conf.aef, conf.ol,
                                        kind="last")
    if not conf.of.is_file():
        create_exon_first_or_last_table(conf.aef, conf.of,
                                        kind="first")
    if logf:
        # execute_logf(conf, group_name)
        logfigure(conf, group_name)
    thresholds = [0, 2000]
    for t in thresholds:
        # main_stat(all_exon_file, output_folder, factor=group_name,
        #           threshold=t)
        main_stat(conf.ol, conf.output_folder, factor=group_name, threshold=t,
                  terminal='regulation', kind="last")
        main_stat(conf.of, conf.output_folder, factor=group_name, threshold=t,
                  terminal='regulation', kind="first")


launcher()
