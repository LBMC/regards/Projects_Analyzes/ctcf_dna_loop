#!/usr/bin/env python3

"""
Launch every scripts in vicinity folder
"""

from .merge_ctrl_and_sipp import main_merge
from .statistical_analysis import main_stat
from .create_terminal_exon_file import create_exon_first_or_last_table
import lazyparser as lp
from .create_ASE_factor_file import create_exon_file
from .launcher_function import ConfCustomFile
from pathlib import Path
from rpy2.rinterface_lib.embedded import RRuntimeError


@lp.parse
def launcher(control_file: str, factor_name: str = "CTCF"):
    """
    Test if exons regulated by DDX5/17, \
    have a different closeness to some factor peaks

    :param control_file: The control factor we want to use
    :param factor_name: The name of the factor studied
    """
    control = "None"
    control_file = Path(control_file)
    name = "-".join(control_file.stem.split("_")[:3])
    create_exon_file(control)
    conf = ConfCustomFile(control_file, name)
    if not conf.ief.is_file():
        main_merge(conf.ief, conf.iif, factor=control,
                   remove_first_and_last=True)
    if not conf.aef.is_file():
        main_merge(conf.aef, conf.iaf, factor=control)
    if not conf.ol.is_file():
        create_exon_first_or_last_table(conf.aef, conf.ol,
                                        kind="last")
    if not conf.of.is_file():
        create_exon_first_or_last_table(conf.aef, conf.of,
                                        kind="first")
    thresholds = [0, 2000]
    for t in thresholds:
        try:
            main_stat(conf.ief, conf.output_folder, factor=control,
                      threshold=t, factor_name=factor_name)
        except RRuntimeError:
            pass
        try:
            main_stat(conf.ief, conf.output_folder, factor=control,
                      threshold=t, up_and_down=False, factor_name=factor_name)
        except RRuntimeError:
            pass
        try:
            main_stat(conf.ol, conf.output_folder, factor=control, threshold=t,
                      terminal='regulation', kind="last",
                      factor_name=factor_name)
        except RRuntimeError:
            pass
        try:
            main_stat(conf.of, conf.output_folder, factor=control, threshold=t,
                      terminal='regulation', kind="first",
                      factor_name=factor_name)
        except RRuntimeError:
            pass
        try:
            main_stat(conf.ol, conf.output_folder, factor=control, threshold=t,
                      terminal='regulation', kind="last",
                      factor_name=factor_name)
        except RRuntimeError:
            pass
        try:
            main_stat(conf.of, conf.output_folder, factor=control, threshold=t,
                      terminal='regulation', kind="first",
                      factor_name=factor_name)
        except RRuntimeError:
            pass


if __name__ == "__main__":
    launcher()
