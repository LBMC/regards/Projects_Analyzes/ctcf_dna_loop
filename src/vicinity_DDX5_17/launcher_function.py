#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description:
"""

import datetime
from pathlib import Path

from .create_distance2peak_dsitribution import create_distance_figure
from .ctcf_logdist import create_lineplot_figure


class ConfFile:
    def __init__(self, factor: str, custom: bool = False):
        self.result = Path(__file__).parents[2] / "result"
        self.input_folder = self.result / "CTCF_distance_files"
        self.outfold = self.result / "vinity_DDX5_17"
        if not custom:
            self.ief = (
                self.outfold
                / f"distance_CTCF_internal_n_SIPP_exons_{factor}.csv"
            )
            self.aef = (
                self.outfold / f"distance_CTCF_ALL_n_SIPP_exons_{factor}.csv"
            )
        else:
            self.ief = (
                self.outfold / f"distance_CTCF_internal_n_{factor}_exons.csv"
            )
            self.aef = self.outfold / f"distance_CTCF_ALL_n_{factor}_exons.csv"
        self.iif = self.input_folder / "internal_exon_vs_CTCF_orientation.csv"
        self.iaf = self.input_folder / "All_Exons_vs_CTCF_orientation.csv"
        self.ol = self.outfold / f"terminal_exons_{factor}.csv"
        self.of = self.outfold / f"first_exons_{factor}.csv"
        self.fold_name = f"{factor}_" + datetime.datetime.now().strftime(
            "%d-%m-%Y-%H:%M"
        )
        self.output_folder = self.outfold / self.fold_name
        self.pf = Path(__file__).parents[2] / "data" / "CTCF_merged.bed"


class ConfCustomFile:
    def __init__(self, distance_file: Path, name: str = "MYCN"):
        self.result = Path(__file__).parents[2] / "result"
        self.data = Path(__file__).parents[2] / "data"
        self.input_folder = distance_file.parent
        self.outfold = self.result / "vinity_DDX5_17"
        self.ief = self.outfold / f"distance_{name}_internal_n_None_exons.csv"
        self.aef = self.outfold / f"distance_{name}_ALL_n_None_exons.csv"
        self.iif = self.input_folder / f"internal_{distance_file.name}"
        self.iaf = distance_file
        self.ol = self.outfold / f"terminal_exons_{name}_None.csv"
        self.of = self.outfold / f"first_exons_{name}_None.csv"
        self.fold_name = (
            f"distance_{name}_"
            + datetime.datetime.now().strftime("%d-%m-%Y-%H:%M")
        )
        self.output_folder = self.outfold / self.fold_name


class ConfCustom:
    def __init__(
        self,
        distance_file: Path,
        exon_name: str,
        factor: str = "MYCN",
        rm_internal_genes: bool = True,
    ):
        self.result = Path(__file__).parents[2] / "result"
        self.data = Path(__file__).parents[2] / "data"
        self.input_folder = distance_file.parent
        nn = "" if rm_internal_genes else "_winternex"
        self.outfold = self.result / "vinity_DDX5_17"
        self.ief = (
            self.outfold
            / f"distance_{factor}_internal_n_{exon_name}_exons{nn}.csv"
        )
        self.aef = (
            self.outfold
            / f"distance_{factor}_ALL_n_{exon_name}_exons{nn}.csv"
        )
        self.iif = self.input_folder / f"internal_{distance_file.name}{nn}"
        self.iaf = distance_file
        self.ol = (
            self.outfold / f"terminal_exons_{factor}_{exon_name}{nn}.csv"
        )
        self.of = self.outfold / f"first_exons_{factor}_{exon_name}{nn}.csv"
        self.fold_name = (
            f"distance_{factor}_"
            + datetime.datetime.now().strftime("%d-%m-%Y-%H:%M")
        )
        self.output_folder = self.outfold / self.fold_name


def execute_logf(conf: ConfFile, control: str) -> None:
    """
    execute function to get the fold change of CTCF sites in function of \
    the distance to the exon.

    :param conf: A class containing files and folder
    :param control: The control used
    """
    create_lineplot_figure(
        conf.ol,
        conf.output_folder,
        control,
        up_and_down=False,
        kind="last",
        peak_file=conf.pf,
    )
    create_lineplot_figure(
        conf.ol,
        conf.output_folder,
        control,
        up_and_down=True,
        kind="last",
        peak_file=conf.pf,
    )
    create_lineplot_figure(
        conf.of,
        conf.output_folder,
        control,
        up_and_down=False,
        kind="first",
        peak_file=conf.pf,
    )
    create_lineplot_figure(
        conf.of,
        conf.output_folder,
        control,
        up_and_down=True,
        kind="first",
        peak_file=conf.pf,
    )
    create_lineplot_figure(
        conf.ief,
        conf.output_folder,
        control,
        up_and_down=True,
        kind="internal",
        peak_file=conf.pf,
    )
    create_lineplot_figure(
        conf.ief,
        conf.output_folder,
        control,
        up_and_down=False,
        kind="internal",
        peak_file=conf.pf,
    )


def logfigure(conf: ConfFile, factor: str, custom: bool = False) -> None:
    """
    Create a figure that show the distribution of the distance to the closest \
    CTCF sites downstream or upstream exons in log2 scale.

    :param conf:  A class containing files and folder
    :param factor: The factor used
    :param custom: Create only the figure needed with a custom input file
    """
    create_distance_figure(
        conf.ol, conf.output_folder, factor, up_and_down=False, kind="last"
    )
    create_distance_figure(
        conf.of, conf.output_folder, factor, up_and_down=False, kind="first"
    )
    if not custom:
        create_distance_figure(
            conf.ief,
            conf.output_folder,
            factor,
            up_and_down=True,
            kind="internal",
        )
