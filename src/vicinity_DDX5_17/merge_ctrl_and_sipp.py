#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Description:
    The goal of this script is to merge the dataframe of distances to \
    CTCF sites for all exons and the one for sipp exons
"""

import logging
import re
from pathlib import Path
from typing import List, Tuple, Union

import pandas as pd

from .create_terminal_exon_file import format_exon_bed


class GeneNotFound(Exception):
    pass


def update_new_group(row: pd.Series, factor: str) -> str:
    """
    Update the new group value of a row.

    :param row: A row of a dataframe of exons
    :param factor: The factor that will be used as control
    :return: The new group
    """
    if pd.isna(row.regulation):
        return row.new_group
    elif factor in row.regulation and row.new_group == "ctrl":
        return row.regulation
    elif factor in row.regulation:
        return "multiple"


def merge_dataframe(
    df_ctrl: pd.DataFrame,
    df_sipp: pd.DataFrame,
    df_factor: pd.DataFrame,
    factor: str,
) -> pd.DataFrame:
    """
    Merge df_ctrl and df_custom and add another columns to clearly define \
    the control exons to the one of interest.

    :param df_ctrl: Dataframe of distances to CTCF binding sites for control \
    exons
    :param df_sipp: Dataframe of distance to CTCF binding sites for sipp exons.
    :param df_factor: Dataframe of distance to CTCF binding sites for \
    snrnp70 exons.
    :param factor: The factro that will be used as control
    :return: The merged dataframe
    """
    df_ctrl = df_ctrl.loc[df_ctrl["group"] != "First_and_Last"]
    df_sipp = df_sipp[["exon_name", "exon_id", "group", "strand", "deltaPSI"]]
    common_cols = ["exon_name", "exon_id", "group", "strand"]
    df = df_ctrl.merge(df_sipp, how="left", on=common_cols)
    df.loc[df["group"].isna(), "group"] = "Undef"
    logging.debug(df.loc[pd.notna(df.deltaPSI), :].head())
    df["new_group"] = df.apply(
        lambda x: x["group"]
        if "siPP" in x.group and abs(x.deltaPSI) >= 0.1
        else "ctrl",
        axis=1,
    )
    if "exon_id" in df_factor.columns:
        df = df.merge(df_factor, how="left", on=["exon_name", "exon_id"])
    else:
        df = df.merge(df_factor, how="left", on="exon_name")
    r = df_factor.loc[~df_factor["exon_name"].isin(df["exon_name"]), :]
    logging.warning(
        f"{len(r)} exons were found in df_factor and not in " f"df_ctrl"
    )
    logging.debug("second merge")
    logging.debug(df.loc[pd.notna(df.deltaPSI), :].head())
    df["new_group"] = df.apply(update_new_group, factor=factor, axis=1)
    logging.debug(df.head())
    return df


def merge_cutsom_dataframe(
    df_ctrl: pd.DataFrame,
    df_custom: pd.DataFrame,
    rm_gene_with_internal_exon: bool = True,
) -> pd.DataFrame:
    """
    Merge df_ctrl and df_custom and add another columns to clearly define \
    the control exons to the one of interest.

    :param df_ctrl: Dataframe of distances to CTCF binding sites for control \
    exons
    :param df_custom: A dataframe containing a custom list of exons
    :param rm_gene_with_internal_exon: Remove gene with internal exons,
    defaults to true
    :param factor: The factro that will be used as control
    :return: The merged dataframe
    """
    if "group_exon" not in df_custom.columns:
        raise ValueError("df_custom should contains a column 'group_exon'")
    if rm_gene_with_internal_exon:
        df_ctrl = df_ctrl.loc[df_ctrl["group"] != "First_and_Last"]
    common_cols = ["exon_name"]
    df = df_ctrl.merge(df_custom, how="left", on=common_cols)
    logging.debug(df.shape)
    df.drop_duplicates(subset="exon_name", keep=False)
    logging.debug(df.shape)
    df.rename({"group_exon": "new_group"}, axis=1, inplace=True)
    df["new_group"] = df["new_group"].fillna("ctrl")
    r = df_custom.loc[~df_custom["exon_name"].isin(df["exon_name"]), :]
    logging.warning(
        f"{len(r)} exons were found in df_custom and not in " f"df_ctrl"
    )
    return df


def add_exonid_column(df: pd.DataFrame) -> pd.DataFrame:
    """
    Add a column exonid in the merged dataframe

    :param df: The merged dataframe
    :return:
    """
    exon_df = format_exon_bed()
    df["gene_name"] = df["exon_name"].str.replace(r"_\d+", "")
    df["exon_pos"] = df["exon_name"].str.extract(r"_(\d+)").astype(int)
    df = df.merge(exon_df, how="left", on=["exon_id", "gene_name", "exon_pos"])
    return df.drop(["gene_name", "exon_pos"], axis=1)


def contained(exon: List[Union[str, int]], gene: pd.Series) -> bool:
    """
    Check if ``exon`` is contained in ``gene``.

    :param exon: An exon
    :param gene: A gene
    :return: True of False
    """
    return (
        exon[0] == gene.chr
        and gene.start <= exon[1]
        and gene.stop >= exon[2]
        and gene.strand == exon[3]
    )


def get_distance(exon: List[Union[int, str]], gene: pd.Series) -> int:
    """
    Compute the distance to promotor.

    :param exon: An exon
    :param gene: A gene
    :return: The distance to promotor
    """
    if gene.strand == "+":
        return exon[1] - gene.start + 1
    else:
        return gene.stop - exon[2] + 1


def compute_promotor_distance(exon: pd.Series, bed: pd.DataFrame) -> int:
    """
    For an exon compute the distance to the promoter.

    :param exon: An exon corresponding to a dataframe row
    :param bed: A dataframe of fasterdb gene
    :return: The distance to the promoter of exon
    """
    if "RP4-695O20__B.10" not in exon.exon_name:
        exon_gene = exon.exon_name.split("_")[0]
    else:
        exon_gene = "RP4-695O20__B.10"
    df = bed.loc[bed.name == exon_gene, :]
    if df.empty:
        logging.exception(f"The gene {exon_gene} wasn't found in fasterDb!")
        raise GeneNotFound(f"The gene {exon_gene} wasn't found !")
    for i in range(len(df)):
        exon_coord = re.split(r":|-", exon.exon_id) + [exon.strand]
        exon_coord[1] = int(exon_coord[1]) - 1
        exon_coord[2] = int(exon_coord[2])
        gene = df.iloc[i, :]
        if contained(exon_coord, gene):
            return get_distance(exon_coord, gene)
    raise GeneNotFound(f"No gene with find for {exon.exon_name}")


def add_distance2promoter(df: pd.DataFrame, bed: pd.DataFrame) -> pd.DataFrame:
    """
    Add a column promoter_distance to ``df``.

    :param df: Dataframe of distance to CTCF sites for fasterDB exons
    :param bed: A bed file containing fasterDB gene
    :return: The dataframe ``df`` with *promoter_distance* column
    """
    df["promoter_distance"] = df.apply(
        lambda x: compute_promotor_distance(x, bed), axis=1
    )
    logging.debug(df.head())
    return df


def get_last_exon(
    kind="score", get_df: bool = False
) -> Tuple[List[str], pd.DataFrame] | List[str]:
    """
    Get the name of the last exon for every human gene.

    :param kind: score to get the exon id with its gene symbol or \
    'id' to get the exon id with its gene id
    :return: The list of last exons name on the form of gene_symbol_position \
    and a dataframe containing bed exon data and a column name
    """
    exon_bed = Path(__file__).parents[2] / "data" / "exon.bed"
    df_exon = pd.read_csv(exon_bed, sep="\t")
    df_exon["pos"] = df_exon.id.str.replace(r"\d+_", "", regex=True)
    df_exon["id"] = df_exon.id.str.replace(r"_\d+", "", regex=True)
    df_exon["id"] = df_exon["id"].astype(int)
    df_exon["pos"] = df_exon["pos"].astype(int)
    df_exon = df_exon.sort_values(
        ["id", "pos"], ascending=[True, False]
    ).drop_duplicates(subset=["id"], keep="first")
    df_exon = df_exon.drop("score", axis=1)
    gene_bed = Path(__file__).parents[2] / "data" / "gene.bed"
    df_gene = pd.read_csv(gene_bed, sep="\t")[["id", "score"]]
    df = df_exon.merge(df_gene, how="left", on="id")
    df["name"] = df[kind].astype(str) + "_" + df["pos"].astype(str)
    if get_df:
        return list(df["name"].values), df
    return list(df["name"].values)


def main_merge(
    filename: Path,
    input_file: Path,
    factor: str = "SNRNP70",
    remove_first_and_last=False,
    use_sipp: bool = True,
):
    """
    Adapt distance tables

    :param filename: The result file
    :param input_file: The input file
    :param factor: The factro that will be used as control
    :param use_sipp: A boolean indicating if we want to use a
    """
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    base_dir = Path(__file__).parents[2]
    factor_file = base_dir / "data" / f"ASE_{factor}.csv"
    sipp = (
        base_dir / "result" / "CTCF_distance_files" / "Sipp_exon_vs_CTCF.csv"
    )
    df_sipp = pd.read_csv(sipp, sep="\t")
    logging.info("Merging dataframe")
    if not use_sipp:
        df_sipp = pd.DataFrame(columns=df_sipp.columns)
    df = merge_dataframe(
        pd.read_csv(input_file, sep="\t"),
        df_sipp,
        pd.read_csv(factor_file, sep="\t"),
        factor,
    )
    if remove_first_and_last:
        # remove first exons
        df = df[-df.exon_name.str.contains("_1$")]
        # remove last exons
        last_exon = get_last_exon()
        logging.info("removing last exons...")
        logging.debug(f"shape before removing {df.shape}")
        logging.debug(last_exon[0:5])
        logging.debug(df[df["exon_name"].isin(last_exon)])
        df = df[-df["exon_name"].isin(last_exon)]
        logging.debug(f"shape after removing {df.shape}")
    logging.info("Writting results...")
    df.to_csv(filename, sep="\t", index=False)


def remove_genes_with_no_internal_exons(
    df: pd.DataFrame, rm_gene_with_internal_exon: bool
):
    """
    update df by removing genes without internal exons

    :param df: A dataframe containing genes and exons
    :param rm_gene_with_internal_exon: Remove gene with internal exons,
    defaults to true
    :return: The update df
    """
    # remove exons in gene with no internal exons
    df = add_exonid_column(df)
    # elimination des exons sans exons internes
    if rm_gene_with_internal_exon:
        bad_gene = [
            int(i.split("_")[0])
            for i in get_last_exon("id")
            if int(i.split("_")[1]) <= 2
        ]
        df = df[-df["gene_id"].isin(bad_gene)].copy()
    df.drop(["id", "gene_id", "last_fasterDB_exon"], axis=1, inplace=True)
    return df


def main_merge_custom(
    filename: Path,
    input_file: Path,
    custom_file: Path,
    remove_first_and_last=False,
    rm_gene_with_internal_exon=True,
):
    """
    Adapt distance tables

    :param filename: The result file
    :param input_file: The input file
    :param rm_gene_with_internal_exon: Remove gene with internal exons,
    defaults to true
    """
    logging.basicConfig(level=logging.DEBUG, format="%(message)s")
    logging.info("Merging dataframe")

    df = merge_cutsom_dataframe(
        pd.read_csv(input_file, sep="\t"),
        pd.read_csv(custom_file, sep="\t"),
        rm_gene_with_internal_exon,
    )
    df = remove_genes_with_no_internal_exons(df, rm_gene_with_internal_exon)
    if remove_first_and_last:
        # remove first exons
        df = df[-df.exon_name.str.contains("_1$")]
        # remove last exons
        last_exon = get_last_exon()
        logging.info("removing last exons...")
        logging.debug(f"shape before removing {df.shape}")
        logging.debug(last_exon[:5])
        logging.debug(df[df["exon_name"].isin(last_exon)])
        df = df[-df["exon_name"].isin(last_exon)]
        logging.debug(f"shape after removing {df.shape}")
    logging.info("Writting results...")
    df.to_csv(filename, sep="\t", index=False)
