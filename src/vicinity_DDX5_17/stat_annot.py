#!/usr/bin/env python3

# -*- coding;: utf-8 -*-

"""
Description:
    Add annotation on figure.
    Script adapted from
    github.com/webermarcolivier/statannot/blob/master/statannot/statannot.py.
"""


import matplotlib.pyplot as plt
from matplotlib import lines
import matplotlib.transforms as mtransforms
from matplotlib.font_manager import FontProperties
import numpy as np
import seaborn as sns
from seaborn.utils import remove_na


def add_stat_annotation(ax, data=None, x=None, y=None, hue=None, order=None,
                        hue_order=None, boxPairList=None, loc='inside',
                        useFixedOffset=False, lineYOffsetToBoxAxesCoord=None,
                        lineYOffsetAxesCoord=None, lineHeightAxesCoord=0.02,
                        textYOffsetPoints=1, color='0.2', linewidth=1.5,
                        fontsize='medium', verbose=1):
    """
    User should use the same argument for the data, x, y, hue, order, \
    hue_order as the seaborn boxplot function.
    boxPairList can be of either form:
    boxplot: [(cat1, cat2, pval), (cat3, cat4, pval)]
    """

    def find_x_position_box(boxPlotter, boxName):
        """
        boxName can be either a name "cat" or a tuple ("cat", "hue")
        """
        if boxPlotter.plot_hues is None:
            cat = boxName
            hueOffset = 0
        else:
            cat = boxName[0]
            hue =  boxName[1]
            hueOffset = boxPlotter.hue_offsets[
                boxPlotter.hue_names.index(hue)]

        groupPos = boxPlotter.group_names.index(cat)
        boxPos = groupPos + hueOffset
        return boxPos


    def get_box_data(boxPlotter, boxName):
        """
        boxName can be either a name "cat" or a tuple ("cat", "hue")
        Here we really have to duplicate seaborn code, because there is not
        direct access to the
        box_data in the BoxPlotter class.
        """
        cat = boxName
        if boxPlotter.plot_hues is None:
            box_max_l = []
            for cat in boxPlotter.group_names:
                i = boxPlotter.group_names.index(cat)
                group_data = boxPlotter.plot_data[i]
                box_data = remove_na(group_data)
                box_max_l.append(np.max(box_data))
            return max(box_max_l)
        else:
            i = boxPlotter.group_names.index(cat[0])
            group_data = boxPlotter.plot_data[i]
            box_data = []
            for hue in boxPlotter.hue_names:
                hue_mask = boxPlotter.plot_hues[i] == hue
                box_data.append(np.max(remove_na(group_data[hue_mask])))
            return max(box_data)

    fig = plt.gcf()

    validList = ['inside', 'outside']
    if loc not in validList:
        raise ValueError(f"loc value should be one of the following: "
                         f"{', '.join(validList)}.")

    # Create the same BoxPlotter object as seaborn's boxplot
    boxPlotter = sns.categorical._BoxPlotter(x, y, hue, data, order, hue_order,
                                             orient=None, width=.8, color=None,
                                             palette=None, saturation=.75,
                                             dodge=True, fliersize=5,
                                             linewidth=None)
    ylim = ax.get_ylim()
    yRange = ylim[1] - ylim[0]

    if lineYOffsetAxesCoord is None:
        if loc == 'inside':
            lineYOffsetAxesCoord = 0.005
            if lineYOffsetToBoxAxesCoord is None:
                lineYOffsetToBoxAxesCoord = 0.1
        elif loc == 'outside':
            lineYOffsetAxesCoord = 0.03
            lineYOffsetToBoxAxesCoord = lineYOffsetAxesCoord
    else:
        if loc == 'inside':
            if lineYOffsetToBoxAxesCoord is None:
                lineYOffsetToBoxAxesCoord = 0.06
        elif loc == 'outside':
            lineYOffsetToBoxAxesCoord = lineYOffsetAxesCoord
    yOffset = lineYOffsetAxesCoord*yRange
    yOffsetToBox = lineYOffsetToBoxAxesCoord*yRange

    yStack = []
    annList = []
    for box1, box2, pval in boxPairList:

        groupNames = boxPlotter.group_names
        cat1 = box1
        cat2 = box2
        if isinstance(cat1, tuple):
            hue_names = boxPlotter.hue_names
            valid = cat1[0] in groupNames and cat2[0] in groupNames and \
                cat1[1] in hue_names and cat2[1] in hue_names
        else:
            valid = cat1 in groupNames and cat2 in groupNames

        if valid:
            # Get position of boxes
            x1 = find_x_position_box(boxPlotter, box1)
            x2 = find_x_position_box(boxPlotter, box2)
            box_data1 = get_box_data(boxPlotter, box1)
            box_data2 = get_box_data(boxPlotter, box2)
            ymax1 = box_data1
            ymax2 = box_data2

            if pval > 1e-16:
                text = "p = {:.2e}".format(pval)
            else:
                text = "p < 1e-16"

            if loc == 'inside':
                yRef = max(ymax1, ymax2)
            else:
                yRef = ylim[1]
            if len(yStack) > 0:
                yRef2 = max(yRef, max(yStack))
            else:
                yRef2 = yRef

            if len(yStack) == 0:
                y = yRef2 + yOffsetToBox
            else:
                y = yRef2 + yOffset
            h = lineHeightAxesCoord*yRange
            lineX, lineY = [x1, x1, x2, x2], [y, y + h, y + h, y]
            if loc == 'inside':
                ax.plot(lineX, lineY, lw=linewidth, c=color)
            elif loc == 'outside':
                line = lines.Line2D(lineX, lineY, lw=linewidth, c=color, transform=ax.transData)
                line.set_clip_on(False)
                ax.add_line(line)

            if text is not None:
                ann = ax.annotate(text, xy=(np.mean([x1, x2]), y + h),
                                  xytext=(0, textYOffsetPoints), textcoords='offset points',
                                  xycoords='data', ha='center', va='bottom', fontsize=fontsize,
                                  clip_on=False, annotation_clip=False)
                annList.append(ann)

            new_max_ylim = 1.1*(y + h)
            if new_max_ylim > ylim[1]:
                ax.set_ylim((ylim[0], 1.1*(y + h)))

            if text is not None:
                plt.draw()
                yTopAnnot = None
                gotMatplotlibError = False
                if not useFixedOffset:
                    try:
                        bbox = ann.get_window_extent()
                        bbox_data = bbox.transformed(ax.transData.inverted())
                        yTopAnnot = bbox_data.ymax
                    except RuntimeError:
                        gotMatplotlibError = True

                if useFixedOffset or gotMatplotlibError:
                    if verbose >= 1:
                        print("Warning: cannot get the text bounding box. "
                              "Falling back to a fixed y offset. Layout may "
                              "be not optimal.")
                    # We will apply a fixed offset in points, based on the font size of the annotation.
                    fontsizePoints = FontProperties(size='medium').get_size_in_points()
                    offsetTrans = mtransforms.offset_copy(ax.transData, fig=fig,
                                                          x=0, y=1.0 * fontsizePoints + textYOffsetPoints, units='points')
                    yTopDisplay = offsetTrans.transform((0, y + h))
                    yTopAnnot = ax.transData.inverted().transform(yTopDisplay)[1]
            else:
                yTopAnnot = y + h

            yStack.append(yTopAnnot)
        else:
            raise ValueError("boxPairList contains an unvalid box pair.")

    yStackMax = max(yStack)
    if loc == 'inside':
        if ylim[1] < 1.03*yStackMax:
            ax.set_ylim((ylim[0], 1.03*yStackMax))
    elif loc == 'outside':
        ax.set_ylim((ylim[0], ylim[1]))
    return ax
