#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""
This scrip aims to make statistical analysis to test whether DDX5_17 exons \
are closer to CTCF sites than control exons
"""

from pathlib import Path
from typing import List, Optional, Tuple

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from rpy2.robjects import pandas2ri, r

from .create_distance2peak_dsitribution import NCOLOR
from .stat_annot import add_stat_annotation


def get_partial_name(
    threshold: int,
    up_and_down: bool,
    terminal: Optional[str],
    kind: Optional[str],
    factor: str,
    output: Path,
    rm0: bool,
    factor_name: str = "CTCF",
) -> Tuple[Path, str]:
    """
    Get the partial name that will be used for the figure name.

    :param threshold: a distance threshold
    :param terminal: If not None, this function checks if gene having one \
    exons regulated by DDX5_17 have their terminal \
    exons more often contains CTCF binding sites than \
    other exons. Possible values are None, uniq_regulation, regulation
    :param kind: The kind of exons contained in filename. \
    Choose from first and last
    :param factor: The name of the interest factor
    :param output: Folder were the results will be created
    :param rm0: True to remove exons containing CTCF of the analysis
    :param factor_name: The name of the main factor analysed
    :return: The partial name of the figure And the folder where the result \
    will be created
    """
    basename = f"distance_{factor_name}_exons_si{factor}_siPP"
    if up_and_down and terminal is None and kind is None:
        basename += "_inclus_exclus"
    if rm0:
        basename = basename.replace(
            "exons", f"exons-sans-{factor_name}-chevauchant"
        )
    if terminal is None:
        folder = output / basename
        return folder, f"{factor}_{threshold}"
    elif "uniq" in terminal:
        etype = kind.replace("first", "premier").replace("last", "terminaux")
        basename = (
            basename.replace(
                f"{factor_name}_exons", f"{factor_name}_{etype}-exons"
            )
            + "_inclus_exclus"
        )
        folder = output / basename
        return folder, f"{factor}_{threshold}-{kind}-uniq"
    else:
        etype = kind.replace("first", "premier").replace("last", "terminaux")
        basename = basename.replace(
            f"{factor_name}_exons", f"{factor_name}_{etype}-exons"
        )
        folder = output / basename
        return folder, f"{factor}_{threshold}-{kind}"


def glm_binomial_stat(
    df: pd.DataFrame,
    output: Path,
    threshold: int,
    terminal: Optional[str],
    partial_name: str,
) -> pd.DataFrame:
    """Perform a glm binomial test on ``df``

    :param df: A dataframe of exons and the distance to the closest CTCF \
    binding site
    :param output: The result folder
    :param threshold: a threshold
    :param partial_name: thee figure partial name
    :param terminal: If not None, this function checks if gene having one \
    exons regulated by DDX5_17 have their terminal \
    exons more often contains CTCF binding sites than \
    other exons. Possible values are None, uniq_regulation, regulation
    :return The tukey's test results
    """
    rep_col = "new_group" if terminal is None else terminal
    tmp = df[["zdist", rep_col]].copy()
    pandas2ri.activate()
    stat_s = r(
        """
    # require("DHARMa")
    require("emmeans")
    function(data, output, threshold, partial_name){
        data$%s <- as.factor(data$%s)
        mod <- glm(zdist ~ %s, family="binomial", data=data)
        # simulationOutput <- simulateResiduals(fittedModel = mod, n = 2000)
        # png(paste0(output, "/dignostics_", partial_name, ".png"))
        # plot(simulationOutput)
        # dev.off()
        comp <- emmeans(mod, ~ %s)
        tab <- as.data.frame(pairs(comp))
        tab <- tab[, c('contrast', 'estimate', 'SE', 'z.ratio', 'p.value')]
        colnames(tab) <- c("Comparison", "Estimate", "Std.Error",
                           "z.ratio", "p.value")
        return(tab)
    }
    """
        % (rep_col, rep_col, rep_col, rep_col)
    )
    res = stat_s(tmp, str(output), threshold, partial_name)
    return res


def append_prop(
    df: pd.DataFrame,
    filename: Path,
    threshold: int,
    terminal: Optional[str],
    factor_name: str = "CTCF",
) -> None:
    """
    Add proportion of close CTCFs, sites for different kind of exons.

    :param df: dataframe with exon and their CTCF exons sites
    :param filename: file were the results will be written
    :param threshold: the distance in nucleotide between an CTCF biding site \
    and the exon
    :param terminal: If not None, this function checks if gene having one \
    exons regulated by DDX5_17 have their terminal \
    exons more often contains CTCF binding sites than \
    other exons. Possible values are None, uniq_regulation, regulation
    :param factor_name: The name of the main factor studied
    """
    rep_col = "new_group" if terminal is None else terminal
    exon_type = df[rep_col].unique()
    with filename.open(mode="a") as f:
        for ctype in exon_type:
            print(ctype)
            proxi = len(df.loc[(df[rep_col] == ctype) & (df["zdist"] == 1), :])
            # print(proxi)
            dist = len(df.loc[(df[rep_col] == ctype), :])
            # print(dist)
            res = round(proxi / dist * 100, 2)
            print(proxi, "/", dist, "=", res)
            f.write(
                f"{res} % of {ctype} exons with a {factor_name} site far "
                f"from at most {threshold} nucleotide\n"
            )


def barplot_maker(
    df: pd.DataFrame,
    output: Path,
    threshold: int,
    side: str,
    stats: List[List],
    terminal: Optional[str],
    partial_name: str,
    factor_name: str = "CTCF",
):
    """
    Create a barplot showing The percentage of close exons between \
    ctrl siPP_DOWN and siPP_UP.

    :param df: dataframe with exon and their \
    CTCF exons sites
    :param output: Folder were the result will be created
    :param threshold: a threshold
    :param side: the side of interest
    :param stats: A list containing sublist. Each sublist contains \
    the name of the groups analyzed along whith their p-value
    :param partial_name: The partial name of the figure
    :param terminal: If not None, this function checks if gene having one \
    exons regulated by DDX5_17 have their terminal \
    exons more often contains CTCF binding sites than \
    other exons. Possible values are None, uniq_regulation, regulation
    :param factor_name: The name of the main factor studied
    """
    sns.set()
    sns.set_context("poster")
    rep_col = "new_group" if terminal is None else terminal
    df2 = df[[rep_col, "zdist", "group"]].pivot_table(
        index=rep_col, columns="zdist", aggfunc="count"
    )
    df2.columns = df2.columns.get_level_values(1)
    df2["prop"] = (
        0 if 1 not in df2.columns else df2[1] / (df2[0] + df2[1]) * 100
    )
    df2.reset_index(inplace=True)
    g = sns.catplot(
        x=rep_col,
        y="prop",
        data=df2,
        kind="bar",
        ci=None,
        height=9,
        aspect=1.77,
        palette=NCOLOR,
    )
    g.ax.set_xlabel("")
    if terminal is None:
        g.ax.set_ylabel("Exon proportion (%)")
    else:
        g.ax.set_ylabel("Gene proportion (%)")
    if threshold == 0 and "last" not in partial_name:
        g.ax.set_title(f"Proportion of exons containing a {factor_name} peak")
    elif threshold == 0 and "last" in partial_name:
        g.ax.set_title(
            f"Proportion of gene having a terminal exon "
            f"containing a {factor_name} peak"
        )
    else:
        prc = ""
        if side == "both":
            loc = ""

        elif side == "bothe":
            loc = ""
            prc = " (including exon)"
        else:
            loc = side + " "
        if "last" not in partial_name:
            g.ax.set_title(
                f"Proportion of exons with a {loc}{factor_name} "
                f"peak at a distance of {threshold} nucleotides "
                f"or less{prc}"
            )
        else:
            g.ax.set_title(
                f"Proportion of gene with a terminal exon with a "
                f"{loc}{factor_name} peak at a distance of "
                f"{threshold} nucleotides or less{prc}"
            )
    add_stat_annotation(
        g.ax,
        data=df2,
        x=rep_col,
        y="prop",
        loc="inside",
        boxPairList=stats,
        linewidth=1,
        fontsize="xx-small",
        lineYOffsetToBoxAxesCoord=0.02,
    )
    g.savefig(output / f"barplot_{partial_name}_{side}.pdf")
    plt.close()


def get_stat_figure(df_stat: pd.DataFrame) -> List[List]:
    """
    From a dataframe containing statistical analysis, get the p-values \
    associated for 2 list of exons

    :param df_stat: A dataframe of p-values
    :return: The p-values
    """
    res = df_stat[["Comparison", "p.value"]].values
    return [val[0].split(" - ") + [val[1]] for val in res]


def create_table_ctcf(
    df: pd.DataFrame, side: str, threshold: int, rm0: bool = False
) -> pd.DataFrame:
    """
    Create a new table from df with a new column indicating if \
    the exon is associated with ctcf in the wanted interval.

    :param df: The table associating exons with a ctcf site
    :param side: the side of interest
    :param threshold: A threshold
    :param rm0: Boolean indicating if the exons containing ctcf must be \
    removed from the analysis
    :return: The table with the new column indicating if \
    the exon is associated with ctcf in the wanted interval.
    """
    print(side)
    tdf = df.copy()
    tdf["zdist"] = [0] * len(tdf)
    if threshold != 0 and rm0:
        tdf = tdf.loc[tdf["dist"] != 0, :]
    if side in {"both", "bothe"}:
        tdf.loc[abs(tdf["dist"]) <= threshold, "zdist"] = 1
        if side == "both":
            if threshold != 0:
                tdf.loc[tdf["dist"] == 0, "zdist"] = 0
    elif side == "upstream":
        tdf.loc[
            (tdf["upstream_dist"] < 0)
            & (tdf["upstream_dist"] >= threshold * -1),
            "zdist",
        ] = 1
    elif side == "downstream":
        tdf.loc[
            (tdf["downstream_dist"] > 0)
            & (tdf["downstream_dist"] <= threshold),
            "zdist",
        ] = 1
    return tdf


def main_stat(
    filename: Path,
    folder: Path,
    factor: str,
    threshold: int = 0,
    terminal: Optional[str] = None,
    up_and_down=True,
    kind: Optional[str] = None,
    rm0: bool = False,
    factor_name: str = "CTCF",
):
    """
    Checks if DDX5_17 exons more often contains CTCF binding sites than \
    other exons.

    :param filename: file that contains the dataframe with exon and their \
    CTCF exons sites
    :param folder: Folder where the figures will be created
    :param factor: The name of the output factor
    :param up_and_down: Say if we want to consider up and down exons separatly
    :param threshold: a threshold
    :param terminal: If not None, this function checks if gene having one \
    exons regulated by DDX5_17 have their terminal \
    exons more often contains CTCF binding sites than \
    other exons. Possible values are None, uniq_regulation, regulation
    :param kind: The kind of exons contained in filename. \
    Choose from first and last
    :param factor_name: The name of the main factor studied
    """
    output = filename.parents[0]
    output_diag = output / "digas"
    output_diag.mkdir(exist_ok=True)
    df = pd.read_csv(filename, sep="\t")
    if not up_and_down and terminal is None and kind is None:
        df["new_group"] = df["new_group"].str.replace(
            r"_down|_DOWN|_up|_UP", ""
        )
    if terminal is None:
        kind = None
        if "regulation" in df.columns:
            df.drop("regulation", axis=1, inplace=True)
        df = df.loc[df["new_group"] != "multiple", :]
    else:
        df = df.loc[df[terminal] != "multiple", :]
    # df = df.loc[(df["new_group"] != "siPP_UP") &
    #             (df["new_group"] != "U170K_up"), :]
    if threshold != 0:
        sides = ["both", "bothe", "upstream", "downstream"]
    else:
        sides = ["both"]
    for side in sides:
        tdf = create_table_ctcf(df, side, threshold, rm0)
        cfolder, partial_name = get_partial_name(
            threshold,
            up_and_down,
            terminal,
            kind,
            factor,
            folder,
            rm0,
            factor_name,
        )
        cfolder.mkdir(exist_ok=True, parents=True)
        tdf.to_csv(folder / f"tmp_{partial_name}_{side}.txt", sep="\t")
        res = glm_binomial_stat(
            tdf, output_diag, threshold, terminal, partial_name
        )
        res_file = cfolder / f"stat_{partial_name}_{side}.txt"
        res.to_csv(res_file, sep="\t", index=False)
        append_prop(tdf, res_file, threshold, terminal, factor_name)
        barplot_maker(
            tdf,
            cfolder,
            threshold,
            side,
            get_stat_figure(res),
            terminal,
            partial_name,
            factor_name,
        )
