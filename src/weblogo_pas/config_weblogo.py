#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: File containing variable used in this submodule
"""

from pathlib import Path


class ConfigLogo:
    """
    Variable used in this submodule
    """
    output = Path("result") / "weblogo"
    meme_ctcf = Path(__file__).parents[2] / "data" / "CTCF.meme"
    meme_input = Path("result") / "sea" / "meme_files"
