#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to convert .mat PFM (position frequency
matrix) (from mcross \
database) into meme PPM (position probability matrix) to be used by sea
"""

from itertools import takewhile
from pathlib import Path
from typing import List

import lazyparser as lp
import pandas as pd

from .config_weblogo import ConfigLogo


def get_motif_name(content: List[str]) -> str:
    """
    Get the name of a motif

    :param mfile: A file containing a .mat motif
    :return: The name of the motif
    """
    return content[0].split("\t")[1]


def get_nb_site(content: List[str]) -> int:
    """
    Get the number of site used to produce the motif

    :param content: The content of a .mat file
    :return: The number of site in which the motif was found
    """
    return int(content[8].split("\t")[1].split(",")[0][2:])


def build_matrix_df(
    content: List[str], ifile: Path, nsite: int, kind: str
) -> pd.DataFrame:
    """
    Build the pwd the will be used in the meme file

    :param content: The content of the .mat file
    :param ifile: the matrix file
    :param nsite: The number of site used to get the motifs
    :param kind: The kind of motif contained into input_folder files, \
    defaults to 'mCross'
    :return: A dataframe containing the frequency probability of each letter
    """
    if kind != "mCross":
        return pd.read_csv(ifile, sep="\t", index_col="Pos")
    res = list(
        takewhile(lambda x: x.split("\t", 1)[0].isdigit(), content[10:])
    )
    table = [[int(y) for y in x.split("\t")[1:-1]] for x in res]
    df = pd.DataFrame(table, columns=["A", "C", "G", "U"])
    return round(df / nsite, 5)


def get_str_df(df: pd.DataFrame) -> str:
    """
    Get the str dataframe to use in the final file

    :param df: The pwm dataframe
    :return: The string dataframe
    """
    return " " + df.to_string(index=False, header=False).replace("\n", "\n ")


def write_meme_file(
    content: List[str], ifile: Path, outfile: Path, kind: str
) -> None:
    """
    Write meme file

    :param content: Content of a .mat file
    :param ifile: An input .mat/.txt file from mcross base
    :param outfile: The .meme file
    :param kind: The kind of motif contained into input_folder files, \
    defaults to 'mCross'
    """
    kv = kind == "mCross"
    motif_name = get_motif_name(content) if kv else ifile.stem.split("_", 1)[1]
    nbsite = get_nb_site(content) if kv else 1000
    df = build_matrix_df(content, ifile, nbsite, kind)
    str_df = get_str_df(df)
    width = df.shape[0]
    url = "http://zhanglab.c2b2.columbia.edu/data/mCross/eCLIP_mCross_PWM.tgz"
    if not kv:
        base = "http://cisbp-rna.ccbr.utoronto.ca/data/0.6/DataFiles/"
        url = f"{base}PWMs/Files/{motif_name}.txt"
    new_content = f"""MEME version 4

ALPHABET= ACGU

strands: + -

Background letter frequencies
A 0.25 C 0.25 G 0.25 U 0.25

MOTIF {motif_name}
letter-probability matrix: alength= 4 w= {width} nsites= {nbsite} E= 1e-4
{str_df}
URL {url}
"""
    with outfile.open("w") as outf:
        outf.write(new_content)


def create_meme(input_mat: Path, kind: str = "mCross") -> None:
    """
    Build a meme position probability matrix format (.meme) from \
    a position frequency matrix of mcross base (https://zhanglab.c2b2.columbia.edu/mCrossBase/)

    :param intput_mat: An input .mat file from mcross base
    :param kind: The kind of motif contained into input_folder files, \
    defaults to 'mCross'
    """
    ConfigLogo.meme_input.mkdir(exist_ok=True)
    outfile = ConfigLogo.meme_input / f"{input_mat.stem}.meme"
    content = input_mat.open("r").read().splitlines()
    write_meme_file(content, input_mat, outfile, kind)


@lp.parse(kind=["mCross", "cisbprna"])
def build_meme_files(input_folder: str, kind: str = "mCross") -> None:
    """
    Build .meme file from .mat/.txt file located inside input_folder

    :param input_folder: A folder containing .mat file
    :param kind: The kind of motif contained into input_folder files, \
    defaults to 'mCross'
    """
    ext = "*.mat" if kind == "mCross" else "*.txt"
    mat_files = Path(input_folder).glob(ext)
    for mat_file in mat_files:
        create_meme(mat_file, kind)


if __name__ == "__main__":
    build_meme_files()  # pylint: disable=no-value-for-parameter
