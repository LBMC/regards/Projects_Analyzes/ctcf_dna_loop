#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Create sequences corresponding to the last 50 nt \
of list of exons given in input
"""
import doctest
import subprocess as sp
import sys
from pathlib import Path
from typing import Dict, Tuple

import lazyparser as lp
import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from loguru import logger
from pyfaidx import Fasta

from .config_weblogo import ConfigLogo
from .parse_meme_file import create_location_table


def load_exon_file(exon_file: Path) -> pd.DataFrame:
    """
    Load a file containing terminal exons produced by the scripts \
    vicinity_DDX5_17/__main__.py or vicinity_DDX5_17/__main__.py or \
    launcher_custom_input.py.

    :param exon_file: A file containing exons
    :return: the file loaded as a datavrame
    """
    return pd.read_csv(exon_file, sep="\t")


def get_coordinates(exon_df: pd.DataFrame) -> pd.DataFrame:
    """
    Transform a dataframe corresponding to a terminal exon file \
    into a dataframe containing for each terminal exons, it's coordinates \
    and it's group.

    :param exon_df: A exon dataframe
    :return: A dataframe containing exons coordinates and their regulation

    >>> edf = {'exon_name': ['DSC2_9', 'DSC1_9', 'DSG1_6', 'DSG4_6', 'TPT1_6'],
    ... 'dist': [-33526, -6911, -14811, 9405, -2985],
    ... 'exon_id': ['1:10-20', '2:30-40', '3:50-60', '4:70-80', '5:90-100'],
    ... 'group': ['Last', 'Last', 'Last', 'Last', 'Last'],
    ... 'strand': ['-', '-', '+', '+', '-'],
    ... 'gene_name': ['DSC2', 'DSC1', 'DSG1', 'DSG4', 'TPT1'],
    ... 'exon_pos': [19, 17, 16, 16, 6],
    ... 'id': ['1_19', '2_17', '3_16', '4_16', '6_6'],
    ... 'gene_id': [1, 2, 3, 4, 6],
    ... 'last_fasterDB_exon': [19, 17, 16, 16, 6],
    ... 'uniq_regulation': ['ctrl', 'ctrl', 'ctrl', 'ctrl', 'ctrl'],
    ... 'regulation': ['ctrl', 'ctrl', 'ctrl', 'ctrl', 'ctrl']}
    >>> get_coordinates(pd.DataFrame(edf))
      exon_name    id regulation strand chr  start  stop
    0    DSC2_9  1_19       ctrl      -   1      9    20
    1    DSC1_9  2_17       ctrl      -   2     29    40
    2    DSG1_6  3_16       ctrl      +   3     49    60
    3    DSG4_6  4_16       ctrl      +   4     69    80
    4    TPT1_6   6_6       ctrl      -   5     89   100
    """
    coord = pd.DataFrame(
        exon_df["exon_id"].str.split(r":|-").to_list(),
        columns=["chr", "start", "stop"],
    )
    coord["start"] = coord["start"].astype(int)
    coord["stop"] = coord["stop"].astype(int)
    coord["start"] -= 1
    exon_df = exon_df[["exon_name", "id", "regulation", "strand"]].copy()
    df = pd.concat([exon_df, coord], axis=1)
    df["start"] = df["start"].astype(int)
    df["stop"] = df["stop"].astype(int)
    return df


def resize_exons(
    df: pd.DataFrame, size: int, center: str = "end"
) -> pd.DataFrame:
    """
    Resize the interval in the dataframe for them to have the size \
    of `size` nucleotide at their end

    :param df: A dataframe containing exons and their genomic intervals
    :param size: The size of the last part of exons we want to have
    :param center: Parameter indicating if the sequence from which \
    the weblogo is produced if centered on the end border of exon (end) or \
    it's start border (start)
    :return: The dataframe with the intervals resized

    >>> cdf = pd.DataFrame({"exon_name": range(1, 5),
    ... "strand": ["-", "-", "+", "+"], "chr": range(1, 5),
    ... "start": [100, 150, 100, 150], "stop": [120, 180, 110, 157]})
    >>> resize_exons(cdf, 5, "end")
       exon_name strand  chr  start  stop
    0          1      -    1     95   105
    1          2      -    2    145   155
    2          3      +    3    105   115
    3          4      +    4    152   162
    >>> resize_exons(cdf, 20, "end")
       exon_name strand  chr  start  stop
    0          1      -    1     80   120
    1          2      -    2    130   170
    2          3      +    3     90   130
    3          4      +    4    137   177
    >>> resize_exons(cdf, 5, "start")
       exon_name strand  chr  start  stop
    0          1      -    1    115   125
    1          2      -    2    175   185
    2          3      +    3     95   105
    3          4      +    4    145   155
    >>> resize_exons(cdf, 20, "start")
       exon_name strand  chr  start  stop
    0          1      -    1    100   140
    1          2      -    2    160   200
    2          3      +    3     80   120
    3          4      +    4    130   170
    >>> resize_exons(cdf, 0, "all")
       exon_name strand  chr  start  stop
    0          1      -    1    100   120
    1          2      -    2    150   180
    2          3      +    3    100   110
    3          4      +    4    150   157
    >>> resize_exons(cdf, 10, "all")
       exon_name strand  chr  start  stop
    0          1      -    1     90   130
    1          2      -    2    140   190
    2          3      +    3     90   120
    3          4      +    4    140   167
    """
    list_series = []
    for i in range(df.shape[0]):
        mserie = df.iloc[i, :].copy()
        if center == "all":
            mserie["start"] = max(0, mserie["start"] - size)
            mserie["stop"] = mserie["stop"] + size
        elif (mserie["strand"] == "+" and center == "end") or (
            mserie["strand"] == "-" and center == "start"
        ):
            mserie["start"] = max(0, mserie["stop"] - size)
            mserie["stop"] = mserie["stop"] + size
        else:
            mserie["stop"] = mserie["start"] + size
            mserie["start"] = max(0, mserie["start"] - size)
        list_series.append(mserie)
    return pd.DataFrame(list_series)


def get_sequence(mseries: pd.Series, dic_seq: Fasta) -> Tuple[str, str]:
    """
    Get the sequence of a genomic interval of interest

    :param mseries: A series containing the name of an interval and \
    its coordinates
    :param dic_seq: A dictionary containing chromosome sequences
    :return: The sequences of an interval

    >>> ds = {"1": "AAAAAAAAAATGCTGCTGA"}
    >>> ms = pd.Series({"exon_name": "test", "id": "1", "strand": "+",
    ... "chr": "1", "start": 10, "stop": 19})
    >>> get_sequence(ms, ds)
    ('1 - test', 'TGCTGCTGA')
    >>> ms = pd.Series({"exon_name": "test", "id": "1", "strand": "-",
    ... "chr": "1", "start": 10, "stop": 19})
    >>> get_sequence(ms, ds)
    ('1 - test', 'TCAGCAGCA')
    """
    res = str(dic_seq[mseries["chr"]][mseries["start"] : mseries["stop"]])
    if mseries["strand"] == "-":
        res = str(Seq(res).reverse_complement())
    return f"{mseries['id']} - {mseries['exon_name']}", res


def get_sequences(df: pd.DataFrame, dic_seq: Fasta) -> Dict:
    """
    Get the sequences for every interval in the dataframe

    :param df: A dataframe containing genomic intervals
    :param dic_seq: A dictionary of chromosome sequence
    :return: A dictionary containing for each column \
    regulation, the list of sequences within it.

    >>> d = pd.DataFrame({"exon_name": list("AB"), "id": ["1", "2"],
    ... "regulation": ["ctrl", "siPP"], "strand": ["+", "-"],
    ... "chr": ["1", "1"], "start": [10, 10], "stop": [19, 19]})
    >>> ds = {"1": "AAAAAAAAAATGCTGCTGA"}
    >>> get_sequences(d, ds)
    {'ctrl': [('1 - A', 'TGCTGCTGA')], 'siPP': [('2 - B', 'TCAGCAGCA')]}
    """
    groups = df["regulation"].unique()
    dic = {g: [] for g in groups}
    for i in range(df.shape[0]):
        mserie = df.iloc[i, :]
        dic[mserie["regulation"]].append(get_sequence(mserie, dic_seq))
    return dic


def create_fasta(
    sequences: Dict,
    output: Path,
    center: str,
    launch_meme: bool = True,
    nmotifs: int = 10,
    minw: int = 6,
    maxw: int = 6,
) -> Dict[str, Path]:
    """
    Create Fasta files for each data located inside a dictionary of sequences.

    :param sequences: A dictionary of sequences
    :param output: The folder where the result will be created
    :param center: Parameter indicating if the sequence from which \
    the weblogo is produced if centered on the end border of exon (end) or \
    it's start border (start)
    :param launch_meme: A boolean indicating if a motif search must be run \
    using meme
    :return: The list of fasta files to create
    """
    seq_files = {}
    for g in sequences:
        fasta_file = output / f"{center}_sequences_{g}.fasta"
        seq_record = [SeqRecord(Seq(seq), id=mid) for mid, seq in sequences[g]]
        with open(str(fasta_file), "w") as outf:
            SeqIO.write(seq_record, outf, "fasta")
        if launch_meme:
            cmd = (
                f"data/meme_program/meme {fasta_file} -mod zoops "
                f"-nmotifs {nmotifs} -minw {minw} -maxw {maxw} -brief 200000 -dna "
                f"-nostatus -o {output / g}"
            )
            logger.info(f"Execution of -> {cmd}")
            sp.check_call(cmd, shell=True)
        seq_files[g] = fasta_file
    return seq_files


def adjust_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Adjust the dataframe if it corresponds to terminal or internal exon \
    dataframe and their distance to CTCF sites.

    :param df: A dataframe containing internal or terminal exon \
    along with their respective distance to CTCF sites
    :return: The dataframe adjusted
    """
    if "id" in df.columns:
        return df
    df["id"] = df.index.astype(str) + "-" + df["exon_name"]
    df["regulation"] = df["new_group"].str.replace("_.*", "")
    return df


@logger.catch(reraise=True)
def launch_sea(
    sequences: Dict[str, Path],
    motif_file: Path = ConfigLogo.meme_ctcf,
    enrichment: bool = True,
) -> None:
    """
    Launch SEA program to check if CTCF motifs is enriched in our sequences.

    :param sequences: A dictionary indicating fasta sequence file
    :param motif_file: A motif file in meme format
    :param enrichment: True to permform enrichment analysis False for \
    impoverishment
    """
    output = sequences["ctrl"].parent
    k = [k for k in sequences if k != "ctrl"]
    if len(k) > 1:
        raise IndexError(f"Only two keys must be in sequences: not {k}")
    if enrichment:
        cmd = (
            f"sea --m {motif_file} --p {sequences[k[0]]} "
            f"--n {sequences['ctrl']} --oc {output}/enrichment"
        )
    else:
        cmd = (
            f"sea --m {motif_file} --n {sequences[k[0]]} "
            f"--p {sequences['ctrl']} --oc {output}/impoverichment"
        )
    logger.info(f"Execution of -> {cmd}")
    sp.check_call(cmd, shell=True)


@lp.parse(input_file="file", genome="file", center=["end", "start"])
def create_fasta_files(
    input_file: str,
    genome: str,
    size: int,
    output_folder: str,
    center: str = "end",
):
    """

    :param input_file: A file containing terminal exons  produced by \
    the scripts \
    vicinity_DDX5_17/__main__.py or vicinity_DDX5_17/__main__.py or \
    launcher_custom_input.py.
    :param genome: A genome fasta file
    :param size: the size of interval at the end of exons
    :param output_folder: Name of the folder were the result will be created
    :param center: Parameter indicating if the sequence from which \
    the weblogo is produced if centered on the end border of exon (end) or \
    it's start border (start)
    """
    output = ConfigLogo.output
    output.mkdir(exist_ok=True)
    output = output / output_folder
    output.mkdir(exist_ok=True)
    dic_seq = Fasta(genome)
    df = load_exon_file(Path(input_file))
    df = adjust_df(df)
    df_exon = get_coordinates(df)
    df_end = resize_exons(df_exon, size, center)
    sequences = get_sequences(df_end, dic_seq)
    sequences = create_fasta(sequences, output, center)
    create_location_table(output, size * 2)
    launch_sea(sequences)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        doctest.testmod()
    else:
        create_fasta_files()  # pylint: disable=no-value-for-parameter
