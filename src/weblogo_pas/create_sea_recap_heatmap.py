#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to produce a heatmap recapitulating \
sea's results
"""

import re
from pathlib import Path
from typing import List

import lazyparser as lp
import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio

pio.kaleido.scope.mathjax = None


def get_sea_result_files(folder: Path) -> List[Path]:
    """
    Get the list of sea files

    :param folder: Folder containing sea results file
    :return: The list of sea file
    """
    return list(folder.glob("*_down_*impov.html"))


def get_pval(mfile: Path) -> float:
    """
    Open a sea result file to get the pvalue of it

    :param mfile: The sea file
    :return: The pvalue
    """
    content = mfile.open("r").read()
    pval = float(re.findall(r"\"pvalue\": \"(\S+)\"", content)[0])
    nb1 = int(re.findall(r"\"count\": (\d+)", content)[1])
    nb2 = int(re.findall(r"\"count\": (\d+)", content)[2])
    nb = min([nb1, nb2])
    return np.nan if nb < 100 else pval


def build_df(sea_files: List[Path]) -> pd.DataFrame:
    """
    Build a dataframe of 1-p-value

    :param sea_files: The list of sea results files of interest
    :return: The dataframe of p-values
    """
    dic = {"factor": [], "down": [], "up": []}
    for sea in sea_files:
        factor = sea.stem.split("_")[1]
        dic["factor"].append(factor)
        for reg in ["up", "down"]:
            tmp = sea.parent / sea.name.replace("_down_", f"_{reg}_")
            impov_reg = get_pval(tmp)
            enrich_reg = get_pval(tmp.parent / tmp.name.replace("_impov", ""))
            if np.isnan(impov_reg) and np.isnan(enrich_reg):
                dic[reg].append(np.nan)
            elif np.isnan(impov_reg) and not np.isnan(enrich_reg):
                dic[reg].append(1 - enrich_reg)
            elif not np.isnan(impov_reg) and np.isnan(enrich_reg):
                dic[reg].append((1 - impov_reg) * -1)
            elif impov_reg < enrich_reg:
                dic[reg].append((1 - impov_reg) * -1)
            else:
                dic[reg].append(1 - enrich_reg)
    df = pd.DataFrame(dic)
    df = df.sort_values("up")
    return df.set_index("factor")


def heatmap_pval(df_pval: pd.DataFrame, outfile: Path) -> None:
    """
    Produce the pvalue heatmap

    :param df_pval: Heatmap of 1-p-values
    :param outfile: File were the figure will be created
    """
    df_pval = df_pval.T
    vpval = np.nan_to_num(
        df_pval.values, nan=np.nanmax(df_pval.values) + 0.001
    )
    mi, ma = np.nanmin(df_pval.values), np.nanmax(df_pval.values)
    if mi < 0:
        ma = abs(mi) + abs(ma)
    v1 = (-0.95 - mi) / ma
    v2 = (0.95 - mi) / ma
    cs = [
        (0, "#1338BE"),
        (v1, "#1338BE"),
        (v1 + 0.0005, "#888888"),
        (v2 - 0.0005, "#888888"),
        (v2, "#D21404"),
        (0.99998, "#D21404"),
        (0.999989, "#FFFFFF"),
        (1, "#FFFFFF"),
    ]
    heatmap = [
        go.Heatmap(
            x=df_pval.columns,
            y=df_pval.index,
            z=vpval,
            xgap=2,
            ygap=2,
            colorbar={"x": 1.1},
            colorscale=cs,
        )
    ]
    fig = go.Figure()
    for data in heatmap:
        fig.add_trace(data)

    fig["layout"].update(
        {
            "autosize": True,
            "height": 320,
            "width": 1200,
            "plot_bgcolor": "black",
            "showlegend": False,
            "hovermode": "closest",
            "font": {"size": 27},
            "font_family": "arial",
            "xaxis_showgrid": False,
            "yaxis_showgrid": False,
        }
    )
    plotly.offline.plot(
        fig,
        filename=str(outfile).replace(".html", "_pval.html"),
        auto_open=False,
    )
    fig.write_image(str(outfile).replace(".html", "_pval.pdf"))


@lp.parse
def main(folder: str) -> None:
    """
    Produce a heatmap recapitulating the sea p-values optained in \
    a folder

    :param folder: A folder containing sea html file
    """
    mfiles = get_sea_result_files(Path(folder))
    df = build_df(mfiles)
    outfile = Path(folder) / "recap.txt"
    df.to_csv(outfile, sep="\t")
    heatmap_pval(df, Path(folder) / "recap.html")


if __name__ == "__main__":
    main()
