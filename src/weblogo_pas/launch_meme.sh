#!/bin/bash

# Meme analysis of MYCN peaks at less than 2000kb of an exon

# Input definition
SUMMIT_BED=${1:-"data/data_mycn/MYCN_concatsummit.bed"}
TERMINAL_EXON_BED=${2:-"data/data_mycn/chimerreadthg2_exon.bed"}
OUTPUT=${3:-"result/weblogo/MEME_MYCN"}
GENOME_SIZE=${4:-"data/hg19.ren.chrom.sizes"}
GENOME=${5:-"data/Homo_sapiens.GRCh37.dna.primary_assembly.fa"}
MOTIF=${6:-"data/data_mycn/MYCN.meme"}

# Create output folder
if [ ! -d "$OUTPUT" ]; then
  mkdir $OUTPUT
fi

# Define a folder where tmp file will be created
TMPDIR="${OUTPUT}/tmp"
if [ ! -d "$TMPDIR" ]; then
  mkdir $TMPDIR
fi


# Create terminal exon bed file
outfile="${TMPDIR}/$(basename $TERMINAL_EXON_BED)"
cat $TERMINAL_EXON_BED | grep -v "chr" | awk 'BEGIN{FS="\t"; OFS="\t"}{print($2,$3,$4,$1)}' > $outfile

# Remove chr from the summit file
tmp_summit="${TMPDIR}/$(basename $SUMMIT_BED)"
sed "s|^chr||g" $SUMMIT_BED > $tmp_summit

# increase of 25pb in both size the summit peaks
summit_resized="${TMPDIR}/$(basename $tmp_summit '.bed')_extended.bed"
summit_resized_250="${TMPDIR}/$(basename $tmp_summit '.bed')_extended_250.bed"
bedtools slop -b 25 -i $tmp_summit -g $GENOME_SIZE > $summit_resized
bedtools slop -b 250 -i $tmp_summit -g $GENOME_SIZE > $summit_resized_250


# increase of 2000pb in both size the exons in the list (should be terminal exons)
exon_resized="${TMPDIR}/$(basename $outfile '.bed')_extended.bed"
bedtools slop -b 2000 -i $outfile -g $GENOME_SIZE > $exon_resized

# intersection between exon and summit and exon - keeping only summit  with more than 50% of inclustion in the peak file
terminal_summit="${TMPDIR}/$(basename $summit_resized '.bed')_terminal.bed"
terminal_summit_250="${TMPDIR}/$(basename $summit_resized_250 '.bed')_terminal.bed"
bedtools intersect -a $summit_resized -b $exon_resized -f 0.51 -wa -u > $terminal_summit
bedtools intersect -a $summit_resized_250 -b $exon_resized -f 0.51 -wa -u > $terminal_summit_250


# Merge summit together
merged_summit="${TMPDIR}/$(basename $terminal_summit '.bed')_merged.bed"
sort -k1,1 -k2,2n  $terminal_summit | bedtools merge -i - -c 4,5 -o collapse,mean > $merged_summit


# Create a fasta file of ther merged summit file
fasta_summit="${TMPDIR}/$(basename $merged_summit '.bed').fa"
fasta_summit_250="${TMPDIR}/$(basename $terminal_summit_250 '.bed').fa"
bedtools getfasta -fi $GENOME -bed $merged_summit -name -fullHeader > $fasta_summit
bedtools getfasta -fi $GENOME -bed $terminal_summit_250 -name -fullHeader > $fasta_summit_250

# Launch meme
outmeme="${OUTPUT}/meme_analysis"
outmeme_250="${OUTPUT}/meme_analysis_250_ce"
data/meme_program/meme $fasta_summit -mod zoops -nmotifs 10 -w 6 -dna -nostatus -objfun de -shuf 1 -oc $outmeme
data/meme_program/meme $fasta_summit_250 -mod zoops -nmotifs 10 -w 6 -dna -nostatus -objfun ce -shuf 1 -oc $outmeme_250


# Launch sea
outsea="${OUTPUT}/sea_analysis"
sea --p $fasta_summit --m $MOTIF -oc $outsea --order 1