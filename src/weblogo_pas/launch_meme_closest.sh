#!/bin/bash

# Meme analysis of MYCN peaks at less than 2000kb of an exon

# Input definition
SUMMIT_BED=${1:-"data/data_mycn/MYCN_concatsummit.bed"}
TERMINAL_EXON_BED=${2:-"data/data_mycn/chimerreadthg2_exon.bed"}
OUTPUT=${3:-"result/weblogo/MEME_MYCN_CLOSEST"}
GENOME_SIZE=${4:-"data/hg19.ren.chrom.sizes"}
GENOME=${5:-"data/Homo_sapiens.GRCh37.dna.primary_assembly.fa"}
MOTIF=${6:-"data/data_mycn/MYCN.meme"}

# Create output folder
if [ ! -d "$OUTPUT" ]; then
  mkdir $OUTPUT
fi

# Define a folder where tmp file will be created
TMPDIR="${OUTPUT}/tmp_closest"
if [ ! -d "$TMPDIR" ]; then
  mkdir $TMPDIR
fi


# Create terminal exon bed file
outfile="${TMPDIR}/$(basename $TERMINAL_EXON_BED)"
cat $TERMINAL_EXON_BED | grep -v "chr" | awk 'BEGIN{FS="\t"; OFS="\t"}{print($2,$3,$4,$1)}' | sort -k1,1 -k2,2n > $outfile

# Remove chr from the summit file
tmp_summit="${TMPDIR}/$(basename $SUMMIT_BED)"
sed "s|^chr||g" $SUMMIT_BED | sort -k1,1 -k2,2n > $tmp_summit


# Launch bedtools closest to get the peaks closest to MYCN peaks data
closest_summit="${TMPDIR}/$(basename $tmp_summit '.bed')_closest.bed"
bedtools closest -a $outfile -b $tmp_summit -d -t first | awk 'BEGIN{FS="\t"; OFS="\t"}{if($10 <= 2000){print($5,$6,$7,$8,$9)}}' > $closest_summit


# Merge summit together
merged_summit="${TMPDIR}/$(basename $closest_summit '.bed')_merged.bed"
sort -k1,1 -k2,2n  $closest_summit | bedtools merge -i - -c 4,5 -o collapse,mean > $merged_summit


# increase of 25pb in both size the summit peaks
summit_resized="${TMPDIR}/$(basename $merged_summit '.bed')_extended.bed"
summit_resized_250="${TMPDIR}/$(basename $merged_summit '.bed')_extended_250.bed"
bedtools slop -b 25 -i $merged_summit -g $GENOME_SIZE | sort -k1,1 -k2,2n > $summit_resized
bedtools slop -b 250 -i $merged_summit -g $GENOME_SIZE | sort -k1,1 -k2,2n > $summit_resized_250


# Create a fasta file of ther merged summit file
fasta_summit="${TMPDIR}/$(basename $summit_resized '.bed').fa"
fasta_summit_250="${TMPDIR}/$(basename $summit_resized_250 '.bed').fa"
bedtools getfasta -fi $GENOME -bed $summit_resized -name -fullHeader > $fasta_summit
bedtools getfasta -fi $GENOME -bed $summit_resized_250 -name -fullHeader > $fasta_summit_250

# Launch meme
outmeme="${OUTPUT}/meme_analysis_closest"
outmeme_250="${OUTPUT}/meme_analysis_closest_250_ce"
data/meme_program/meme $fasta_summit -mod zoops -nmotifs 10 -w 6 -dna -nostatus -objfun de -shuf 1 -oc $outmeme
data/meme_program/meme $fasta_summit_250 -mod zoops -nmotifs 10 -w 6 -dna -nostatus -objfun ce -shuf 1 -oc $outmeme_250


# Launch sea
outsea="${OUTPUT}/sea_analysis_closest"
sea --p $fasta_summit --m $MOTIF -oc $outsea --order 1

outsea="${OUTPUT}/sea_analysis_closest_250"
sea --p $fasta_summit_250 --m $MOTIF -oc $outsea --order 1