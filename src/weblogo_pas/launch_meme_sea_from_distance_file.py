#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to create a fasta file containing \
sequences of closest peaks located in a distance file
"""


import subprocess as sp
from pathlib import Path
from typing import List

import lazyparser as lp
import pandas as pd


def load_distance_file(mfile: Path, group: str, dist_max: int) -> pd.DataFrame:
    """
    Load the distance file of interest keeping only closest hit \
    to exons having regulation: 'groups' and within a distance below `dist_max`

    :param mfile: The file containing exon distance to peaks
    :param group: The group of regulated exons of interest
    :param dist_max: The maximum distance of exons to peaks
    :return: The peaks within a distance dist_max of exons \
    belonging to 'groups'
    """
    df = pd.read_csv(mfile, sep="\t")
    df = df[df["regulation"] == group].copy()
    df = df[(df["dist"] >= -dist_max) & (df["dist"] <= dist_max)].copy()
    df = df[-df["hit_id"].isna()].copy()
    df = df.reset_index(drop=True)
    return df[["hit_id", "strand", "id", "dist"]]


def build_bed_file(
    df: pd.DataFrame, outfolder: Path, extend: int, group: str, dist_max: int
) -> Path:
    """
    Build a bed file containing peak close to regulated exon

    :param df: The dataframe of peaks
    :param outfolder: Folder where the bed file will be created
    :param extend: The number of nucleotide used to extend peaks from \
    left to right
    :param group: The name of the group of exon to take in account
    :param dist_max: The maximum distance to exon to consider a peak
    :return: The path of the newly created bed file
    """
    dfn = pd.DataFrame(
        df["hit_id"].str.split(r"[:-]").to_list(),
        columns=["chr", "start", "end"],
    )
    dfn["name"] = df["hit_id"] + "~" + df["id"] + "~" + df["dist"].astype(str)
    dfn["score"] = ["."] * df.shape[0]
    dfn["strand"] = df["strand"]
    dfn["start"] = dfn["start"].astype(int) - extend
    dfn["end"] = dfn["end"].astype(int) + extend
    outfile = outfolder / f"{group}_dist{dist_max}_e{extend}.bed"
    dfn.to_csv(outfile, sep="\t", index=False, header=False)
    return outfile


def launch_bedtool(bedfile: Path, genome: Path, outfolder: Path) -> Path:
    """
    Launch bedtools to obtain a fasta file

    :param bedfile: A bedfile containing peaks data
    :param genome: Path to human genome
    :param outfolder: Folder where the fasta file will be created
    :return: The path to the created fasta file
    """
    outfile = outfolder / f"{bedfile.stem}.fasta"
    cmd = f"bedtools getfasta -fi {genome} -bed {bedfile} -s -name > {outfile}"
    print(cmd)
    sp.check_call(cmd, shell=True)
    return outfile


def launch_meme(
    fasta: Path,
    outfolder: Path,
    nmotifs: int = 5,
    width: int = 6,
    mod="oops",
    cons: List[str] = (),
    nw: bool = False,
) -> None:
    """
    Launch meme command

    :param fasta: A fasta file used to launch meme
    :param outfolder: Folder that will contains meme results file
    :param nmotifs: The number of motifs meme should create, defaults to 5
    :param width: The width of logo to seek, defaults to 6
    :param mod: The mod of meme, defaults to "oops"
    :param cons: The consensus to use, defaults to ()
    :param nw: False to use width True else
    """
    addn = "2" if nw else ""
    nout = outfolder / f"meme_analysis{addn}"
    nout.mkdir(exist_ok=True)
    cmd = (
        f"data/meme_program/meme {fasta} -mod {mod} -brief 1000 "
        + f"-nmotifs {nmotifs}  -w {width} -dna -oc {nout}"
    )
    if nw:
        cmd = cmd.replace(f"-w {width}", "")
    if cons:
        for c in cons:
            cmd += f" -cons {c}"
    print(cmd)
    sp.check_call(cmd, shell=True)


def launch_sea(fasta: Path, motif_file: Path, outfolder: Path) -> None:
    """
    Launch sea enrichment analysis

    :param fasta: The fasta file containing sequence of interest
    :param motif_file: The motif file to use
    :param outfolder: Folder where sea results will be created
    """
    outf = outfolder / "sea"
    outf.mkdir(exist_ok=True)
    cmd = f"sea --m {motif_file} --order 0 --p {fasta} --oc {outfolder}/sea"
    print(cmd)
    sp.check_call(cmd, shell=True)


@lp.parse(
    distance_file="file",
    genome="file",
    motif_file="file",
    mod=["oops", "zoops", "anr"],
)
def main(
    distance_file: str,
    group: str,
    genome: str,
    motif_file: str,
    output: str,
    max_dist: int = 2000,
    extend: int = 25,
    nmotifs: int = 5,
    width: int = 6,
    mod: str = "oops",
    cons: List[str] = (),
):
    """
    Use a distance file to selected peaks at most max_dist close to \
    interest exons of group 'group', then extend them by extend nucleotide \
    at each side then create a fasta file used to launch sea and \
    meme programs on those sequence


    :param distance_file: A file containing exons and their distance to peaks
    :param group: The group of exons of interest
    :param genome: The genome to use to recover sequence
    :param motif_file: A file in meme format corresponding to the motif \
    for wich we want to find an enrichment in the peak sequences
    :param output: Folder where the results will be created
    :param max_dist: The maximum distance to interest exons to \
    consider peaks in nucleotides, defaults to 2000
    :param extend: The number of nucleotide used to extend peaks on both \
    sides, defaults to 25
    :param nmotifs: The number of motifs MEME should find, defaults to 5
    :param width: The width of motif MEME should find, defaults to 6
    :param mod: The mod of MEME program, defaults to "oops"
    :param cons: given to -cons parameter of meme program, defaults to ()
    """
    tdf = load_distance_file(Path(distance_file), group, max_dist)
    outf = Path(output)
    bed_file = build_bed_file(tdf, outf, extend, group, max_dist)
    fasta_file = launch_bedtool(bed_file, Path(genome), outf)
    launch_sea(fasta_file, Path(motif_file), outf)
    launch_meme(fasta_file, outf, nmotifs, width, mod, cons)
    launch_meme(fasta_file, outf, nmotifs, width, mod, cons, True)


if __name__ == "__main__":
    main()
