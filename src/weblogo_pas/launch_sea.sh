#!/bin/zsh

# WARNING use ZSH
mkdir result/sea/union_1seq
exon_lists=($(ls data/motifs/regulated_exon/*.txt))
for elist in ${exon_lists}; do
	tmp=`basename $elist ".txt"`
	tmp2=($(echo $tmp | tr "_" "\n"))
	sf=${tmp2[1]}
	reg=${tmp2[-1]}
	meme_files=($(ls result/sea/meme_files/*${sf}*))
	for meme in ${meme_files}; do
		tmpm=`basename $meme ".txt"`
		tmp2m=($(echo $tmpm | tr "." "\n"))
		mname="sea_${sf}_${reg}_${tmp2m[1]}-${tmp2m[2]}-1${tmp2m[6]}"
		python3.11 -m src.weblogo_pas.sea_enrichment_analysis -i ${elist} -g data/Homo_sapiens.GRCh37.dna.primary_assembly.fa -m ${meme} -s 200 -o result/sea/union_1seq/${mname} -c all --filter_file data/splicing_lore_detected_exons.txt
        mv result/sea/union_1seq/${mname}/enrichment/sea.html result/sea/union_1seq/${mname}/enrichment/${mname}.html
		python3.11 -m src.weblogo_pas.sea_enrichment_analysis -i ${elist} -g data/Homo_sapiens.GRCh37.dna.primary_assembly.fa -m ${meme} -s 200 -o result/sea/union_1seq/${mname} -c all --filter_file data/splicing_lore_detected_exons.txt --enrichment False
        mv result/sea/union_1seq/${mname}/impoverichment/sea.html result/sea/union_1seq/${mname}/impoverichment/${mname}_impov.html
        rm result/sea/union_1seq/${mname}/all_sequences_ctrl.fasta
	done
done

mkdir result/sea/union_1seq/sea_result_union1sig/
files=($(find result/sea/union_1seq/ -mmin 15 -name "sea_*.html" -type f)); for f in ${files[*]}; do cp $f result/sea/union_1seq/sea_result_union1sig/; done

python3.11 -m src.weblogo_pas.create_sea_recap_heatmap -f result/sea/union_1seq/sea_result_union1sig/

# exon_lists=($(ls data/motifs/regulated_exons_2_sig/*.txt))
# mkdir result/sea/union_2seq
# for elist in ${exon_lists}; do
# 	tmp=`basename $elist ".txt"`
# 	tmp2=($(echo $tmp | tr "_" "\n"))
# 	sf=${tmp2[1]}
# 	reg=${tmp2[-1]}
# 	meme_files=($(ls result/sea/meme_files/*${sf}*))
# 	for meme in ${meme_files}; do
# 		tmpm=`basename $meme ".txt"`
# 		tmp2m=($(echo $tmpm | tr "." "\n"))
# 		mname="sea_${sf}_${reg}_${tmp2m[1]}-${tmp2m[2]}-1${tmp2m[6]}"
# 		python3.11 -m src.weblogo_pas.sea_enrichment_analysis -i ${elist} -g data/Homo_sapiens.GRCh37.dna.primary_assembly.fa -m ${meme} -s 200 -o result/sea/union_2seq/${mname} -c all --filter_file data/splicing_lore_detected_exons.txt
#         mv result/sea/union_2seq/${mname}/enrichment/sea.html result/sea/union_2seq/${mname}/enrichment/${mname}.html
#         rm result/sea/union_2seq/${mname}/all_sequences_ctrl.fasta
# 	done
# done

# mkdir result/sea/union_2seq/sea_result_union2sig/
# files=($(find result/sea/union_2seq/ -mmin -15 -name "sea_*.html" -type f)); for f in ${files[*]}; do cp $f result/sea/union_2seq/sea_result_union2sig/; done
