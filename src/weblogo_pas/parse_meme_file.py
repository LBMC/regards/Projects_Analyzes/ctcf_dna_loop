#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to parse meme file to recover \
the location of sites found in input sequences
"""
import doctest
from .config_weblogo import ConfigLogo
from pathlib import Path
from typing import List, Dict
import pandas as pd
import xml.etree.ElementTree as ET
import seaborn as sns
from matplotlib.colors import to_hex
from .sequence_visu import display_location


def find_meme_file(folder: Path) -> Path:
    """

    :param folder: A folder containing meme result files
    :return: The report
    """
    return list(folder.glob("*/meme.xml"))


def get_dic(meme_report: Path, kind: str = "sequence") -> Dict[str, str]:
    """
    Link a sequence/motif id to its name

    :param meme_report: A file containing a meme report
    :param kind: The kind of dictionary to produce
    :return: A dictionary linking sequence/motif id to its real name
    """
    tree = ET.parse(meme_report)
    root = tree.getroot()
    if kind == "sequence":
        feature = root.find("training_set").findall("sequence")
    else:
        feature = root.find("motifs").findall("motif")
    return {ft.attrib["id"]: ft.attrib["name"] for ft in feature}


def file_parser(meme_report: Path) -> pd.DataFrame:
    """
    Parse a report file to get the location of sequence containing the motif \
    of interest

    :param meme_report: A file containing the meme report
    :return: A table containing for each sequence the motif of interest

    >>> file_parser(Path("result/weblogo/weblogo_readthrough/readthrough/meme.xml"))
    """
    dic = {"sequence_id": [], "position": [], "motif_id": [], "motif": []}
    tree = ET.parse(meme_report)
    root = tree.getroot()
    motifs = root.find("motifs")
    for motif in motifs:
        sites = motif.find("contributing_sites")
        if len(sites.findall("contributing_site")) == 0:
            return None
        for site in sites.findall("contributing_site"):
            dic["sequence_id"].append(site.attrib["sequence_id"])
            dic["position"].append(int(site.attrib["position"]) + 1)
            dic["motif_id"].append(motif.attrib["id"])
            dic["motif"].append(motif.attrib["name"])
    df = pd.DataFrame(dic)
    seq_dic = get_dic(meme_report, "sequence")
    df["sequence"] = df["sequence_id"].map(seq_dic)
    df = df[["motif_id", "motif", "sequence_id", "sequence", "position"]]
    return df.sort_values("motif_id")


def get_avg_positions(df: pd.DataFrame) -> pd.DataFrame:
    """
    Get the average and standard error start positions of motifs.

    :param df: A dataframe containing motifs location on sequence
    :return: The dataframe containing average motif position on sequence

    >>> d = pd.DataFrame({"motif": ["LOL", "XD", "LOL", "XD"],
    ... "motif_id": [1, 2, 1, 2], "position": [1, 2, 3, 4]})
    >>> get_avg_positions(d)
       motif_id motif  position_mean  position_std  end_mean
    0         1   LOL              2      1.414214         5
    1         2    XD              3      1.414214         5
    """
    df_tmp = df[["motif_id", "motif", "position"]]
    summary = df_tmp.groupby(["motif_id", "motif"]).agg(["mean", "std"])\
        .reset_index()
    summary.columns = summary.columns.map("_".join).str.strip("_")
    summary["end_mean"] = summary["position_mean"] + summary["motif"].str.len()
    return summary[["motif_id", "motif", "position_mean", "end_mean",
                    "position_std"]]


def create_figure(df_summary: pd.DataFrame, outfile: Path, sequence_length: int
                  ) -> None:
    """
    Create a figure to visualize the motifs locations.

    :param df_summary: A dataframe summarizing the location of each motifs
    :param outfile:The output figure
    :param sequence_length: The length of the sequence
    """
    sd_list = list(round(df_summary["position_std"], 2))
    motif = df_summary["motif"].to_list()
    starts = list(round(df_summary["position_mean"], 2))
    ends = list(round(df_summary["end_mean"], 2))
    locations = [(s, e) for s, e in zip(starts, ends)]
    colors = [to_hex(x) for x in sns.color_palette("hls", len(motif))]
    display_location(locations, sequence_length, colors, motif, sd_list,
                     outfile)


def create_location_table(subfolder: Path, seq_length: int) -> None:
    """
    Create the dataframe containing the location of motifs find by meme.

    :param subfolder: A folder containing meme result xml
    :param seq_length: The length of the sequence
    """
    meme_files = find_meme_file(subfolder)
    for meme_report in meme_files:
        df = file_parser(meme_report)
        if df is not None:
            df_summary = get_avg_positions(df)
            out1 = meme_report.parent / "motifs_position.txt"
            out2 = meme_report.parent / "motifs_position_summary.txt"
            out3 = meme_report.parent / "motifs_position_summary.html"
            df.to_csv(out1, sep="\t", index=False)
            df_summary.to_csv(out2, sep="\t", index=False)
            create_figure(df_summary, out3, seq_length)





if __name__ == "__main__":
    # doctest.testmod()
    create_location_table("weblogo_readthrough/readthrough", 100)