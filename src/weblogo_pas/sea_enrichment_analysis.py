#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: The goal of this script is to perform an enrichment analysis of \
a motif located before or after exons
"""


from pathlib import Path

import lazyparser as lp
import pandas as pd
from loguru import logger
from pyfaidx import Fasta

from ..sipp_tad_location.configTAD import ConfigTAD
from .create_pas_weblogo import (
    create_fasta,
    get_sequences,
    launch_sea,
    resize_exons,
)


def get_regulation(mserie: pd.Series) -> str:
    """
    Get the regulation of an exon from a splicing lore file

    :param mserie: A serie corresponding to a splicing lore file
    :return: The regulation
    """
    if "pvalue_glm_corr" in mserie:
        pcol = "pvalue_glm_corr"
    else:
        pcol = "pvalue_fisher_corr"
    if -0.1 < mserie["DeltaPSI"] < 0.1 or mserie[pcol] > 0.05:
        return "ctrl"
    return "Reg"


def load_input_table(mfile: Path) -> pd.DataFrame:
    """
    Load a file having this format:

    n';gene_symbole;exon_skipped;coordonates;exons_flaquants;DeltaPSI;pvalue_fisher_corr;pvalue_glm_corr
    1;PRMT7;19;16:68390604-68390700;18:20;-0.335231;1.14927e-25;8.86575e-09
    2;HMG20B;8;19:3576890-3577105;7:9;-0.200238;1.91623e-33;1.02413e-06

    or this format:

    exons   chr     start   end
    SKA2_3  17      57196680        57196856
    RPGRIP1L_18     16      53679537        53679915

    :param mfile: The file of interest
    :return: The loaded file
    """
    if "\t" in Path(mfile).open("r", encoding="UTF-8").readline():
        return load_tsv_table(mfile)
    return load_sl_table(mfile)


def load_tsv_table(mfile: Path) -> pd.DataFrame:
    """
    Load a file containing exons

    :param mfile: The file of interest that must contains those columns:
    exons   chr     start   end
    :return: The loaded file with the columns
    exon_name,gene_symbole,exon_skipped,chr,start,stop,regulation
    """
    df = pd.read_csv(mfile, sep="\t")
    df = df.rename({"exons": "exon_name", "end": "stop"}, axis=1)
    df["regulation"] = ["Reg"] * df.shape[0]
    df["gene_symbole"] = df["exon_name"].str.replace(r"_\d+", "", regex=True)
    df["exon_skipped"] = df["exon_name"].str.replace(r".+?_", "", regex=True)
    return df[
        [
            "exon_name",
            "gene_symbole",
            "exon_skipped",
            "chr",
            "start",
            "stop",
            "regulation",
        ]
    ].copy()


def load_sl_table(mfile: Path) -> pd.DataFrame:
    """
    Load a file containing exons in splicing lore format

    :param mfile: A file containing in splicing lore format: e.g it \
    must have the following collumns \
    gene_symbole,exon_skipped,coordonates,exons_flaquants,DeltaPSI,\
    pvalue_fisher_corr,pvalue_glm_corr
    :return: The loaded dataframe
    """
    df = pd.read_csv(mfile, sep=";")
    df["regulation"] = df.apply(get_regulation, axis=1)
    df["exon_name"] = df["gene_symbole"] + "_" + df["exon_skipped"].astype(str)
    tmp = pd.DataFrame(
        df["coordonates"].str.split(r"[:-]", regex=True).to_list(),
        columns=["chr", "start", "stop"],
    )
    return pd.concat([df, tmp], axis=1)


def add_strand(df: pd.DataFrame, gene_bed: Path) -> pd.DataFrame:
    """
    Add a strand column to the dataframe

    :param df: A dataframe containing a list of exons
    :param gene_bed: A file containing genes and their id
    :return: The dataframe with a columns strand
    """
    tmp = df.shape[0]
    df_g = pd.read_csv(gene_bed, sep="\t")[["score", "id", "strand"]]
    df_g.rename({"score": "gene_symbole"}, axis=1, inplace=True)
    df_g = df_g.drop_duplicates(subset="gene_symbole", keep=False).copy()
    df = df.merge(df_g, how="inner", on="gene_symbole")
    if not df[df["strand"].isna()].empty:
        raise ValueError("All genes should have a defined strand")
    df["exon_id"] = (
        df["id"].astype("str") + "_" + df["exon_skipped"].astype(str)
    )
    if df.shape[0] != tmp:
        logger.warning(f"{tmp - df.shape[0]} exon lost to recover strand")
    return df[["chr", "start", "stop", "strand", "exon_id", "regulation"]]


def get_ctrl_exons(
    df: pd.DataFrame,
    exon_bed: Path,
    filter_file: str,
    ctrl_kind: str = "internal",
) -> pd.DataFrame:
    """
    Add control exons for analysis

    :param df: A dataframe
    :param exon_bed: A bed file containing exons
    :param filter_file: A filter file or empty string to get only the \
    control exons defined in this file. Exon id in this file must have the \
    form 1_1
    :param ctrl_kind: The kind of control to produce
    :return: Add internal control exons
    """
    df_exon = pd.read_csv(exon_bed, sep="\t")
    df_exon.rename(
        {"#ref": "chr", "end": "stop", "id": "exon_id"}, axis=1, inplace=True
    )
    if filter_file:
        exons_to_keep = open(filter_file, "r").read().splitlines()
        df_exon = df_exon[df_exon["exon_id"].isin(exons_to_keep)].copy()
    df_exon["gene_id"] = df_exon["exon_id"].str.replace(
        r"_\d+", "", regex=True
    )
    df_exon["pos"] = (
        df_exon["exon_id"].str.replace(r"\d+_", "", regex=True).astype(int)
    )
    tmp = df_exon[["gene_id", "pos"]].groupby("gene_id").max().reset_index()
    terminal_exon = tmp["gene_id"] + "_" + tmp["pos"].astype(str).to_list()
    if ctrl_kind == "first":
        df_exon = df_exon[df_exon["pos"] == 1].copy()
    elif ctrl_kind == "internal":
        df_exon = df_exon[df_exon["pos"] != 1].copy()
        df_exon = df_exon[-df_exon["exon_id"].isin(terminal_exon)].copy()
    else:
        df_exon = df_exon[df_exon["exon_id"].isin(terminal_exon)].copy()
    df_exon = df_exon[-df_exon["exon_id"].isin(df["exon_id"].values)].copy()
    df_exon = df_exon[["chr", "start", "stop", "strand", "exon_id"]]
    df_exon["regulation"] = ["ctrl"] * df_exon.shape[0]
    df_exon["id"] = df_exon["exon_id"]
    df_exon["exon_name"] = df_exon["exon_id"]
    return pd.concat([df, df_exon], axis=0, ignore_index=True)


def adjust_df(
    mfile: Path,
    exon_bed: Path,
    gene_bed: Path,
    filter_file: str,
    ctrl_kind: str = "internal",
) -> pd.DataFrame:
    """
    Adjust the dataframe of exons

    :param mfile: The exons file of interest
    :param exon_bed: A bed file contaninig exon
    :param gene_bed: A bed file containing genes
    :param filter_file: A filter file or empty string to get only the \
    control exons defined in this file. Exon id in this file must have the \
    form 1_1
    :param ctrl_kind: The kind of  control to produce
    :return: The adjusted dataframe
    """
    df = load_input_table(mfile)
    df = add_strand(df, gene_bed)
    df["start"] = df["start"].astype(int)
    df["start"] -= 1
    df["stop"] = df["stop"].astype(int)
    df["id"] = df["exon_id"].astype(str)
    df["exon_name"] = df["id"].astype(str)
    return get_ctrl_exons(df, exon_bed, filter_file, ctrl_kind)


@lp.parse(
    input_file="file",
    genome="file",
    center=["end", "start", "all"],
    ctrl_kind=["internal", "last", "first"],
)
def sea_enrichment_analysis(
    input_file: str,
    genome: str,
    meme_motif: str,
    size: int,
    output: str,
    center: str = "end",
    enrichment: bool = True,
    filter_file: str = "",
    ctrl_kind: str = "internal",
    launch_meme: bool = False,
    nmotif: int = 10,
    minw: int = 6,
    maxw: int = 20,
):
    """

    :param input_file: A file containing exons (splicing lore format)
    :param genome: A genome fasta file
    :param meme_motif: A motif in meme format
    :param size: the size of interval at the end of exons
    :param output: Name of the folder were the result will be created
    :param center: Parameter indicating if the sequence to use in \
    sea enrichment. If center is "start" or "end", then the sequence \
    is centered on start-size and start+size region (end-size and end+size) \
    region respectivally. If center is 'all' then the sequence  \
    'start-size and end+size region is used
    :param enrichment: True to perform an enrichment analysis, false for \
    impoverishment (enrichment on control sequence), defaults to True
    :param filter: A filter file or empty string to get only the \
    control exons defined in this file. Exon id in this file must have the \
    form 1_1, default to ""
    :param ctrl_kind: The kind of control to produce
    """
    noutput = Path(output)
    noutput.mkdir(exist_ok=True)
    dic_seq = Fasta(genome)
    df = adjust_df(
        Path(input_file),
        ConfigTAD.exon_bed,
        ConfigTAD.gene_bed,
        filter_file,
        ctrl_kind,
    )
    df_end = resize_exons(df, size, center)
    tmp = df_end[df_end["regulation"] == "Reg"][
        ["chr", "start", "stop", "exon_id", "strand"]
    ]
    tmp.to_csv(noutput / "Reg.bed", sep="\t", index=False, header=False)
    sequences = get_sequences(df_end, dic_seq)
    sequences = create_fasta(
        sequences,
        noutput,
        center,
        launch_meme=launch_meme,
        nmotifs=nmotif,
        minw=minw,
        maxw=maxw,
    )
    launch_sea(sequences, Path(meme_motif), enrichment)


if __name__ == "__main__":
    sea_enrichment_analysis()  # pylint: disable=no-value-for-parameter
