#!/usr/bin/env python3

# -*- coding: UTF-8 -*-

"""
Description: Figure to display location of sequences
"""

import plotly.graph_objects as go
from typing import Tuple, List
import plotly
from pathlib import Path
import numpy as np


def display_location(loc: List[Tuple[float, float]], seq_length: int,
                     color: List[str], motif_name: List[str],
                     sd: List[float], outfile: Path) -> None:
    """

    :param loc: Strand and stop position of a motifs
    :param seq_length: The length of a sequence
    :param color: The color selected for the motifs
    :param motif_name: The list of the motifs
    :param sd: The list of standard error
    :param outfile: The result file
    :return:
    """
    if len(loc) != len(motif_name) != len(color) != len(sd):
        raise IndexError("The length of one of the parameters differ")
    move_st = np.linspace(0.0, 1.0, num=len(loc) + 2) - 0.5
    move_st = move_st[1:-1]
    fig = go.Figure()
    for i, l in enumerate(loc):
        for k, v in enumerate(l):
            name_loc = "start" if k == 0 else "stop"
            fig.add_trace(go.Scatter(x=[v - sd[i], v + sd[i]],
                                     y=[0.5 + move_st[i], 0.5 + move_st[i]],
                                     line={"width": 2, "color": color[i]},
                                     mode="lines", hoverinfo='skip',
                                     name=f"motif_{i + 1} - {name_loc}"))
        fig.add_trace(go.Scatter(x=[l[0], l[1], l[1], l[0], l[0]],
                                 y=[0, 0, 1, 1, 0],
                                 fill="toself", fillcolor=color[i],
                                 name=f"{motif_name[i]}",
                                 hoverinfo='skip',
                                 line={"width": 0}, mode="lines", opacity=0.8))
        fig.add_trace(go.Scatter(x=[l[0], l[1], l[1], l[0], l[0]],
                                 y=[0, 0, 1, 1, 0],
                                 fill="toself", fillcolor=color[i],
                                 text=f"{motif_name[i]}<br>Start: "
                                      f"{l[0]}<br>End: {l[1]}<br>SD: {sd[i]}",
                                 name='', showlegend=False,
                                 line={"width": 0}, mode="lines", opacity=0))
    fig.add_trace(go.Scatter(x=[0, seq_length], y=[0, 0],
                             line={"width": 6, "color": "black"}, mode="lines",
                             name="sequence"))
    fig.update_yaxes(range=[0, 5], showticklabels=False)
    fig.update_xaxes(range=[0, seq_length], tickfont={"size": 20})
    fig.update_layout(height=300, plot_bgcolor='rgba(0,0,0,0)')
    plotly.offline.plot(fig, filename=str(outfile), auto_open=False)
